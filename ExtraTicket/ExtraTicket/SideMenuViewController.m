//
//  SideMenuViewController.m
//  ExtraTicket
//
//  Created by mac on 10/16/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import "SideMenuViewController.h"
#import "MFSideMenuContainerViewController.h"
#import "ETBuyTicketViewController.h"
#import "ETLoggedUserDetails.h"
#import "ETSendFeedBackViewController.h"
#import "ETAboutInfoViewController.h"
#import "ETHomeViewController.h"
#import "UIImageView+AFNetworking.h"
#import "ETMyTicketsViewController.h"
#import "ETEditProfileViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "ETMyFavouritesViewController.h"
#include "ETAppDelegate.h"


@interface SideMenuViewController (){
    IBOutlet UIImageView *imageViewProfilePic;
    IBOutlet UILabel *labelPofileName;
    IBOutlet UILabel *labelProfilePype;
    IBOutlet UITableView *tableViewMenu;
    NSString *stringCheckBuyOrCell;
    NSMutableArray *arrayTableViewImages;
    ETLoggedUserDetails *loggedUserDetails;
}

@end

@implementation SideMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTableData) name:@"ReloadMenuTable" object:nil];
    
    loggedUserDetails=[ETLoggedUserDetails sharedObject];
 
    NSString *imagePath=[NSString stringWithFormat:@"http://extraticketapp.com/extraticket/images/%@",loggedUserDetails.stringProfilePic];
    UIImage *placeHolder=[UIImage imageNamed:@"profilepic.png"];
    //[imageViewProfilePic sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:placeHolder];
    
    //TODO : Have to comment : Baji
    
//    [imageViewProfilePic setImageWithURL:[NSURL URLWithString:imagePath]
//                     placeholderImage:placeHolder
//                              options:SDWebImageRefreshCached];
    [imageViewProfilePic sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:placeHolder options:SDWebImageRefreshCached];
    
//    [imageViewProfilePic sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:placeHolder];
    labelPofileName.text=loggedUserDetails.stringUserName;
  
    arrayList=[[NSMutableArray alloc]initWithObjects:@"Home",@"Buy Ticket",@"Sell Ticket",@"About",@"Refer A Friend",@"Edit Profile",@"My Tickets",@"My Favorites",@"Log Out",nil];
    arrayTableViewImages=[[NSMutableArray alloc]initWithObjects:@"HomeNew.png",@"icon2.png",@"icon2.png",@"icon3.png",@"icon4.png",@"EditProfile.png",@"listicon6k.png",@"FavoritesNew.png",@"logout.png",nil];
    
    // Do any additional setup after loading the view from its nib.
}

- (MFSideMenuContainerViewController *)menuContainerViewController {
    return (MFSideMenuContainerViewController *)self.parentViewController;
}

-(void)reloadTableData
{
    [tableViewMenu reloadData];
}

#pragma mark -
#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    //return [NSString stringWithFormat:@"Section %d", section];
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrayList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    NSArray *nibs=[[NSBundle mainBundle] loadNibNamed:@"SlideViewTableCell" owner:nil options:nil];
    if ([nibs count]>0) {
        cell=[nibs objectAtIndex:0];
    }
    NSString *itemName=[[NSString alloc]initWithString:[arrayList objectAtIndex:indexPath.row]];
    UIImageView *imageView=(UIImageView *)[cell viewWithTag:1];
    UILabel *labelAnswer=(UILabel *)[cell viewWithTag:2];
    [labelAnswer setText:itemName];
    [imageView setImage:[UIImage imageNamed:[arrayTableViewImages objectAtIndex:indexPath.row]]];
    [cell setIndentationLevel:0];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell.textLabel setTextColor:[UIColor blackColor]];
    
    if (indexPath.row==6 && ([loggedUserDetails.stringMessageCount integerValue] != 0))
 {
        UIImageView *imageViewBgndSeeMsgCount = [[UIImageView alloc] initWithFrame:CGRectMake(220, 14, 25, 25)];
        [imageViewBgndSeeMsgCount setImage:[UIImage imageNamed:@"redDot.png"]];
        UILabel *labelToseeSeeMsgCount = [[UILabel alloc] initWithFrame:CGRectMake(220, 14, 25, 25)];
        [labelToseeSeeMsgCount setFont:[UIFont systemFontOfSize:14.0]];
     labelToseeSeeMsgCount.textAlignment = NSTextAlignmentCenter;
        NSString *stringMsgCount=[NSString stringWithFormat:@"%@",loggedUserDetails.stringMessageCount];
        [labelToseeSeeMsgCount setText:stringMsgCount];
        [cell.contentView addSubview:imageViewBgndSeeMsgCount];
        [cell.contentView addSubview:labelToseeSeeMsgCount];
    }
    
    return cell;
}
-(void)loadTableContents{
    
}

#pragma mark -
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers;
    
    if(indexPath.row==0)
    {
        ETHomeViewController *homeViewController = [[ETHomeViewController alloc] initWithNibName:@"ETHomeViewController" bundle:nil];
        homeViewController.title = [arrayList objectAtIndex:indexPath.row];
        controllers = [NSArray arrayWithObject:homeViewController];
        
    }
    if(indexPath.row==1)
    {
        stringCheckBuyOrCell=@"Buy";
        ETBuyTicketViewController *buyTicketViewController = [[ETBuyTicketViewController alloc] initWithNibName:@"ETBuyTicketViewController" bundle:nil];
        buyTicketViewController.title = [arrayList objectAtIndex:indexPath.row];
        [buyTicketViewController setStringCheckBuyOrCell:stringCheckBuyOrCell];
        controllers = [NSArray arrayWithObject:buyTicketViewController];
        
    }
    if(indexPath.row==2)
    {
        stringCheckBuyOrCell=@"Sell";
        ETBuyTicketViewController *buyTicketViewController = [[ETBuyTicketViewController alloc] initWithNibName:@"ETBuyTicketViewController" bundle:nil];
        buyTicketViewController.title = [arrayList objectAtIndex:indexPath.row];
        [buyTicketViewController setStringCheckBuyOrCell:stringCheckBuyOrCell];
        controllers = [NSArray arrayWithObject:buyTicketViewController];
        
    }

    if(indexPath.row==3)
    {
        
        ETAboutInfoViewController *aboutViewController = [[ETAboutInfoViewController alloc] initWithNibName:@"ETAboutInfoViewController" bundle:nil];
        aboutViewController.title = [arrayList objectAtIndex:indexPath.row];
        controllers = [NSArray arrayWithObject:aboutViewController];
        
    }
    else if(indexPath.row==4)
    {
        ETSendFeedBackViewController *sendFeedBackViewController = [[ETSendFeedBackViewController alloc] initWithNibName:@"ETSendFeedBackViewController" bundle:nil];
        sendFeedBackViewController.title = [arrayList objectAtIndex:indexPath.row];
        controllers = [NSArray arrayWithObject:sendFeedBackViewController];
        
    }
    else if(indexPath.row==5)
    {
        ETEditProfileViewController *editProfileViewController=[[ETEditProfileViewController alloc]initWithNibName:@"ETEditProfileViewController" bundle:nil];
        editProfileViewController.title=[arrayList objectAtIndex:indexPath.row];
        controllers=[NSArray arrayWithObject:editProfileViewController];
    }
    else if(indexPath.row==6){
        ETMyTicketsViewController *myTicketsViewController = [[ETMyTicketsViewController alloc] initWithNibName:@"ETMyTicketsViewController" bundle:nil];
        myTicketsViewController.title = [arrayList objectAtIndex:indexPath.row];
        controllers = [NSArray arrayWithObject:myTicketsViewController];
    }
    else if(indexPath.row==7){
        ETMyFavouritesViewController *myFavouritesViewController=[[ETMyFavouritesViewController alloc]initWithNibName:@"ETMyFavouritesViewController" bundle:nil];
        myFavouritesViewController.title = [arrayList objectAtIndex:indexPath.row];
        controllers = [NSArray arrayWithObject:myFavouritesViewController];
    }
    else if(indexPath.row==8){
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"LoggedIn"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"RememberMe"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        ETAppDelegate *appDelegate=(ETAppDelegate *)[[UIApplication sharedApplication]delegate];
        [appDelegate setFirstlyLogin];
    }
    
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
  //  if(self.searchBar.isFirstResponder) [self.searchBar resignFirstResponder];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
