//
//  ETBuyTicketViewController.h
//  ExtraTicket
//
//  Created by mac on 10/16/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "ETSellerDetails.h"
@interface ETBuyTicketViewController : UIViewController<CLLocationManagerDelegate>{
    IBOutlet UITextField *textFieldLocation;
    IBOutlet UITextField *textFieldSetLocation;
    IBOutlet UISwitch *switchLocation;
    IBOutlet UITextView *textViewDescription;
    IBOutlet UITextField *textFieldTitleToBuyTicket;
    IBOutlet UITextField *textFieldZipCode;
}
@property(nonatomic,strong)ETSellerDetails *sellerFromMyTickets;
@property(nonatomic,strong)NSString *stringCheckEdit;
@property(nonatomic,strong)NSString *stringCheckBuyOrCell;
-(IBAction)didFindCurrentPositionOnSwitchAction:(id)sender;
-(IBAction)saveDetailsToSetLocation:(id)sender;
-(IBAction)searchLocation:(id)sender;
@end
