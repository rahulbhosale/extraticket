//
//  ETSignUpViewController.h
//  ExtraTicket
//
//  Created by mac on 10/15/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ETSignUpViewController : UIViewController{
    IBOutlet UITextField *textFieldUserName;
    IBOutlet UITextField *textFieldEmailID;
    IBOutlet UITextField *textFieldPhoneNumber;
    IBOutlet UITextField *textFieldPassword;
    IBOutlet UITextField *textFieldConfirmPassword;
    IBOutlet UITextField *textFieldZipCode;
    IBOutlet UIImageView *imageViewProficPic;
    IBOutlet UIButton *buttonTickMe;
    IBOutlet UIButton *buttonSelectBuyer;
    IBOutlet UIButton *buttonSelectSeller;
    
    NSData *pngData;
    BOOL isSelectedForTermsAndConditions;
}
@property(nonatomic,strong) IBOutlet UIButton *btnTermsAndConditions;
-(IBAction)didRegisteringAction:(id)sender;
-(IBAction)didChoosingBuyerAndSellerAction:(id)sender;
-(IBAction)didSelectingTermsAndConditions:(id)sender;
-(IBAction)didTickMarkChosenTermsAndConditions:(id)sender;
@end
