//
//  ETAppDelegate.m
//  ExtraTicket
//
//  Created by mac on 10/15/13.
//  Copyright (c) 2013 mac. All rights reserved.
//



/*
 
 userInfo {
 aps =     {
 alert = "New Ad Posted in your area";
 badge = 4;
 sound = default;
 type = ticket;
 };
 }

 
 */

/*
 
 userInfo {
 aps =     {
 alert = "How about business?";
 badge = 3;
 "current_location" = "Kazhakkoottam,Kerala";
 fromuserid = 13;
 "select_location" = "";
 sound = default;
 ticketid = 120;
 touserid = 12;
 type = chat;
 };
 }
 
 */


#import "ETAppDelegate.h"
#import "SideMenuViewController.h"
#import "MFSideMenuContainerViewController.h"
#import "ETLoginViewController.h"
#import "ETHomeViewController.h"
#import "ETEditProfileViewController.h"
#import "AFNetworking.h"
#import "ETLoggedUserDetails.h"
#import "ETChatViewController.h"
#import "ETSellerDetails.h"
#import "ETMyTicketsMessagesViewController.h"
#import <objc/runtime.h>
#import "ETAdDetailViewController.h"

@implementation UIApplication (Private)

- (BOOL)customOpenURL:(NSURL*)url
{
    ETAppDelegate *MyWatcher = (ETAppDelegate *) [[UIApplication sharedApplication] delegate];
    if (MyWatcher.currentViewController) {
        [MyWatcher.currentViewController handleURL:url];
        return YES;
    }
    return NO;
}
@end

@implementation ETAppDelegate
{
    ETLoggedUserDetails *loggedUserDetails;
    NSString *pushTicketID;
    BOOL isFromBackGround;
}
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize currentViewController=_currentViewController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Start Crashlytics
    application.applicationIconBadgeNumber = 0;
    [self addPush];
    [self updatePushCount];
    if (launchOptions) {
        application.applicationIconBadgeNumber = [[[[launchOptions objectForKey:@"UIApplicationLaunchOptionsRemoteNotificationKey"] objectForKey:@"aps"] objectForKey:@"badge"]integerValue];
        [self NotfnData:launchOptions];
    }
    else{
    }
    float systemVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (systemVersion >= 7.0) {
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"topbar.png"] forBarMetrics:UIBarMetricsDefault];
    }
    else{
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"topbariOS6.png"] forBarMetrics:UIBarMetricsDefault];
    
    }
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    CGSize iOSDeviceScreenSize ;
    iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
    UIImageView *imgBackground=[[UIImageView alloc]initWithFrame:[[UIScreen mainScreen] bounds]];
    [imgBackground setContentMode:UIViewContentModeScaleAspectFill];
    
    // Override point for customization after application launch.
    if (iOSDeviceScreenSize.height == 568)
    {
        [imgBackground setImage:[UIImage imageNamed:@"splash2x.png"]];
        [self.window addSubview:imgBackground];
    }
    else{
        
        [imgBackground setImage:[UIImage imageNamed:@"splash.png"]];
        [self.window addSubview:imgBackground];
    }
     [self.window makeKeyAndVisible];
    self.window.backgroundColor = [UIColor whiteColor];
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"LoggedIn"]) {
        locationManager = [[CLLocationManager alloc] init];
        [self gettingCurrentLocation];
    }
    else{
        [self setFirstlyLogin];
    }
    return YES;
}
/*
 {
 aps =     {
 alert = "New Ad Posted in your area";
 badge = 5;
 id = 142;
 sound = default;
 type = ticket;
 };
 }

 */
-(void)gettingCurrentLocation{
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    [locationManager startUpdatingLocation];
}
#pragma mark- CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    [locationManager stopUpdatingLocation];
    locationManager.delegate = nil;
    CLLocation *currentLocation = newLocation;
    if (currentLocation != nil) {
    }
    float degrees = newLocation.coordinate.latitude;
    double decimal = fabs(newLocation.coordinate.latitude - degrees);
    int minutes = decimal * 60;
    double seconds = decimal * 3600 - minutes * 60;
    degrees = newLocation.coordinate.longitude;
    decimal = fabs(newLocation.coordinate.longitude - degrees);
    minutes = decimal * 60;
    seconds = decimal * 3600 - minutes * 60;
    longitude =[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
    latitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    userEmail = [prefs stringForKey:@"Email"];
    password = [prefs stringForKey:@"Password"];
    [self performLogInAction];
}
-(void)performLogInAction{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *stringDevToken = [prefs valueForKey:@"token"];
    NSString *pathParameters = [NSString stringWithFormat:@"http://extraticketapp.com/extraticket/login.php?email=%@&password=%@&longitude=%@&latitude=%@&device_token=%@",userEmail,password,longitude,latitude,stringDevToken];
    pathParameters = [pathParameters stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [[NSURL alloc] initWithString:pathParameters];
    // NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:20
                             ];
    NSSet *contentType = [[NSSet alloc] initWithObjects:@"text/plain",@"text/html", nil];
    [AFJSONRequestOperation addAcceptableContentTypes:contentType];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"Bajirao : %@", (NSString *) JSON);
        if ([[[JSON objectForKey:@"root"]objectForKey:@"Result"]isEqualToString:@"Not a Member"]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Your login failed.Please check your username and password." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        else{
            loggedUserDetails=[ETLoggedUserDetails sharedObject];
            loggedUserDetails.stringUserID=[[[JSON objectForKey:@"root"]objectForKey:@"Details"] objectForKey:@"id"];
            loggedUserDetails.stringUserName=[[[JSON objectForKey:@"root"]objectForKey:@"Details"] objectForKey:@"username"];
            loggedUserDetails.stringUserEmail=[[[JSON objectForKey:@"root"]objectForKey:@"Details"] objectForKey:@"emailid"];
            loggedUserDetails.stringBuyerOrSeller=[[[JSON objectForKey:@"root"]objectForKey:@"Details"] objectForKey:@"type"];
            loggedUserDetails.stringUserPhoneNumber=[[[JSON objectForKey:@"root"]objectForKey:@"Details"] objectForKey:@"phone_num"];
            loggedUserDetails.stringProfilePic=[[[JSON objectForKey:@"root"]objectForKey:@"Details"] objectForKey:@"profile_pic"];
            loggedUserDetails.stringPassword=[[[JSON objectForKey:@"root"]objectForKey:@"Details"] objectForKey:@"password"];
            loggedUserDetails.stringMessageCount=[[[JSON objectForKey:@"root"]objectForKey:@"Details"] objectForKey:@"message_count"];
            [self setLogin];
        }
    }
                                                                                        failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {UIAlertView *temp=[[UIAlertView alloc]initWithTitle:@"Message!" message:@"No response from webserver" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                                                                            [temp show];
                                                                                        }];
    [operation start];
   
}
-(void)updatePushCount{
    NSUserDefaults *prefsForId = [NSUserDefaults standardUserDefaults];
    userId = [prefsForId stringForKey:@"userID"];
    NSString *pathParameters = [NSString stringWithFormat:@"http://extraticketapp.com/extraticket/update_pushcount.php?user_id=%@",userId];
    NSLog(@"%@",pathParameters);
//    pathParameters = [pathParameters stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [[NSURL alloc] initWithString:pathParameters];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:[NSURLRequest requestWithURL:url] delegate:nil];
    [connection start];
//    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLCacheStorageNotAllowed timeoutInterval:20];
//    NSSet *contentType = [[NSSet alloc] initWithObjects:@"text/plain",@"text/html", nil];
//    [AFJSONRequestOperation addAcceptableContentTypes:contentType];
//    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
//        NSLog(@"%@",JSON);
//        if ([[JSON objectForKey:@"root"]isEqualToString:@"updated"]) {
//            NSLog(@"--------\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\----------------");
//        }
//        else{
//        }
//    }
//                                                                                        failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
//                                                                                            UIAlertView *temp=[[UIAlertView alloc]initWithTitle:@"Message!" message:@"No response from webserver" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//                                                                                            [temp show];
//                                                                                        }];
//    [operation start];
}

-(void)addPush
{
    if (![[NSUserDefaults standardUserDefaults] valueForKey:@"EnablePushNotification"]){
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }else{
        if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"EnablePushNotification"] isEqualToString:@"yes"]) {
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
             (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
        }
    }
   
}

-(void)cancelPush
{
    [[UIApplication sharedApplication] unregisterForRemoteNotifications];

}

-(void)setFirstlyLogin{
    ETLoginViewController *loginViewController=[[ETLoginViewController alloc]initWithNibName:@"ETLoginViewController" bundle:nil];
    navController=[[UINavigationController alloc]initWithRootViewController:loginViewController];
    [self.window setRootViewController:navController];
 
}
-(void)setLogin{
    SideMenuViewController *leftMenuViewController = [[SideMenuViewController alloc] init];
    SideMenuViewController *rightMenuViewController = [[SideMenuViewController alloc] init];
    MFSideMenuContainerViewController *container = [MFSideMenuContainerViewController
                                                    containerWithCenterViewController:[self navigationController]
                                                    leftMenuViewController:leftMenuViewController
                                                    rightMenuViewController:rightMenuViewController];
    self.window.rootViewController = container;
}
-(ETHomeViewController *)homeViewController
{
    return [[ETHomeViewController alloc] initWithNibName:@"ETHomeViewController" bundle:nil];
}
- (UINavigationController *)navigationController
{
    // [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"nav640x88.png"] forBarMetrics:UIBarMetricsDefault];
    return [[UINavigationController alloc]
            initWithRootViewController:[self homeViewController]];
    
}

//-(ETLoginViewController *)loginViewController
//{
//    return [[ETLoginViewController alloc] initWithNibName:@"ETLoginViewController" bundle:nil];
//}
//- (UINavigationController *)navigationController
//{
//   // [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"nav640x88.png"] forBarMetrics:UIBarMetricsDefault];
//    return [[UINavigationController alloc]
//            initWithRootViewController:[self loginViewController]];
//    
//}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    Method customOpenUrl = class_getInstanceMethod([UIApplication class], @selector(customOpenURL:));
    Method openUrl = class_getInstanceMethod([UIApplication class], @selector(openURL:));
   

    method_exchangeImplementations(openUrl, customOpenUrl);

    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
            abort();
        } 
    }
}

#pragma mark Notifications

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"userInfo %d",[application applicationState]);

    application.applicationIconBadgeNumber=application.applicationIconBadgeNumber+1;
    [self updatePushCount];
//    if ([application applicationState] == UIApplicationStateActive) {
        [self NotfnData:userInfo];
//    }
}


-(void)NotfnData:(NSDictionary *)userNotfn {
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;

    if([[[userNotfn objectForKey:@"aps"] valueForKey:@"type"] isEqualToString:@"ticket"])
    {
        NSLog(@"userInfo %d",[[UIApplication sharedApplication] applicationState]);
        
        stringPushNotificationContent=[[userNotfn objectForKey:@"aps"] objectForKey:@"alert"];
        pushTicketID = userNotfn[@"aps"][@"id"];
        if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive) {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Extra Ticket Alert!" message:[[userNotfn objectForKey:@"aps"] objectForKey:@"alert"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel", nil];
            
            alert.tag=102;
            [alert show];
        }
        else{
            ETAdDetailViewController *detailViewController = [[ETAdDetailViewController alloc] initWithNibName:@"ETAdDetailViewController" bundle:Nil fromPushNotification:YES];
            detailViewController.sellerAdDetails = [[ETSellerDetails alloc] init];
            detailViewController.sellerAdDetails.stringTicketId = pushTicketID;
            [self.window.rootViewController presentViewController:[[UINavigationController alloc] initWithRootViewController:detailViewController] animated:YES completion:nil];
        }
        
    }
    else
    {
        NSLog(@"userInfo %d",[[UIApplication sharedApplication] applicationState]);
        
        sellerDetails=[[ETSellerDetails alloc]init];
        sellerDetails.stringPosterId=[[userNotfn objectForKey:@"aps"] objectForKey:@"fromuserid"];
        sellerDetails.stringTicketId=[[userNotfn objectForKey:@"aps"] objectForKey:@"ticketid"];
        sellerDetails.stringPosterSetLocation=[[userNotfn objectForKey:@"aps"] objectForKey:@"select_location"];
        sellerDetails.stringPosterCurrentLocation=[[userNotfn objectForKey:@"aps"] objectForKey:@"current_location"];
        stringPushNotificationContent=[[userNotfn objectForKey:@"aps"] objectForKey:@"alert"];
        if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive) {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Extra Ticket Alert!" message:[[userNotfn objectForKey:@"aps"] objectForKey:@"alert"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel", nil];
            alert.tag=101;
            [alert show];
        }
        else{
            if ([stringPushNotificationContent isEqualToString:@"New Ad Posted in your area"]) {
                [self setLogin];
            }
            else{
                
                ETLoggedUserDetails *loggedUserDetailss=[ETLoggedUserDetails sharedObject];
                if ([loggedUserDetailss.stringUserID isEqualToString:sellerDetails.stringPosterId]) {
                    
                    //    [self setLoginForChat];
                    ETMyTicketsMessagesViewController *myTicketsMessagesViewController=[[ETMyTicketsMessagesViewController alloc]initWithNibName:@"ETMyTicketsMessagesViewController" bundle:nil];
                    myTicketsMessagesViewController.sellerFromMyTicketsForMessages=sellerDetails;
                    stringLoadingFromPush=@"pushnotification";
                    [myTicketsMessagesViewController setStringLoadingFromPush:stringLoadingFromPush];
                    UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:myTicketsMessagesViewController];
                    [self.window.rootViewController presentViewController:nav animated:NO completion:nil];
                }
                else{
                    //[self setLoginForChat];
                    ETChatViewController *chatViewController=[[ETChatViewController alloc]initWithNibName:@"ETChatViewController" bundle:nil];
                    stringLoadingFromPush=@"pushnotification";
                    [chatViewController setStringLoadingFromPush:stringLoadingFromPush];
                    chatViewController.sellerFromMyTicketsForChat=sellerDetails;
                    UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:chatViewController];
                    [self.window.rootViewController presentViewController:nav animated:NO completion:nil];
                    
                }
                
            }
        }
        
    }
    
//    
//    sellerDetails=[[ETSellerDetails alloc]init];
//    sellerDetails.stringPosterId=[[userNotfn objectForKey:@"aps"] objectForKey:@"fromuserid"];
//    sellerDetails.stringTicketId=[[userNotfn objectForKey:@"aps"] objectForKey:@"ticketid"];
//    sellerDetails.stringPosterSetLocation=[[userNotfn objectForKey:@"aps"] objectForKey:@"select_location"];
//    sellerDetails.stringPosterCurrentLocation=[[userNotfn objectForKey:@"aps"] objectForKey:@"current_location"];
//    if ([[userNotfn objectForKey:@"aps"] objectForKey:@"alert"]==nil || [[userNotfn objectForKey:@"aps"] objectForKey:@"alert"]==NULL) {
//        
//    }else{
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Extra Ticket Alert!" message:[[userNotfn objectForKey:@"aps"] objectForKey:@"alert"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel", nil];
//        stringPushNotificationContent=[[userNotfn objectForKey:@"aps"] objectForKey:@"alert"];
//        alert.tag=101;
//        [alert show];
//    }
    
    [self updatePushCount];

}



- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    NSString *devTockenString = [[[[deviceToken description] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (!deviceToken) {
        devTockenString = @"1234567890";
    }
    NSLog(@"%@",devTockenString);
    NSUserDefaults *userDef=[NSUserDefaults standardUserDefaults];
    [userDef setValue:devTockenString forKey:@"token"];
    [userDef synchronize];
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error{
    NSLog(@"%@",error);
    NSUserDefaults *userDef=[NSUserDefaults standardUserDefaults];
    [userDef setValue:@"1234567890" forKey:@"token"];
    [userDef synchronize];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (alertView.tag == 102)
    {
      if( buttonIndex == 0)
        {
            ETAdDetailViewController *detailViewController = [[ETAdDetailViewController alloc] initWithNibName:@"ETAdDetailViewController" bundle:Nil fromPushNotification:YES];
            detailViewController.sellerAdDetails = [[ETSellerDetails alloc] init];
            detailViewController.sellerAdDetails.stringTicketId = pushTicketID;
            [self.window.rootViewController presentViewController:[[UINavigationController alloc] initWithRootViewController:detailViewController] animated:YES completion:nil];
        
        }
        else if (buttonIndex == 1)
        {
            NSLog(@"Cancel");
        }
    }
    
    
    else if (alertView.tag == 101)
    {
        if( buttonIndex == 0)
        {
            if ([stringPushNotificationContent isEqualToString:@"New Ad Posted in your area"]) {
                [self setLogin];
            }
            else{
                
                ETLoggedUserDetails *loggedUserDetailss=[ETLoggedUserDetails sharedObject];
                if ([loggedUserDetailss.stringUserID isEqualToString:sellerDetails.stringPosterId]) {
                    
                    //    [self setLoginForChat];
                    ETMyTicketsMessagesViewController *myTicketsMessagesViewController=[[ETMyTicketsMessagesViewController alloc]initWithNibName:@"ETMyTicketsMessagesViewController" bundle:nil];
                    myTicketsMessagesViewController.sellerFromMyTicketsForMessages=sellerDetails;
                    stringLoadingFromPush=@"pushnotification";
                    [myTicketsMessagesViewController setStringLoadingFromPush:stringLoadingFromPush];
                    UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:myTicketsMessagesViewController];
                    [self.window.rootViewController presentViewController:nav animated:YES completion:nil];
                }
                else{
                    //[self setLoginForChat];
                    ETChatViewController *chatViewController=[[ETChatViewController alloc]initWithNibName:@"ETChatViewController" bundle:nil];
                    stringLoadingFromPush=@"pushnotification";
                    [chatViewController setStringLoadingFromPush:stringLoadingFromPush];
                    chatViewController.sellerFromMyTicketsForChat=sellerDetails;
                    UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:chatViewController];
                    [self.window.rootViewController presentViewController:nav animated:YES completion:nil];
                    
                }
                
            }
        }
        else if (buttonIndex == 1)
        {
            NSLog(@"Cancel");
        }
    }

    
}

-(void)setLoginForChat{
    SideMenuViewController *leftMenuViewController = [[SideMenuViewController alloc] init];
    SideMenuViewController *rightMenuViewController = [[SideMenuViewController alloc] init];
    MFSideMenuContainerViewController *container = [MFSideMenuContainerViewController
                                                    containerWithCenterViewController:[self navigationControllerforChat]
                                                    leftMenuViewController:leftMenuViewController
                                                    rightMenuViewController:rightMenuViewController];
    self.window.rootViewController = container;
}
-(ETChatViewController *)chatViewController
{
    
    ETChatViewController *obj=[[ETChatViewController alloc] initWithNibName:@"ETChatViewController" bundle:nil];
    obj.sellerFromMyTicketsForChat=sellerDetails;
    return obj;
    
}
- (UINavigationController *)navigationControllerforChat
{
    // [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"nav640x88.png"] forBarMetrics:UIBarMetricsDefault];
    return [[UINavigationController alloc]
            initWithRootViewController:[self chatViewController]];
    
}



#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"ExtraTicket" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"ExtraTicket.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        abort();
    }    
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
