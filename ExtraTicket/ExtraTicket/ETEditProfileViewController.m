//
//  ETEditProfileViewController.m
//  ExtraTicket
//
//  Created by mac on 10/18/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import "ETEditProfileViewController.h"
#import "MFSideMenuContainerViewController.h"
#import "ETLoggedUserDetails.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "MBProgressHUD.h"
#import "ETAppDelegate.h"
#import "SideMenuViewController.h"
#import "ETHomeViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "MBProgressHUD.h"

@interface ETEditProfileViewController ()<MBProgressHUDDelegate>{
    MBProgressHUD *HUD;
}

@end

@implementation ETEditProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupMenuBarButtonItems];
    [self loadUserDetails];
   // UIBarButtonItem *leftBar=[[UIBarButtonItem alloc]initWithTitle:@"Log Out" style:UIBarButtonItemStylePlain target:self action:@selector(logoutAction)];
  //  self.navigationItem.rightBarButtonItem=leftBar;
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"EnablePushNotification"] isEqualToString:@"yes"]) {
        [switchNotification setOn:NO];
    }else{
        [switchNotification setOn:YES];
    }

    self.navigationItem.title=@"Edit Profile";
    UITapGestureRecognizer *tapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(uploadProfilePicture:)];
    tapGestureRecognizer.numberOfTapsRequired=1;
    [imageViewProficPic addGestureRecognizer:tapGestureRecognizer];

}
-(void)uploadProfilePicture:(id)sender{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Choose Image Source"delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: @"Take Photo",@"Choose Photo",nil];
    [alert show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    UIImagePickerController *picker=[[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    if (buttonIndex == 1){
        if (![UIImagePickerController isCameraDeviceAvailable: UIImagePickerControllerCameraDeviceRear])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Camera Available" message:@"his Feature requires camera"  delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil];
            [alert show];
        }
        else{
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:picker animated:YES completion:Nil];
            [picker setAllowsEditing:NO];
            
        }
    }
    else if (buttonIndex == 2){
        picker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:Nil];
        [picker setAllowsEditing:NO];
    }
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage : (UIImage *)image editingInfo:(NSDictionary *)editingInfo{
    imageViewProficPic.image=image;
    pngData=UIImageJPEGRepresentation(image, 0.5);
    // imageViewProfilePhoto.contentMode=UIViewContentModeScaleAspectFit;
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)logoutAction{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"LoggedIn"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    ETAppDelegate *appDelegate=(ETAppDelegate *)[[UIApplication sharedApplication]delegate];
    [appDelegate setFirstlyLogin];
}
-(void)loadUserDetails{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.mode = MBProgressHUDModeAnnularDeterminate;
    HUD.progress = 0.01f;
    [HUD show:YES];
    ETLoggedUserDetails *loggedUserDetails=[ETLoggedUserDetails sharedObject];
    NSString *pathParameters = [NSString stringWithFormat:@"http://extraticketapp.com/extraticket/users_details.php?user_id=%@",loggedUserDetails.stringUserID];
    pathParameters = [pathParameters stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [[NSURL alloc] initWithString:pathParameters];
    // NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:5
                             ];
    NSSet *contentType = [[NSSet alloc] initWithObjects:@"text/plain",@"text/html", nil];
    [AFJSONRequestOperation addAcceptableContentTypes:contentType];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        if ([[JSON objectForKey:@"root"] isKindOfClass:[NSString class]]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"There are no details" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [HUD hide:YES];
        }
        else{
            textFieldUserName.text=[[JSON objectForKey:@"root"] objectForKey:@"username"];
            textFieldEmailID.text=[[JSON objectForKey:@"root"] objectForKey:@"emailid"];
            textFieldPhoneNumber.text=[[JSON objectForKey:@"root"] objectForKey:@"phone_num"];
            textFieldZipCode.text=[[JSON objectForKey:@"root"] objectForKey:@"zipcode"];
            NSString *imagePath=[NSString stringWithFormat:@"http://extraticketapp.com/extraticket/images/%@",[[JSON objectForKey:@"root"] objectForKey:@"profile_pic"]];
            UIImage *placeHolder=[UIImage imageNamed:@"profilepic.png"];
            [imageViewProficPic sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:placeHolder];
            textFieldPassword.text=[[JSON objectForKey:@"root"] objectForKey:@"password"];
            [HUD hide:YES];
        }
    }
                                                                                        failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                                                            UIAlertView *temp=[[UIAlertView alloc]initWithTitle:@"Message!" message:@"No response from webserver" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                                                                            [temp show];
                                                            [HUD hide:YES];                            }];
    [operation start];
}

#pragma  mark-split view controller
- (MFSideMenuContainerViewController *)menuContainerViewController {
    return (MFSideMenuContainerViewController *)self.navigationController.parentViewController;
}
#pragma mark - UIBarButtonItems

- (void)setupMenuBarButtonItems
{
    UIButton *leftBarButtonItem = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftBarButtonItem setImage:[UIImage imageNamed:@"menu-icon.png"] forState:UIControlStateNormal];
    leftBarButtonItem.frame = CGRectMake(0, 0, 43, 28);
    [leftBarButtonItem addTarget:self action:@selector(leftSideMenuButtonPressed:) forControlEvents:
     UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftBarButtonItem];
}

#pragma mark UIButtonActions

-(IBAction)switchNotification:(id)sender{
    if ([switchNotification isOn]) {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
        NSUserDefaults *userDef=[NSUserDefaults standardUserDefaults];
        [userDef setValue:@"no" forKey:@"EnablePushNotification"];
        [userDef synchronize];
    }
    else{
        NSUserDefaults *userDef=[NSUserDefaults standardUserDefaults];
        [userDef setValue:@"yes" forKey:@"EnablePushNotification"];
        [userDef synchronize];
        [[UIApplication sharedApplication] unregisterForRemoteNotifications];
    }
}

-(IBAction)didUpdationAction:(id)sender{
    //http://extraticketapp.com/extraticket/edit_userdetails.php?user_id=3&username=revathy&email=revu@gmail.com&password=qwerty&phone_num=271348&image=revu@gmail.com.jpg&zipcode=123
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.mode = MBProgressHUDModeAnnularDeterminate;
    HUD.progress = 0.01f;
    [HUD show:YES];
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    if([emailTest evaluateWithObject:textFieldEmailID.text] == NO)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"You Entered Incorrect Email ID." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        [HUD hide:YES];
        return;
    }
    
    else
    {
        ETLoggedUserDetails *loggedUserDetails=[ETLoggedUserDetails sharedObject];
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];//type=buyer
        [parameters setObject:loggedUserDetails.stringUserID forKey:@"user_id"];
        [parameters setObject:textFieldUserName.text forKey:@"username"];
        [parameters setObject:textFieldEmailID.text forKey:@"email"];
        [parameters setObject:textFieldPassword.text forKey:@"password"];
        [parameters setObject:textFieldPhoneNumber.text forKey:@"phone_num"];
        [parameters setObject:textFieldZipCode.text forKey:@"zipcode"];
         NSURL *url = [NSURL URLWithString:@"http://extraticketapp.com/extraticket/"];
//        pngData=UIImagePNGRepresentation(imageViewProficPic.image);
        pngData = UIImageJPEGRepresentation(imageViewProficPic.image, 0.6);
        AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
        NSMutableURLRequest *afRequest = [httpClient multipartFormRequestWithMethod:@"POST"
                                                                               path:@"edit_userdetails.php"
                                                                         parameters:parameters
                                                          constructingBodyWithBlock:^(id <AFMultipartFormData>formData)
                                          {
                                              [formData appendPartWithFileData:pngData
                                                                          name:@"image"
                                                                      fileName:[NSString stringWithFormat:@"%@.jpg",textFieldEmailID.text]
                                                                      mimeType:@"image/jpeg"];
                                              
                                          }
                                          ];
        AFHTTPRequestOperation *operation =
        [httpClient HTTPRequestOperationWithRequest:afRequest
                                            success:^(AFHTTPRequestOperation *operation, id json) {
                                                NSData *resultData=(NSData *)json;
                                                NSDictionary *result = [NSJSONSerialization JSONObjectWithData:resultData options:NSJSONReadingAllowFragments error:nil];
                                                if ([[[result objectForKey:@"root"]objectForKey:@"Result"]isEqualToString:@"Updated"]){
                                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"You have updated the profile successfully" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                    [alert show];
                                                    NSArray *viewControllers = [self.navigationController viewControllers];
                                                    for (UIViewController *controller in viewControllers) {
                                                        if ([controller isKindOfClass:[ETHomeViewController class]]) {
                                                            [self.navigationController popToViewController:controller animated:YES];
                                                        }
                                                    }

                                                    [HUD hide:YES];
                                                }
                                            }
                                            failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message!" message:@"Connection Error,No response from web servies!!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                [alert show];
                                                [HUD hide:YES];
                                            }];
        [httpClient enqueueHTTPRequestOperation:operation];
    }
  
}
#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - UIBarButtonItem Callbacks

- (void)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)leftSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^
     {
         [self setupMenuBarButtonItems];
     }];
}

- (void)rightSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleRightSideMenuCompletion:^
     {
         [self setupMenuBarButtonItems];
     }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
