//
//  ETSellerDetails.h
//  ExtraTicket
//
//  Created by mac on 10/16/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ETSellerDetails : NSObject{
    
}
@property(nonatomic,strong)NSString *stringPosterImage;
@property(nonatomic,strong)NSString *stringPosterName;
@property(nonatomic,strong)NSString *stringPosterPhoneNo;
@property(nonatomic,strong)NSString *stringPosterDate;
@property(nonatomic,strong)NSString *stringPosterDescription;
@property(nonatomic,strong)NSString *stringPosterCurrentLocation;
@property(nonatomic,strong)NSString *stringPosterSetLocation;
@property(nonatomic,strong)NSString *stringPosterId;
@property(nonatomic,strong)NSString *stringTicketId;
@property(nonatomic,strong)NSString *stringAdTitle;
@property(nonatomic,strong)NSString *stringDistance;
@property(nonatomic,strong)NSString *stringTypeOfTicket;
@property(nonatomic,strong)NSString *stringZipCode;
@property(nonatomic,strong)NSString *stringFavourite;
@property(nonatomic,strong)NSString *stringLatitude;
@property(nonatomic,strong)NSString *stringLongitude;
@property(nonatomic,strong)NSString *stringBadgeForMessageCount;


@end
