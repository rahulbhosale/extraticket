//
//  ETHomeViewController.m
//  ExtraTicket
//
//  Created by mac on 10/15/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import "ETHomeViewController.h"
#import "MFSideMenuContainerViewController.h"
#import "MBProgressHUD.h"
#import "AFNetworking.h"
#import "ETSellerDetails.h"
#import "ETLoggedUserDetails.h"
#import "ETChatViewController.h"
//#import <SDWebImage/UIImageView+WebCache.h>
#import "ETAdDetailViewController.h"
#import "ETAdLocationViewController.h"
#import "ETAppDelegate.h"
#include "UIImageView+AFNetworking.h"
#import "ETCommonTableCell.h"

@interface ETHomeViewController (){
    UIBarButtonItem *leftBar;
    NSMutableArray *arraySellerDetails;
    MBProgressHUD *HUD;
    BOOL isSelected;
    UIButton *buttonFavourites;
    ETSellerDetails *sellerDetailsToMakePhoneCall;
    int valueForBuySellAll;
}

@end

@implementation ETHomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma  mark-split view controller
- (MFSideMenuContainerViewController *)menuContainerViewController {
    return (MFSideMenuContainerViewController *)self.navigationController.parentViewController;
}
#pragma mark - UIBarButtonItems

- (void)setupMenuBarButtonItems
{
    UIButton *leftBarButtonItem = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftBarButtonItem setImage:[UIImage imageNamed:@"menu-icon.png"] forState:UIControlStateNormal];
    leftBarButtonItem.frame = CGRectMake(0, 0, 43, 28);
    [leftBarButtonItem addTarget:self action:@selector(leftSideMenuButtonPressed:) forControlEvents:
     UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftBarButtonItem];
}


#pragma mark - UIBarButtonItem Callbacks

- (void)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)leftSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^
     {
         [self setupMenuBarButtonItems];
     }];
}

- (void)rightSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleRightSideMenuCompletion:^
     {
         [self setupMenuBarButtonItems];
     }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    leftBar=[[UIBarButtonItem alloc]initWithTitle:@"Delete" style:UIBarButtonItemStylePlain target:self action:@selector(DeleteAction)];
    float systemVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (systemVersion < 7.0) {
       [searchBarForTable setTintColor:[UIColor brownColor]];
        [leftBar setTintColor:[UIColor brownColor]];
    }
    arraytableData=[[NSMutableArray alloc]init];
    [self setupMenuBarButtonItems];
    isSelected=NO;
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationItem.title=@"Home";
    arraySellerDetails=[[NSMutableArray alloc]init];
    [segmentControlForSellAndBuy setSelectedSegmentIndex:0];
    [self getParsedDetailsForAll];
    self.navigationItem.rightBarButtonItem=leftBar;
}
-(void)DeleteAction{
    if(self.editing)
	{
		[super setEditing:NO animated:NO];
		[tableViewSellerDetails setEditing:NO animated:NO];
		[tableViewSellerDetails reloadData];
		self.navigationItem.rightBarButtonItem.title=@"delete";
    }
	else
    {
		[super setEditing:YES animated:YES];
		[tableViewSellerDetails setEditing:YES animated:YES];
		[tableViewSellerDetails reloadData];
        self.navigationItem.rightBarButtonItem.title=@"done";
        
	}
}
-(void)getParsedDetailsForSeller{
    [arraySellerDetails removeAllObjects];
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.mode = MBProgressHUDModeAnnularDeterminate;
    HUD.progress = 0.01f;
    [HUD show:YES];
    ETLoggedUserDetails *loggedUserDetails=[ETLoggedUserDetails sharedObject];
    NSString *pathParameters = [NSString stringWithFormat:@"http://extraticketapp.com/extraticket/list_sellersposts.php?user_id=%@",loggedUserDetails.stringUserID];
    NSURL *url = [[NSURL alloc] initWithString:pathParameters];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10];
    NSSet *contentType = [[NSSet alloc] initWithObjects:@"text/plain",@"text/html", nil];
    [AFJSONRequestOperation addAcceptableContentTypes:contentType];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        id result=[JSON objectForKey:@"root"];
        if (![result isKindOfClass:[NSArray class]]) {
            [HUD hide:YES];
             [arraytableData removeAllObjects];
            [tableViewSellerDetails reloadData];
        }
        else
        {
            for(int j=0;j<[result count];j++)
            {
                NSDictionary *dictionary = [result objectAtIndex:j];
                ETSellerDetails *sellerDetails=[[ETSellerDetails alloc]init];
                sellerDetails.stringPosterId=[dictionary objectForKey:@"user_id"];
                sellerDetails.stringTicketId=[dictionary objectForKey:@"ticket_id"];
                sellerDetails.stringAdTitle=[dictionary objectForKey:@"ticket_title"];
                sellerDetails.stringDistance=[dictionary objectForKey:@"distance"];
                sellerDetails.stringPosterDescription=[dictionary objectForKey:@"ticket_description"];
                sellerDetails.stringPosterCurrentLocation=[dictionary objectForKey:@"current_location"];
                sellerDetails.stringPosterSetLocation=[dictionary objectForKey:@"select_location"];
                sellerDetails.stringPosterName=[dictionary objectForKey:@"seller_name"];
                sellerDetails.stringPosterPhoneNo=[dictionary objectForKey:@"seller_phone"];
                sellerDetails.stringPosterDate=[dictionary objectForKey:@"date_time"];
                sellerDetails.stringPosterImage=[dictionary objectForKey:@"seller_profile_pic"];
                sellerDetails.stringTypeOfTicket=[dictionary objectForKey:@"type"];
                sellerDetails.stringZipCode=[dictionary objectForKey:@"zipcode"];
                sellerDetails.stringFavourite=[dictionary objectForKey:@"favourite"];
                valueForBuySellAll=FAVOURITESFINDAFTERPARSESELL;
                [arraySellerDetails addObject:sellerDetails];
            }
            [arraytableData removeAllObjects];
            [arraytableData addObjectsFromArray:arraySellerDetails];
            [HUD hide:YES];
            [tableViewSellerDetails reloadData];
        }
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        UIAlertView *temp=[[UIAlertView alloc]initWithTitle:@"Message!" message:@"No response from webserver" delegate:self
                                          cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [temp show];
        [HUD hide:YES];
    }];
    [operation start];
    segmentControlForSellAndBuy.userInteractionEnabled=YES;
}
-(void)getParsedDetailsForBuyer{
   [arraySellerDetails removeAllObjects];
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.mode = MBProgressHUDModeAnnularDeterminate;
    HUD.progress = 0.01f;
    [HUD show:YES];
    ETLoggedUserDetails *loggedUserDetails=[ETLoggedUserDetails sharedObject];
    NSString *pathParameters = [NSString stringWithFormat:@"http://extraticketapp.com/extraticket/list_buyersposts.php?user_id=%@",loggedUserDetails.stringUserID];
    NSURL *url = [[NSURL alloc] initWithString:pathParameters];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10];
    NSSet *contentType = [[NSSet alloc] initWithObjects:@"text/plain",@"text/html", nil];
    [AFJSONRequestOperation addAcceptableContentTypes:contentType];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        id result=[JSON objectForKey:@"root"];
        if (![result isKindOfClass:[NSArray class]]) {
            [HUD hide:YES];
            [arraytableData removeAllObjects];
            [tableViewSellerDetails reloadData];
        }
        else
        {
            for(int j=0;j<[result count];j++)
            {
                NSDictionary *dictionary = [result objectAtIndex:j];
                ETSellerDetails *sellerDetails=[[ETSellerDetails alloc]init];
                sellerDetails.stringPosterId=[dictionary objectForKey:@"user_id"];
                sellerDetails.stringTicketId=[dictionary objectForKey:@"ticket_id"];
                sellerDetails.stringAdTitle=[dictionary objectForKey:@"ticket_title"];
                sellerDetails.stringDistance=[dictionary objectForKey:@"distance"];
                sellerDetails.stringPosterDescription=[dictionary objectForKey:@"ticket_description"];
                sellerDetails.stringPosterCurrentLocation=[dictionary objectForKey:@"current_location"];
                sellerDetails.stringPosterSetLocation=[dictionary objectForKey:@"select_location"];
                sellerDetails.stringPosterName=[dictionary objectForKey:@"buyer_name"];
                sellerDetails.stringPosterPhoneNo=[dictionary objectForKey:@"buyer_phone"];
                sellerDetails.stringPosterDate=[dictionary objectForKey:@"date_time"];
                sellerDetails.stringPosterImage=[dictionary objectForKey:@"buyer_profile_pic"];
                sellerDetails.stringTypeOfTicket=[dictionary objectForKey:@"type"];
                sellerDetails.stringZipCode=[dictionary objectForKey:@"zipcode"];
                sellerDetails.stringFavourite=[dictionary objectForKey:@"favourite"];
                valueForBuySellAll=FAVOURITESFINDAFTERPARSEBUY;
                [arraySellerDetails addObject:sellerDetails];
            }
            [arraytableData removeAllObjects];
            [arraytableData addObjectsFromArray:arraySellerDetails];
            [HUD hide:YES];
            [tableViewSellerDetails reloadData];
        }
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        UIAlertView *temp=[[UIAlertView alloc]initWithTitle:@"Message!" message:@"No response from webserver" delegate:self
                                          cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [temp show];
        [HUD hide:YES];
    }];
    [operation start];
    segmentControlForSellAndBuy.userInteractionEnabled=YES;
}
-(void)getParsedDetailsForAll{
   [arraySellerDetails removeAllObjects];
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.mode = MBProgressHUDModeAnnularDeterminate;
    HUD.progress = 0.01f;
    [HUD show:YES];
    //http://extraticketapp.com/extraticket/list_tickets.php?user_id=1
    ETLoggedUserDetails *loggedUserDetails=[ETLoggedUserDetails sharedObject];
    NSString *pathParameters = [NSString stringWithFormat:@"http://extraticketapp.com/extraticket/list_tickets.php?user_id=%@",loggedUserDetails.stringUserID];
    NSURL *url = [[NSURL alloc] initWithString:pathParameters];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10];
    NSSet *contentType = [[NSSet alloc] initWithObjects:@"text/plain",@"text/html", nil];
    [AFJSONRequestOperation addAcceptableContentTypes:contentType];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"Bajirao : %@", (NSString *) JSON);
        id result=[JSON objectForKey:@"root"];
        if (![result isKindOfClass:[NSArray class]]) {
            [HUD hide:YES];
            [arraytableData removeAllObjects];
        }
        else
        {
            for(int j=0;j<[result count];j++)
            {
                NSDictionary *dictionary = [result objectAtIndex:j];
                ETSellerDetails *sellerDetails=[[ETSellerDetails alloc]init];
                sellerDetails.stringPosterId=[dictionary objectForKey:@"user_id"];
                sellerDetails.stringTicketId=[dictionary objectForKey:@"ticket_id"];
                sellerDetails.stringAdTitle=[dictionary objectForKey:@"ticket_title"];
                sellerDetails.stringDistance=[dictionary objectForKey:@"distance"];
                sellerDetails.stringPosterDescription=[dictionary objectForKey:@"ticket_description"];
                sellerDetails.stringPosterCurrentLocation=[dictionary objectForKey:@"current_location"];
                sellerDetails.stringPosterSetLocation=[dictionary objectForKey:@"select_location"];
                sellerDetails.stringPosterName=[dictionary objectForKey:@"seller_name"];
                sellerDetails.stringPosterPhoneNo=[dictionary objectForKey:@"seller_phone"];
                sellerDetails.stringPosterDate=[dictionary objectForKey:@"date_time"];
                sellerDetails.stringPosterImage=[dictionary objectForKey:@"seller_profile_pic"];
                sellerDetails.stringTypeOfTicket=[dictionary objectForKey:@"type"];
                sellerDetails.stringZipCode=[dictionary objectForKey:@"zipcode"];
                sellerDetails.stringFavourite=[dictionary objectForKey:@"favourite"];
                valueForBuySellAll=FAVOURITESFINDAFTERPARSEALL;
               [arraySellerDetails addObject:sellerDetails];
            }
            [arraytableData removeAllObjects];
            [arraytableData addObjectsFromArray:arraySellerDetails];
            [HUD hide:YES];
            [tableViewSellerDetails reloadData];

        }
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        UIAlertView *temp=[[UIAlertView alloc]initWithTitle:@"Message!" message:@"No response from webserver" delegate:self
                                          cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [temp show];
        [HUD hide:YES];
    }];
    [operation start];
    segmentControlForSellAndBuy.userInteractionEnabled=YES;
}

#pragma mark - Tableview datasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 150;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [arraytableData count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
//   NSString *cellIdentifier = [NSString stringWithFormat:@"%d_%d",indexPath.section,indexPath.row];
//    ETCommonTableCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
//    
//   if (cell == nil)
//    {
//        cell = [[ETCommonTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
//   }
//    
//    ETSellerDetails *sellerDetails=[arraytableData objectAtIndex:indexPath.row];
//    [cell setSellerDetails:sellerDetails];

     
    
    static NSString *CellIdentifier = @"HomeTableCell";
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    //UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    if (cell==nil) {
        NSArray *nibs=[[NSBundle mainBundle] loadNibNamed:@"ETHomeTableCell" owner:nil options:nil];
        if ([nibs count]>0) {
            cell=[nibs objectAtIndex:0];
        }
    }

    ETSellerDetails *sellerDetails=[arraytableData objectAtIndex:indexPath.row];
    UITextView *textViewDocument=(UITextView *)[cell viewWithTag:1];
    [textViewDocument setText:sellerDetails.stringAdTitle];
    UIImageView *imageViewProfile=(UIImageView *)[cell viewWithTag:2];
    NSString *imagePath=[NSString stringWithFormat:@"http://extraticketapp.com/extraticket/images/%@",sellerDetails.stringPosterImage];
    UIImage *placeHolder=[UIImage imageNamed:@"profilepic.png"];
    [imageViewProfile setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:placeHolder];
//    [imageViewProfile setImageWithURL:[NSURL URLWithString:imagePath]
//              placeholderImage:placeHolder
//                       options:SDWebImageProgressiveDownload];
    UILabel *labelName=(UILabel *)[cell viewWithTag:3];
    [labelName setText:sellerDetails.stringPosterName];
    UILabel *labelPhoneNo=(UILabel *)[cell viewWithTag:4];
    [labelPhoneNo setText:sellerDetails.stringPosterPhoneNo];
    UILabel *labelDate=(UILabel *)[cell viewWithTag:5];
    [labelDate setText:sellerDetails.stringPosterDate];
    UIButton *buttonEdit=(UIButton *)[cell viewWithTag:6];
    [buttonEdit setHidden:TRUE];
    buttonFavourites=(UIButton *)[cell viewWithTag:11];
    [buttonFavourites addTarget:self action:@selector(setFavouriteAction:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *buttonPhone=(UIButton *)[cell viewWithTag:12];
    [buttonPhone addTarget:self action:@selector(makePhoneCallAction:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *buttonLocation=(UIButton *)[cell viewWithTag:13];
     UILabel *labelDistanceMiles=(UILabel *)[cell viewWithTag:15];

    if ([sellerDetails.stringDistance isKindOfClass:[NSNull class]])
    {
        [labelDistanceMiles setText:@"No data"];
    }
    else if ([sellerDetails.stringDistance isEqualToString:@"0"]) {
      [labelDistanceMiles setText:sellerDetails.stringDistance];
    }
    else{
        NSString  *mystr=[sellerDetails.stringDistance substringToIndex:3];
        [labelDistanceMiles setText:mystr];
    }
   
    [buttonLocation addTarget:self action:@selector(getLocation:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *buttonAdDetails=(UIButton *)[cell viewWithTag:14];
    [buttonAdDetails addTarget:self action:@selector(showDetailsAdAction:) forControlEvents:UIControlEventTouchUpInside];
    UILabel *labelType=(UILabel *)[cell viewWithTag:17];
    [labelType setText:[NSString stringWithFormat:@"(%@)",sellerDetails.stringTypeOfTicket]];
    UILabel *labelZipCode=(UILabel *)[cell viewWithTag:18];
    [labelZipCode setText:sellerDetails.stringZipCode];
    UILabel *labelMessageCount=(UILabel *)[cell viewWithTag:19];
    [labelMessageCount setHidden:TRUE];
    UIImageView *imgView=(UIImageView *)[cell viewWithTag:20];
    [imgView setHidden:TRUE];
    
    if ([sellerDetails.stringFavourite isEqualToString:@"yes"]) {
      [buttonFavourites setBackgroundImage:[UIImage imageNamed:@"FavoritesYellow.png"] forState:UIControlStateNormal];
    }
    else{
      [buttonFavourites setBackgroundImage:[UIImage imageNamed:@"Favorites.png"] forState:UIControlStateNormal];
    }
    return cell;
}
-(void)getLocation:(UIButton *)sender{
    UITableViewCell *cell;
    NSIndexPath *indexPath;
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // Load resources for iOS 6.1 or earlier
        cell  = (UITableViewCell *)[sender.superview superview];
        indexPath= [tableViewSellerDetails indexPathForCell:cell];
    } else {
        // Load resources for iOS 7 or later
        cell  = (UITableViewCell *)[[sender.superview superview] superview];
        indexPath = [tableViewSellerDetails indexPathForCell:cell];
    }
    ETAdLocationViewController *locationViewController=[[ETAdLocationViewController alloc]initWithNibName:@"ETAdLocationViewController" bundle:nil];
    locationViewController.sellerAdLocationDetails=[arraytableData objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:locationViewController animated:YES];
}
-(void)showDetailsAdAction:(UIButton *)sender{
    UITableViewCell *cell;
    NSIndexPath *indexPath;
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // Load resources for iOS 6.1 or earlier
        cell  = (UITableViewCell *)[sender.superview superview];
        indexPath= [tableViewSellerDetails indexPathForCell:cell];
    } else {
        // Load resources for iOS 7 or later
        cell  = (UITableViewCell *)[[sender.superview superview] superview];
        indexPath = [tableViewSellerDetails indexPathForCell:cell];
    }
    ETAdDetailViewController *adDetailViewController=[[ETAdDetailViewController alloc]initWithNibName:@"ETAdDetailViewController" bundle:nil];
    adDetailViewController.sellerAdDetails=[arraytableData objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:adDetailViewController animated:YES];
}
-(void)makePhoneCallAction:(UIButton *)sender{
    UITableViewCell *cell;
    NSIndexPath *indexPath;
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // Load resources for iOS 6.1 or earlier
        cell  = (UITableViewCell *)[sender.superview superview];
        indexPath= [tableViewSellerDetails indexPathForCell:cell];
    } else {
        // Load resources for iOS 7 or later
        cell  = (UITableViewCell *)[[sender.superview superview] superview];
        indexPath = [tableViewSellerDetails indexPathForCell:cell];
    }
    sellerDetailsToMakePhoneCall=[arraytableData objectAtIndex:indexPath.row];
    UIAlertView *myAlert=[[UIAlertView alloc]initWithTitle:@"Make Call" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    myAlert.tag=1;
    [myAlert show];
 }
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==1) {
        if (buttonIndex==1) {
            NSString *phoneNumber = [@"tel://" stringByAppendingString:sellerDetailsToMakePhoneCall.stringPosterPhoneNo];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];        }
    }
}
-(void)setFavouriteAction:(UIButton *)sender{
    UITableViewCell *cell;
    NSIndexPath *indexPath;
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // Load resources for iOS 6.1 or earlier
        cell  = (UITableViewCell *)[sender.superview superview];
        indexPath= [tableViewSellerDetails indexPathForCell:cell];
    } else {
        // Load resources for iOS 7 or later
        cell  = (UITableViewCell *)[[sender.superview superview] superview];
        indexPath = [tableViewSellerDetails indexPathForCell:cell];
    }
    
    ETSellerDetails *sellerDetails=[arraytableData objectAtIndex:indexPath.row];
    ETLoggedUserDetails *loggedUserDetails=[ETLoggedUserDetails sharedObject];
    NSString *pathParameters = [NSString stringWithFormat:@"http://extraticketapp.com/extraticket/set_favourite.php?ticket_id=%@&user_id=%@",sellerDetails.stringTicketId,loggedUserDetails.stringUserID];
    pathParameters = [pathParameters stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [[NSURL alloc] initWithString:pathParameters];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10];
    //NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLCacheStorageNotAllowed timeoutInterval:20];
    NSSet *contentType = [[NSSet alloc] initWithObjects:@"text/plain",@"text/html", nil];
    [AFJSONRequestOperation addAcceptableContentTypes:contentType];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        if ([[JSON objectForKey:@"root"]isEqualToString:@"Set as favourite"]) {
          //  [buttonFavourites setBackgroundImage:[UIImage imageNamed:@"FavoritesYellow.png"] forState:UIControlStateNormal];
            if (valueForBuySellAll==FAVOURITESFINDAFTERPARSEALL) {
                 [self getParsedDetailsForAll];
            }else if (valueForBuySellAll==FAVOURITESFINDAFTERPARSEBUY){
                [self getParsedDetailsForBuyer];
            }else if (valueForBuySellAll==FAVOURITESFINDAFTERPARSESELL){
               [self getParsedDetailsForSeller];
            }
           
        }
        else if ([[JSON objectForKey:@"root"]isEqualToString:@"Unset"]){
           // [buttonFavourites setBackgroundImage:[UIImage imageNamed:@"Favorites.png"] forState:UIControlStateNormal];
            if (valueForBuySellAll==FAVOURITESFINDAFTERPARSEALL) {
                [self getParsedDetailsForAll];
            }else if (valueForBuySellAll==FAVOURITESFINDAFTERPARSEBUY){
                [self getParsedDetailsForBuyer];
            }else if (valueForBuySellAll==FAVOURITESFINDAFTERPARSESELL){
                [self getParsedDetailsForSeller];
            }
        }
    }
                                                                                        failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                                                            UIAlertView *temp=[[UIAlertView alloc]initWithTitle:@"Message!" message:@"No response from webserver" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                                                                            [temp show];
                                                                                        }];
    [operation start];
}




  /*  if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // Load resources for iOS 6.1 or earlier
        cell  = (UITableViewCell *)[sender.superview superview];
        indexPath= [tableViewSellerDetails indexPathForCell:cell];
    } else {
        // Load resources for iOS 7 or later
        cell  = (UITableViewCell *)[[sender.superview superview] superview];
        indexPath = [tableViewSellerDetails indexPathForCell:cell];
    }
    ETChatViewController *chatViewController=[[ETChatViewController alloc]initWithNibName:@"ETChatViewController" bundle:nil];
    chatViewController.sellerFromMyTicketsForChat=[arraySellerDetails objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:chatViewController animated:YES];*/
- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // No editing style if not editing or the index path is nil.
    if (self.editing == NO || !indexPath )
        return UITableViewCellEditingStyleNone;
   
    else
	{
		return UITableViewCellEditingStyleDelete;
	}
    return UITableViewCellEditingStyleNone;
}

//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
//    return YES;
//}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {//http://extraticketapp.com/extraticket/block_tickets.php?user_id=1&ticket_id=2
        ETLoggedUserDetails *loggedUserDetails=[ETLoggedUserDetails sharedObject];
        ETSellerDetails *sellerDetails=[arraytableData objectAtIndex:indexPath.row];
        NSString *pathParameters = [NSString stringWithFormat:@"http://extraticketapp.com/extraticket/block_tickets.php?user_id=%@&ticket_id=%@",loggedUserDetails.stringUserID,sellerDetails.stringTicketId];
        pathParameters = [pathParameters stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [[NSURL alloc] initWithString:pathParameters];
        // NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:20
                                 ];
        NSSet *contentType = [[NSSet alloc] initWithObjects:@"text/plain",@"text/html", nil];
        [AFJSONRequestOperation addAcceptableContentTypes:contentType];
        AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
            if ([[JSON objectForKey:@"root"]isEqualToString:@"Blocked"]) {
                [arraytableData removeObjectAtIndex:indexPath.row];
                [tableViewSellerDetails reloadData];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"This ticket have been deleted successfully" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
            else{
                NSLog(@"Not deleted!!!");
                
            }
        }
                                                                                            failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                                                                UIAlertView *temp=[[UIAlertView alloc]initWithTitle:@"Message!" message:@"No response from webserver" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                                                                                [temp show];
                                                                                            }];
        [operation start];
    }
    
}

#pragma mark UIButtonActions

-(IBAction)valueChangedForTypeOfParties:(id)sender{
    if (segmentControlForSellAndBuy.selectedSegmentIndex==0) {
        segmentControlForSellAndBuy.userInteractionEnabled=NO;
        [self getParsedDetailsForAll];
    }
    else if (segmentControlForSellAndBuy.selectedSegmentIndex==1)
    {
        segmentControlForSellAndBuy.userInteractionEnabled=NO;
        [self getParsedDetailsForSeller];
    }
    else if (segmentControlForSellAndBuy.selectedSegmentIndex==2)
    {
        segmentControlForSellAndBuy.userInteractionEnabled=NO;
        [self getParsedDetailsForBuyer];
    }
}

#pragma mark SearchBar Delegates
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    // only show the status bar's cancel button while in edit mode
    searchBarForTable.showsCancelButton = YES;
    searchBarForTable.autocorrectionType = UITextAutocorrectionTypeNo;
    // flush the previous search content
    
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    searchBarForTable.showsCancelButton = NO;
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if([searchText isEqualToString:@""]||searchText==nil){
        [arraytableData removeAllObjects];
        //[arraytableData addObjectsFromArray:result];
        [tableViewSellerDetails reloadData];
        return;
    }
    [arraytableData removeAllObjects];
    NSString *searchStrinng = [searchText lowercaseString];
    for (ETSellerDetails *sellItem in arraySellerDetails) {
        if ([[sellItem.stringAdTitle lowercaseString] rangeOfString:searchStrinng].location != NSNotFound) {
            [arraytableData addObject:sellItem];
        }
    }
    
    [tableViewSellerDetails reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    searchBarForTable.text=@"";
    [searchBarForTable resignFirstResponder];
    [arraytableData removeAllObjects];
    [arraytableData addObjectsFromArray:arraySellerDetails];
    [tableViewSellerDetails reloadData];
}
// called when Search (in our case “Done”) button pressed
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [arraytableData removeAllObjects];
    NSString *searchStrinng = [searchBar.text lowercaseString];
    for (ETSellerDetails *sellItem in arraySellerDetails) {
        if ([[sellItem.stringAdTitle lowercaseString] rangeOfString:searchStrinng].location != NSNotFound) {
            [arraytableData addObject:sellItem];
        }
    }
    [tableViewSellerDetails reloadData];
    [searchBar resignFirstResponder];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
