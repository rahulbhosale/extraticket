//
//  ETLoggedUserDetails.h
//  ExtraTicket
//
//  Created by mac on 10/15/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ETLoggedUserDetails : NSObject{
    
}
@property(nonatomic,strong)NSString *stringUserID;
@property(nonatomic,strong)NSString *stringUserName;
@property(nonatomic,strong)NSString *stringUserEmail;
@property(nonatomic,strong)NSString *stringUserPhoneNumber;
@property(nonatomic,strong)NSString *stringBuyerOrSeller;
@property(nonatomic,strong)NSString *stringProfilePic;
@property(nonatomic,strong)NSString *stringPassword;
@property(nonatomic,strong)NSString *stringMessageCount;

+(id)sharedObject;

@end
