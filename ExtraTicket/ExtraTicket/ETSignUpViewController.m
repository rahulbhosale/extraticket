//
//  ETSignUpViewController.m
//  ExtraTicket
//
//  Created by mac on 10/15/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import "ETSignUpViewController.h"
#import "MBProgressHUD.h"
#import "AFNetworking.h"
#import "AFHTTPClient.h"
#import "ETLoggedUserDetails.h"
#import "ETHomeViewController.h"
#import "ETAppDelegate.h"
#import <CoreLocation/CoreLocation.h>
#import <QuartzCore/QuartzCore.h>
#import "UIImage+Extensions.h"
#import "UIImage+Resize.h"
#import "ETTermsAndConditionsViewController.h"
@interface ETSignUpViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,MBProgressHUDDelegate,CLLocationManagerDelegate,sendAcceptDelegate>{
    MBProgressHUD *HUD;
    NSString *valueBuyerOrSeller;
    int intValueBuyerOrSeller;
    int intValueForProfilePhoto;
    int termsAndConditions;

    CLLocationManager *locationManager;
    NSString *latitudeString;
    NSString *longitudeString;
}

@end

@implementation ETSignUpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     isSelectedForTermsAndConditions=YES;
    UIColor *_red=[UIColor brownColor];

    // Add attribute NSUnderlineStyleAttributeName
       NSMutableAttributedString *btnTitle=[[NSMutableAttributedString alloc] initWithString:@"Terms & Conditions"];
    
    [btnTitle addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:NSUnderlineStyleSingle] range:NSMakeRange(0, btnTitle.length)];
    [btnTitle addAttribute:NSForegroundColorAttributeName value:_red range:NSMakeRange(0, btnTitle.length)];

    [self.btnTermsAndConditions setAttributedTitle:btnTitle forState:UIControlStateNormal];
    float systemVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (systemVersion >= 7.0) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }

    
    locationManager = [[CLLocationManager alloc] init];
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationItem.title=@"Create Account";
    UITapGestureRecognizer *tapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(uploadProfilePicture:)];
    tapGestureRecognizer.numberOfTapsRequired=1;
    [imageViewProficPic addGestureRecognizer:tapGestureRecognizer];
    // Do any additional setup after loading the view from its nib.
}

-(void)gettingCurrentLocation{
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    [locationManager startUpdatingLocation];
}
#pragma mark- CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    [locationManager stopUpdatingLocation];
    locationManager.delegate = nil;
    CLLocation *currentLocation = newLocation;
    if (currentLocation != nil) {
    }
    float degrees = newLocation.coordinate.latitude;
    double decimal = fabs(newLocation.coordinate.latitude - degrees);
    int minutes = decimal * 60;
    double seconds = decimal * 3600 - minutes * 60;
    degrees = newLocation.coordinate.longitude;
    decimal = fabs(newLocation.coordinate.longitude - degrees);
    minutes = decimal * 60;
    seconds = decimal * 3600 - minutes * 60;
    longitudeString =[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
    latitudeString = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
    [self performSignUpAction];
}
-(void)performSignUpAction{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.mode = MBProgressHUDModeAnnularDeterminate;
    HUD.progress = 0.01f;
    [HUD show:YES];
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
   if ([textFieldUserName.text isEqualToString:@""] || [textFieldEmailID.text isEqualToString:@""]||[textFieldPhoneNumber.text isEqualToString:@""]||[textFieldPassword.text isEqualToString:@""]||[textFieldConfirmPassword.text isEqualToString:@""] ||[textFieldZipCode.text isEqualToString:@""]) {
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Some fields are missing" message:@"Please Enter all your fields" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertView show];
        [HUD hide:YES];
        
    }
    else if (intValueForProfilePhoto!=1){
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:Nil message:@"Please Choose your profile picture" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertView show];
        [HUD hide:YES];
    }
    else if (isSelectedForTermsAndConditions){
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:Nil message:@"Please accept the terms and conditions!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertView show];
        [HUD hide:YES];
    }
    else if (![textFieldPassword.text isEqualToString:textFieldConfirmPassword.text]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:Nil message:@"Password must be same!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [HUD hide:YES];
    }
    else if([emailTest evaluateWithObject:textFieldEmailID.text] == NO)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"You Entered Incorrect Email ID." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        [HUD hide:YES];
        return;
    }else if ([textFieldZipCode.text length]!=5){
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:Nil message:@"ZipCode must contain 5 digits!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertView show];
        [HUD hide:YES];
    }
    else
    {
    
        //http://extraticketapp.com/extraticket/sign_up.php?username=sru&email=sru@gmail.com&password=b&phone_num=11&image=aa.jpg&latitude=9.036461200000000000&longitude=76.623942299999950000&zipcode=123123123&device_token=222222222222222222
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *stringDevToken = [prefs valueForKey:@"token"];
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];//type=buyer
        [parameters setObject:textFieldUserName.text forKey:@"username"];
        [parameters setObject:textFieldEmailID.text forKey:@"email"];
        [parameters setObject:textFieldPassword.text forKey:@"password"];
        [parameters setObject:textFieldPhoneNumber.text forKey:@"phone_num"];
        [parameters setObject:latitudeString forKey:@"latitude"];
        [parameters setObject:longitudeString forKey:@"longitude"];
        [parameters setObject:textFieldZipCode.text forKey:@"zipcode"];
        [parameters setObject:stringDevToken forKey:@"device_token"];
        //[parameters setObject:textFieldEmailID.text forKey:@"type"];
        NSURL *url = [NSURL URLWithString:@"http://extraticketapp.com/extraticket/"];
       
        
//        CGFloat compression = 0.9f;
//        CGFloat maxCompression = 0.1f;
//        int maxFileSize = 250*1024;
//        
//        pngData = UIImageJPEGRepresentation(imageViewProficPic.image, compression);
//        
//        while ([pngData length] > maxFileSize && compression > maxCompression)
//        {
//            compression -= 0.1;
//            pngData = UIImageJPEGRepresentation(imageViewProficPic.image, compression);
//        }
//        
        
        pngData=UIImageJPEGRepresentation(imageViewProficPic.image, 0.6);
        
        AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
        NSMutableURLRequest *afRequest = [httpClient multipartFormRequestWithMethod:@"POST"
                                                                               path:@"sign_up.php"
                                                                         parameters:parameters
                                                          constructingBodyWithBlock:^(id <AFMultipartFormData>formData)
                                          {
                                              [formData appendPartWithFileData:pngData
                                                                          name:@"image"
                                                                      fileName:[NSString stringWithFormat:@"%@.jpg",textFieldEmailID.text]
                                                                      mimeType:@"image/jpeg"];
                                              
                                          }
                                          ];
        AFHTTPRequestOperation *operation =
        [httpClient HTTPRequestOperationWithRequest:afRequest
                                            success:^(AFHTTPRequestOperation *operation, id json) {
                                                
                                                NSData *resultData=(NSData *)json;
                                                NSDictionary *result = [NSJSONSerialization JSONObjectWithData:resultData options:NSJSONReadingAllowFragments error:nil];
                                                if ([[[result objectForKey:@"root"]objectForKey:@"Result"]isEqualToString:@"Email already exist"]) {
                                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Your Registration failed.Try with another Email." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                    [alert show];
                                                    [HUD hide:YES];
                                                }
                                                else if ([[[result objectForKey:@"root"]objectForKey:@"Result"]isEqualToString:@"Inserted"]){
                                                    ETLoggedUserDetails *loggedUserDetails=[ETLoggedUserDetails sharedObject];
                                                    
                                                    loggedUserDetails.stringUserID=[[[result objectForKey:@"root"]objectForKey:@"Details"] objectForKey:@"id"];
                                                    loggedUserDetails.stringUserName=[[[result objectForKey:@"root"]objectForKey:@"Details"] objectForKey:@"username"];
                                                    loggedUserDetails.stringUserEmail=[[[result objectForKey:@"root"]objectForKey:@"Details"] objectForKey:@"emailid"];
                                                    loggedUserDetails.stringBuyerOrSeller=[[[result objectForKey:@"root"]objectForKey:@"Details"] objectForKey:@"type"];
                                                    loggedUserDetails.stringUserPhoneNumber=[[[result objectForKey:@"root"]objectForKey:@"Details"] objectForKey:@"phone_num"];
                                                    loggedUserDetails.stringProfilePic=[[[result objectForKey:@"root"]objectForKey:@"Details"] objectForKey:@"profile_pic"];
                                                    loggedUserDetails.stringPassword=[[[result objectForKey:@"root"]objectForKey:@"Details"] objectForKey:@"password"];
                                                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"LoggedIn"];
                                                    [[NSUserDefaults standardUserDefaults]setObject:textFieldEmailID.text forKey:@"Email"];
                                                    [[NSUserDefaults standardUserDefaults]setObject:textFieldPassword.text forKey:@"Password"];
                                                    [[NSUserDefaults standardUserDefaults]synchronize];
                                                   
                                                   ETAppDelegate *appDelegate=(ETAppDelegate *)[[UIApplication sharedApplication]delegate];
                                                    [appDelegate setLogin];
                                                    
                                                }
                                            }
                                            failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message!" message:@"Connection Error,No response from web servies!!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                [alert show];
                                                [HUD hide:YES];
                                            }];
        [httpClient enqueueHTTPRequestOperation:operation];
    }
}
-(void)uploadProfilePicture:(id)sender{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Choose Image Source"delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: @"Take Photo",@"Choose Photo",nil];
    [alert show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    UIImagePickerController *picker=[[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    if (buttonIndex == 1){
        if (![UIImagePickerController isCameraDeviceAvailable: UIImagePickerControllerCameraDeviceRear])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Camera Available" message:@"his Feature requires camera"  delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil];
            [alert show];
        }
        else{
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:picker animated:YES completion:Nil];
            [picker setAllowsEditing:NO];
            
        }
    }
    else if (buttonIndex == 2){
        picker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:Nil];
        [picker setAllowsEditing:NO];
    }
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage : (UIImage *)image editingInfo:(NSDictionary *)editingInfo{
    intValueForProfilePhoto=1;
    imageViewProficPic.image=[self imageWithImage:image convertToSize:CGSizeMake(58, 60)];
    
    
    
  //karthi coding technique
/*    dispatch_queue_t bgQ = dispatch_queue_create("bgQ", 0);
    
    dispatch_async(bgQ, ^
    {
        
        UIImage *originalImage = image;
        
        if (!originalImage)
            return;
        
           // Optionally set a placeholder image here while resizing happens in background
        
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            // Set desired maximum height and calculate width
            CGFloat height = 640.0f;  // or whatever you need
            CGFloat width = (height / self.view.frame.size.height) * self.view.frame.size.width;
            
            // Resize the image
            UIImage * imageResized = [originalImage resizedImage:CGSizeMake(width, height) interpolationQuality:kCGInterpolationHigh];
            
            // Optionally save the image here...
           imageViewProficPic.image=imageResized;
        });
    });*/
   
    [picker dismissViewControllerAnimated:YES completion:nil];
}
- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}
-(void)sendAccept :(NSString *)acceptTermsAndConditions{
    if ([acceptTermsAndConditions isEqualToString:@"accept"]) {
        isSelectedForTermsAndConditions=NO;
        termsAndConditions=1;
        [buttonTickMe setBackgroundImage:[UIImage imageNamed:@"cb_dark_on.png"] forState:UIControlStateNormal];
    }
}


#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark UIButtonActions
-(IBAction)didTickMarkChosenTermsAndConditions:(id)sender{
    if (isSelectedForTermsAndConditions) {
        isSelectedForTermsAndConditions=NO;
        termsAndConditions=1;
        [buttonTickMe setBackgroundImage:[UIImage imageNamed:@"cb_dark_on.png"] forState:UIControlStateNormal];
    }
    else{
        [buttonTickMe setBackgroundImage:[UIImage imageNamed:@"cb_dark_off.png"] forState:UIControlStateNormal];
        isSelectedForTermsAndConditions=YES;
    }
}
-(IBAction)didSelectingTermsAndConditions:(id)sender{
    ETTermsAndConditionsViewController *termsAndConditionsViewController=[[ETTermsAndConditionsViewController alloc]initWithNibName:@"ETTermsAndConditionsViewController" bundle:Nil];
    [termsAndConditionsViewController setDelegate:self];
    [self.navigationController presentViewController:termsAndConditionsViewController animated:YES completion:Nil];
}
-(IBAction)didChoosingBuyerAndSellerAction:(id)sender{
    if ([sender tag]==1) {
        valueBuyerOrSeller=@"Buyer";
        intValueBuyerOrSeller=1;
        [buttonSelectBuyer setBackgroundImage:[UIImage imageNamed:@"cb_dark_on.png"] forState:UIControlStateNormal];
        [buttonSelectSeller setBackgroundImage:[UIImage imageNamed:@"cb_dark_off.png"] forState:UIControlStateNormal];
        
    }else if ([sender tag]==2){
        valueBuyerOrSeller=@"Seller";
        intValueBuyerOrSeller=1;
        [buttonSelectBuyer setBackgroundImage:[UIImage imageNamed:@"cb_dark_off.png"] forState:UIControlStateNormal];
        [buttonSelectSeller setBackgroundImage:[UIImage imageNamed:@"cb_dark_on.png"] forState:UIControlStateNormal];
    }
}
-(IBAction)didRegisteringAction:(id)sender{
    [self gettingCurrentLocation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
