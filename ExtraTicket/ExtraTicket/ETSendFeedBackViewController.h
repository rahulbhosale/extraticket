//
//  ETSendFeedBackViewController.h
//  ExtraTicket
//
//  Created by mac on 10/16/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>


@interface ETSendFeedBackViewController : UIViewController<MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate>{
    NSString *message;
    IBOutlet UITextView *textViewMessageContent;
    
    IBOutlet UITextField *textFieldEmail1;
    IBOutlet UITextField *textFieldEmail2;
    IBOutlet UITextField *textFieldEmail3;
    IBOutlet UITextField *textFieldEmail4;
    IBOutlet UITextField *textFieldEmail5;
 
}
-(IBAction)sendMail:(id)sender;
@end
