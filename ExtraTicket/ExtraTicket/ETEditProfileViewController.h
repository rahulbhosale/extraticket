//
//  ETEditProfileViewController.h
//  ExtraTicket
//
//  Created by mac on 10/18/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ETEditProfileViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>{
    IBOutlet UITextField *textFieldUserName;
    IBOutlet UITextField *textFieldEmailID;
    IBOutlet UITextField *textFieldPhoneNumber;
    IBOutlet UITextField *textFieldPassword;
    IBOutlet UITextField *textFieldConfirmPassword;
    IBOutlet UITextField *textFieldZipCode;
    IBOutlet UIImageView *imageViewProficPic;
    IBOutlet UISwitch *switchNotification;
    
    NSData *pngData;
}
-(IBAction)didUpdationAction:(id)sender;
-(IBAction)switchNotification:(id)sender;
@end
