//
//  main.m
//  ExtraTicket
//
//  Created by mac on 10/15/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ETAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ETAppDelegate class]));
    }
}
