//
//  SPHChatData.h
//  ChatBubble
//
//  Created by ivmac on 10/2/13.
//  Copyright (c) 2013 Conciergist. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SPHChatData : NSObject

//http://extraticketapp.com/extraticket/insert_msg.php?from_userid=%@&to_userid=%@&root_id=%@&message=%@&date_time=%@&ticket_id=%@&select_location=%@&current_location=%@

@property(nonatomic,retain)NSString *messageText;
@property(nonatomic,retain)NSString *avatarImageURL;
@property(nonatomic,retain)NSString *messageImageURL;
@property(nonatomic,retain)NSString *messageTime;
@property(nonatomic,retain)NSString *bubbleImageName;
@property(nonatomic,retain)NSString *messageType;
@property(nonatomic,retain)NSString *messagestatus;
@property(nonatomic,retain)NSString *messagesfrom;
@property(nonatomic,retain)UIImage *messageImage;


//@property(nonatomic,retain)NSString *messageText;
//@property(nonatomic,retain)NSString *avatarImageURL;
//@property(nonatomic,retain)NSString *messageImageURL;
//@property(nonatomic,retain)NSString *messageTime;
//@property(nonatomic,retain)NSString *bubbleImageName;
//@property(nonatomic,retain)NSString *messageType;
//@property(nonatomic,retain)NSString *messagestatus;
//@property(nonatomic,retain)NSString *messagesfrom;
//@property(nonatomic,retain)UIImage *messageImage;

@end
