//
//  ETCommonTableViewCell.h
//  ExtraTicket
//
//  Created by mac on 10/15/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ETCommonTableViewCell : UITableViewCell{
    
}
@property(strong,nonatomic)UITextField *textFieldLogin;
@property(strong,nonatomic)UIButton *buttonLogIn;
-(id)initWithLogIn:(NSString *)text;
@end
