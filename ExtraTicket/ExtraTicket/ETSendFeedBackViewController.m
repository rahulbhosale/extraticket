//
//  ETSendFeedBackViewController.m
//  ExtraTicket
//
//  Created by mac on 10/16/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import "ETSendFeedBackViewController.h"
#import "MFSideMenuContainerViewController.h"
#import "ETAppDelegate.h"
@interface ETSendFeedBackViewController (){
    
}

@end

@implementation ETSendFeedBackViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupMenuBarButtonItems];
//    UIBarButtonItem *leftBar=[[UIBarButtonItem alloc]initWithTitle:@"Log Out" style:UIBarButtonItemStylePlain target:self action:@selector(logoutAction)];
//    self.navigationItem.rightBarButtonItem=leftBar;
    // Do any additional setup after loading the view from its nib.
}
-(void)logoutAction{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"LoggedIn"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    ETAppDelegate *appDelegate=(ETAppDelegate *)[[UIApplication sharedApplication]delegate];
    [appDelegate setFirstlyLogin];
}

#pragma  mark-split view controller
- (MFSideMenuContainerViewController *)menuContainerViewController {
    return (MFSideMenuContainerViewController *)self.navigationController.parentViewController;
}
#pragma mark - UIBarButtonItems

- (void)setupMenuBarButtonItems
{
    UIButton *leftBarButtonItem = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftBarButtonItem setImage:[UIImage imageNamed:@"menu-icon.png"] forState:UIControlStateNormal];
    leftBarButtonItem.frame = CGRectMake(0, 0, 43, 28);
    [leftBarButtonItem addTarget:self action:@selector(leftSideMenuButtonPressed:) forControlEvents:
     UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftBarButtonItem];
}


#pragma mark - UIBarButtonItem Callbacks

- (void)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)leftSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^
     {
         [self setupMenuBarButtonItems];
     }];
}

- (void)rightSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleRightSideMenuCompletion:^
     {
         [self setupMenuBarButtonItems];
     }];
}

#pragma mark UIButtonActions

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            message=@"Your message has been Cancelled";
            break;
        case MessageComposeResultFailed:
            message=@"Your message failed to sent";
            
            break;
        case MessageComposeResultSent:
            message=@"Your message was sent successfully";
            break;
            
        default:
            break;
    }
    [[[UIAlertView alloc]initWithTitle:@"Message Status" message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil]show];
   [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    
	switch (result) {
            
        case MFMailComposeResultFailed:
            message=@"Your email has failed to send !";
            break;
            
        case MFMailComposeResultSent :
            message=@"Your email has sent Successfully !";
            break;
            
        case MFMailComposeResultCancelled :
            NSLog(@"cancelled");
            break;
            
        case MFMailComposeResultSaved :
            message=@"Your email has been saved to draft";
            break;
        default:
            break;
    }
    [[[UIAlertView alloc]initWithTitle:@"Mail Status" message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil]show];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(IBAction)sendMail:(id)sender
{
    NSLog(@"rdydd");
    MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
    mail.mailComposeDelegate = self;
    
    if ([MFMailComposeViewController canSendMail]) {
        
        
        NSString *str1=textFieldEmail1.text;
        NSString *str2=textFieldEmail2.text;
        NSString *str3=textFieldEmail3.text;
        NSString *str4=textFieldEmail4.text;
        NSString *str5=textFieldEmail5.text;
        
        [mail setToRecipients:[NSArray arrayWithObjects:str1,str2,str3,str4,str5,nil]];
        [mail setSubject:@"ExtraTicket Refering A Friend"];
        NSString *messageContent=textViewMessageContent.text;
        [mail setMessageBody:messageContent isHTML:NO];
        
        
        //        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        //        NSString *documentsDirectory = [paths objectAtIndex:0];
        //        NSString *stringFullPdfName=objectforSavepdf.pdfName;
        //        NSString *imagePath=[documentsDirectory stringByAppendingPathComponent:stringFullPdfName];
        //        NSArray *filePathsArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:imagePath error:nil];
        //        NSString *filePath = [imagePath stringByAppendingPathComponent:[filePathsArray objectAtIndex:0]];
        
        
        
        //Present the mail view controller
        [self presentViewController:mail animated:YES completion:nil];
        
    }
    
    
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{
	if([text isEqualToString:@"\n"])
    {
        [textViewMessageContent resignFirstResponder];
        
        return NO;
    }
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
