//
//  ETCommonTableCell.m
//  ExtraTicket
//
//  Created by mac on 12/6/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import "ETCommonTableCell.h"
#import "SDWebImage/UIImageView+WebCache.h"
@implementation ETCommonTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
     
    _imageViewFullBg=[[UIImageView alloc]initWithFrame:CGRectMake(160, 62, 324,87)];
    [_imageViewFullBg setImage:[UIImage imageNamed:@"Bg.png"]];
        
    _imageViewBgTop=[[UIImageView alloc]initWithFrame:CGRectMake(162, -5, 323,71)];
    [_imageViewBgTop setImage:[UIImage imageNamed:@"topbar.png"]];
    
    _imageViewProfilePic=[[UIImageView alloc]initWithFrame:CGRectMake(28, 3, 55,57)];
   // [_imageViewProfilePic setImage:[UIImage imageNamed:@"profilepic.png"]];
        
    _imageViewTitle=[[UIImageView alloc]initWithFrame:CGRectMake(144, 84, 268,50)];
    [_imageViewTitle setImage:[UIImage imageNamed:@"one1.png"]];
        
    _imageViewPhone=[[UIImageView alloc]initWithFrame:CGRectMake(221, 5, 26,26)];
    [_imageViewPhone setImage:[UIImage imageNamed:@"Phone.png"]];
        
    _imageViewTime=[[UIImageView alloc]initWithFrame:CGRectMake(67, 37, 26,26)];
    [_imageViewTime setImage:[UIImage imageNamed:@"time.png"]];
        
     _labelName = [[UILabel alloc] initWithFrame:CGRectMake(138,3,149, 21)];
    _labelName.backgroundColor=[UIColor clearColor];
    [_labelName setFont:[UIFont boldSystemFontOfSize:17]];
        
    _labelTypeSellOrBuy = [[UILabel alloc] initWithFrame:CGRectMake(144,20,149, 21)];
    _labelTypeSellOrBuy.backgroundColor=[UIColor clearColor];
    [_labelTypeSellOrBuy setFont:[UIFont boldSystemFontOfSize:15]];
        
    _labelPhoneNumber = [[UILabel alloc] initWithFrame:CGRectMake(280,9,91, 19)];
    _labelPhoneNumber.backgroundColor=[UIColor clearColor];
    [_labelPhoneNumber setFont:[UIFont systemFontOfSize:14]];
        
    _labelSetFavour = [[UILabel alloc] initWithFrame:CGRectMake(280,38,87, 21)];
    _labelSetFavour.backgroundColor=[UIColor clearColor];
    [_labelSetFavour setFont:[UIFont systemFontOfSize:14]];
        
    _labelMiles = [[UILabel alloc] initWithFrame:CGRectMake(300,94,39, 21)];
    _labelMiles.backgroundColor=[UIColor clearColor];
    [_labelMiles setFont:[UIFont boldSystemFontOfSize:15]];
     _labelMiles.textAlignment=NSTextAlignmentCenter;
    
    _labelTime = [[UILabel alloc] initWithFrame:CGRectMake(160,41,164, 21)];
    _labelTime.backgroundColor=[UIColor clearColor];
    [_labelTime setFont:[UIFont systemFontOfSize:13]];
    
    _labelMilesStaticText = [[UILabel alloc] initWithFrame:CGRectMake(302,113,42, 21)];
    _labelMilesStaticText.backgroundColor=[UIColor clearColor];
    [_labelMilesStaticText setFont:[UIFont boldSystemFontOfSize:15]];
    [_labelMilesStaticText setText:@"Miles"];
        
    _labelZipCode = [[UILabel alloc] initWithFrame:CGRectMake(274,66,92,16)];
    _labelZipCode.backgroundColor=[UIColor clearColor];
    [_labelZipCode setFont:[UIFont boldSystemFontOfSize:14]];
        
    _buttonMap = [[UIButton alloc] initWithFrame:CGRectMake(222,33,25,24)];
    _buttonPhone = [[UIButton alloc] initWithFrame:CGRectMake(221,6,24,22)];
    _buttonSetFavourite = [[UIButton alloc] initWithFrame:CGRectMake(299,87,34,36)];
    _buttonTitleExpansion = [[UIButton alloc] initWithFrame:CGRectMake(144,87,268, 44)];
    
    _textViewTitle = [[UITextView alloc] initWithFrame:CGRectMake(144,86,262,44)];
    [_textViewTitle setFont:[UIFont boldSystemFontOfSize:14]];
    
        
    [self.contentView addSubview:_labelName];
    //[self.contentView addSubview:_labelName];
        
        
        
    [self.contentView addSubview: _imageViewProfilePic];
        
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setSellerDetails:(ETSellerDetails *)sellerDetails
{
    NSString *imagePath=[NSString stringWithFormat:@"http://extraticketapp.com/extraticket/images/%@",sellerDetails.stringPosterImage];
    NSLog(@"imagePath iss %@",imagePath);
    UIImage *placeHolder=[UIImage imageNamed:@"profilepic.png"];
    
    //TODO : Have to comment : Baji
    [_imageViewProfilePic sd_setImageWithURL:[NSURL URLWithString:imagePath]
                     placeholderImage:placeHolder
                              options: SDWebImageRefreshCached];
    [_textViewTitle setText:sellerDetails.stringAdTitle];
    [_labelName setText:sellerDetails.stringPosterName];
    [_labelPhoneNumber setText:sellerDetails.stringPosterPhoneNo];
    [_labelTime setText:sellerDetails.stringPosterDate];
    [_labelTypeSellOrBuy setText:[NSString stringWithFormat:@"(%@)",sellerDetails.stringTypeOfTicket]];
    
    if ([sellerDetails.stringDistance isEqualToString:@"0"]) {
        [_labelMiles setText:sellerDetails.stringDistance];
    }
    else{
        NSString  *mystr=[sellerDetails.stringDistance substringToIndex:3];
        [_labelMiles setText:mystr];
    }

}
@end
