//
//  ETMyFavouritesViewController.m
//  ExtraTicket
//
//  Created by mac on 11/16/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import "ETMyFavouritesViewController.h"
#import "MBProgressHUD.h"
#import "ETLoggedUserDetails.h"
#import "AFNetworking.h"
#import "ETSellerDetails.h"
#import "UIImageView+AFNetworking.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "ETAdLocationViewController.h"
#import "ETAdDetailViewController.h"
#import "MFSideMenuContainerViewController.h"
#import "ETCommonTableCell.h"
@interface ETMyFavouritesViewController (){
    NSMutableArray *arraySellerDetails;
    MBProgressHUD *HUD;
    IBOutlet UITableView *tableViewSellerDetails;
    BOOL isSelected;
    UIButton *buttonFavourites;
}


@end

@implementation ETMyFavouritesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma  mark-split view controller
- (MFSideMenuContainerViewController *)menuContainerViewController {
    return (MFSideMenuContainerViewController *)self.navigationController.parentViewController;
}
#pragma mark - UIBarButtonItems

- (void)setupMenuBarButtonItems
{
    UIButton *leftBarButtonItem = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftBarButtonItem setImage:[UIImage imageNamed:@"menu-icon.png"] forState:UIControlStateNormal];
    leftBarButtonItem.frame = CGRectMake(0, 0, 43, 28);
    [leftBarButtonItem addTarget:self action:@selector(leftSideMenuButtonPressed:) forControlEvents:
     UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftBarButtonItem];
}


#pragma mark - UIBarButtonItem Callbacks

- (void)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)leftSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^
     {
         [self setupMenuBarButtonItems];
     }];
}

- (void)rightSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleRightSideMenuCompletion:^
     {
         [self setupMenuBarButtonItems];
     }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupMenuBarButtonItems];
    [self getParsedDetailsForSeller];
    // Do any additional setup after loading the view from its nib.
    UIBarButtonItem *rightBar=[[UIBarButtonItem alloc]initWithTitle:@"Delete" style:UIBarButtonItemStylePlain target:self action:@selector(DeleteAction)];
    self.navigationItem.rightBarButtonItem=rightBar;
}
-(void)DeleteAction{
    if(self.editing)
	{
		[super setEditing:NO animated:NO];
		[tableViewSellerDetails setEditing:NO animated:NO];
		[tableViewSellerDetails reloadData];
		self.navigationItem.rightBarButtonItem.title=@"delete";
    }
	else
    {
		[super setEditing:YES animated:YES];
		[tableViewSellerDetails setEditing:YES animated:YES];
		[tableViewSellerDetails reloadData];
        self.navigationItem.rightBarButtonItem.title=@"done";
        
	}
}
-(void)getParsedDetailsForSeller{
  //  [arraySellerDetails removeAllObjects];
    arraySellerDetails=[[NSMutableArray alloc]init];
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.mode = MBProgressHUDModeAnnularDeterminate;
    HUD.progress = 0.01f;
    [HUD show:YES];
  // http://extraticketapp.com/extraticket/list_favourites.php?user_id=1
    ETLoggedUserDetails *loggedUserDetails=[ETLoggedUserDetails sharedObject];
    NSString *pathParameters = [NSString stringWithFormat:@"http://extraticketapp.com/extraticket/list_favourites.php?user_id=%@",loggedUserDetails.stringUserID];
    NSURL *url = [[NSURL alloc] initWithString:pathParameters];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10];
    NSSet *contentType = [[NSSet alloc] initWithObjects:@"text/plain",@"text/html", nil];
    [AFJSONRequestOperation addAcceptableContentTypes:contentType];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        id result=[JSON objectForKey:@"root"];
        if (![result isKindOfClass:[NSArray class]]) {
            [HUD hide:YES];
        }
        else
        {
            for(int j=0;j<[result count];j++)
            {
                NSDictionary *dictionary = [result objectAtIndex:j];
                ETSellerDetails *sellerDetails=[[ETSellerDetails alloc]init];
                sellerDetails.stringPosterId=[dictionary objectForKey:@"user_id"];
                sellerDetails.stringTicketId=[dictionary objectForKey:@"ticket_id"];
                sellerDetails.stringAdTitle=[dictionary objectForKey:@"ticket_title"];
                sellerDetails.stringDistance=[dictionary objectForKey:@"distance"];
                sellerDetails.stringPosterDescription=[dictionary objectForKey:@"ticket_description"];
                sellerDetails.stringPosterCurrentLocation=[dictionary objectForKey:@"current_location"];
                sellerDetails.stringPosterSetLocation=[dictionary objectForKey:@"select_location"];
                sellerDetails.stringPosterName=[dictionary objectForKey:@"seller_name"];
                sellerDetails.stringPosterPhoneNo=[dictionary objectForKey:@"seller_phone"];
                sellerDetails.stringPosterDate=[dictionary objectForKey:@"date_time"];
                sellerDetails.stringPosterImage=[dictionary objectForKey:@"seller_profile_pic"];
                sellerDetails.stringTypeOfTicket=[dictionary objectForKey:@"type"];
                sellerDetails.stringZipCode=[dictionary objectForKey:@"zipcode"];
                
                [arraySellerDetails addObject:sellerDetails];
            }
            [HUD hide:YES];
            [tableViewSellerDetails reloadData];
        }
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        UIAlertView *temp=[[UIAlertView alloc]initWithTitle:@"Message!" message:@"No response from webserver" delegate:self
                                          cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [temp show];
        [HUD hide:YES];
    }];
    [operation start];
}

#pragma mark - Tableview datasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 150;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [arraySellerDetails count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"HomeTableCell";
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell==nil) {
        NSArray *nibs=[[NSBundle mainBundle] loadNibNamed:@"ETHomeTableCell" owner:nil options:nil];
        if ([nibs count]>0) {
            cell=[nibs objectAtIndex:0];
        }
    }
    
    
    ETSellerDetails *sellerDetails=[arraySellerDetails objectAtIndex:indexPath.row];
    UITextView *textViewDocument=(UITextView *)[cell viewWithTag:1];
    [textViewDocument setText:sellerDetails.stringAdTitle];
    UIImageView *imageViewProfile=(UIImageView *)[cell viewWithTag:2];
    NSString *imagePath=[NSString stringWithFormat:@"http://extraticketapp.com/extraticket/images/%@",sellerDetails.stringPosterImage];
    UIImage *placeHolder=[UIImage imageNamed:@"profilepic.png"];
    
    //TODO : Have to comment : Baji
    [imageViewProfile sd_setImageWithURL:[NSURL URLWithString:imagePath]
                     placeholderImage:placeHolder
                              options:SDWebImageRefreshCached];
    [imageViewProfile sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:placeHolder];
    UILabel *labelName=(UILabel *)[cell viewWithTag:3];
    [labelName setText:sellerDetails.stringPosterName];
    UILabel *labelPhoneNo=(UILabel *)[cell viewWithTag:4];
    [labelPhoneNo setText:sellerDetails.stringPosterPhoneNo];
    UILabel *labelDate=(UILabel *)[cell viewWithTag:5];
    [labelDate setText:sellerDetails.stringPosterDate];
    UIButton *buttonEdit=(UIButton *)[cell viewWithTag:6];
    [buttonEdit setHidden:TRUE];
    buttonFavourites=(UIButton *)[cell viewWithTag:11];
    [buttonFavourites setBackgroundImage:[UIImage imageNamed:@"FavoritesYellow.png"] forState:UIControlStateNormal];
   // [buttonFavourites addTarget:self action:@selector(setFavouriteAction:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *buttonPhone=(UIButton *)[cell viewWithTag:12];
    [buttonPhone addTarget:self action:@selector(makePhoneCallAction:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *buttonLocation=(UIButton *)[cell viewWithTag:13];
    UILabel *labelDistanceMiles=(UILabel *)[cell viewWithTag:15];
    if ([sellerDetails.stringDistance isKindOfClass:[NSNull class]]) {
        
    }
    else if ([sellerDetails.stringDistance isEqualToString:@"0"]) {
        [labelDistanceMiles setText:sellerDetails.stringDistance];
    }
    else{
        NSString  *mystr=[sellerDetails.stringDistance substringToIndex:3];
        [labelDistanceMiles setText:mystr];
    }
    
    [buttonLocation addTarget:self action:@selector(getLocation:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *buttonAdDetails=(UIButton *)[cell viewWithTag:14];
    [buttonAdDetails addTarget:self action:@selector(showDetailsAdAction:) forControlEvents:UIControlEventTouchUpInside];
    UILabel *labelType=(UILabel *)[cell viewWithTag:17];
    [labelType setText:[NSString stringWithFormat:@"(%@)",sellerDetails.stringTypeOfTicket]];
    UILabel *labelMessageCount=(UILabel *)[cell viewWithTag:19];
    [labelMessageCount setHidden:TRUE];
    UIImageView *imgView=(UIImageView *)[cell viewWithTag:20];
    [imgView setHidden:TRUE];

    
    return cell;
}
-(void)getLocation:(UIButton *)sender{
    UITableViewCell *cell;
    NSIndexPath *indexPath;
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // Load resources for iOS 6.1 or earlier
        cell  = (UITableViewCell *)[sender.superview superview];
        indexPath= [tableViewSellerDetails indexPathForCell:cell];
    } else {
        // Load resources for iOS 7 or later
        cell  = (UITableViewCell *)[[sender.superview superview] superview];
        indexPath = [tableViewSellerDetails indexPathForCell:cell];
    }
    ETAdLocationViewController *locationViewController=[[ETAdLocationViewController alloc]initWithNibName:@"ETAdLocationViewController" bundle:nil];
    locationViewController.sellerAdLocationDetails=[arraySellerDetails objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:locationViewController animated:YES];
    
}
-(void)showDetailsAdAction:(UIButton *)sender{
    UITableViewCell *cell;
    NSIndexPath *indexPath;
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // Load resources for iOS 6.1 or earlier
        cell  = (UITableViewCell *)[sender.superview superview];
        indexPath= [tableViewSellerDetails indexPathForCell:cell];
    } else {
        // Load resources for iOS 7 or later
        cell  = (UITableViewCell *)[[sender.superview superview] superview];
        indexPath = [tableViewSellerDetails indexPathForCell:cell];
    }
    ETAdDetailViewController *adDetailViewController=[[ETAdDetailViewController alloc]initWithNibName:@"ETAdDetailViewController" bundle:nil];
    adDetailViewController.sellerAdDetails=[arraySellerDetails objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:adDetailViewController animated:YES];
    
}
-(void)makePhoneCallAction:(UIButton *)sender{
    UITableViewCell *cell;
    NSIndexPath *indexPath;
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // Load resources for iOS 6.1 or earlier
        cell  = (UITableViewCell *)[sender.superview superview];
        indexPath= [tableViewSellerDetails indexPathForCell:cell];
    } else {
        // Load resources for iOS 7 or later
        cell  = (UITableViewCell *)[[sender.superview superview] superview];
        indexPath = [tableViewSellerDetails indexPathForCell:cell];
    }
    ETSellerDetails *sellerDetails=[arraySellerDetails objectAtIndex:indexPath.row];
    NSString *phoneNumber = [@"tel://" stringByAppendingString:sellerDetails.stringPosterPhoneNo];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

-(void)setFavouriteAction:(UIButton *)sender{
    UITableViewCell *cell;
    NSIndexPath *indexPath;
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // Load resources for iOS 6.1 or earlier
        cell  = (UITableViewCell *)[sender.superview superview];
        indexPath= [tableViewSellerDetails indexPathForCell:cell];
    } else {
        // Load resources for iOS 7 or later
        cell  = (UITableViewCell *)[[sender.superview superview] superview];
        indexPath = [tableViewSellerDetails indexPathForCell:cell];
    }
    
    ETSellerDetails *sellerDetails=[arraySellerDetails objectAtIndex:indexPath.row];
    ETLoggedUserDetails *loggedUserDetails=[ETLoggedUserDetails sharedObject];
    NSString *pathParameters = [NSString stringWithFormat:@"http://extraticketapp.com/extraticket/set_favourite.php?ticket_id=%@&user_id=%@",sellerDetails.stringTicketId,loggedUserDetails.stringUserID];
    pathParameters = [pathParameters stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [[NSURL alloc] initWithString:pathParameters];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10];
    //NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLCacheStorageNotAllowed timeoutInterval:20];
    NSSet *contentType = [[NSSet alloc] initWithObjects:@"text/plain",@"text/html", nil];
    [AFJSONRequestOperation addAcceptableContentTypes:contentType];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        if ([[JSON objectForKey:@"root"]isEqualToString:@"Set as favourite"]) {
            [buttonFavourites setBackgroundImage:[UIImage imageNamed:@"FavoritesYellow.png"] forState:UIControlStateNormal];
        }
        else if ([[JSON objectForKey:@"root"]isEqualToString:@"Unset"]){
            [buttonFavourites setBackgroundImage:[UIImage imageNamed:@"Favorites.png"] forState:UIControlStateNormal];
        }
    }
                                                                                        failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                                                            UIAlertView *temp=[[UIAlertView alloc]initWithTitle:@"Message!" message:@"No response from webserver" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                                                                            [temp show];
                                                                                        }];
    [operation start];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // No editing style if not editing or the index path is nil.
    if (self.editing == NO || !indexPath )
        return UITableViewCellEditingStyleNone;
    
    else
	{
		return UITableViewCellEditingStyleDelete;
	}
    return UITableViewCellEditingStyleNone;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
       //http://extraticketapp.com/extraticket/delete_favourite.php?user_id=2&ticket_id=8
      //http://extraticketapp.com/extraticket/block_tickets.php?user_id=1&ticket_id=2
        ETLoggedUserDetails *loggedUserDetails=[ETLoggedUserDetails sharedObject];
        ETSellerDetails *sellerDetails=[arraySellerDetails objectAtIndex:indexPath.row];
        NSString *pathParameters = [NSString stringWithFormat:@"http://extraticketapp.com/extraticket/delete_favourite.php?user_id=%@&ticket_id=%@",loggedUserDetails.stringUserID,sellerDetails.stringTicketId];
        pathParameters = [pathParameters stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [[NSURL alloc] initWithString:pathParameters];
        // NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:20
                                 ];
        NSSet *contentType = [[NSSet alloc] initWithObjects:@"text/plain",@"text/html", nil];
        [AFJSONRequestOperation addAcceptableContentTypes:contentType];
        AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
            if ([[JSON objectForKey:@"root"]isEqualToString:@"deleted"]) {
                [arraySellerDetails removeObjectAtIndex:indexPath.row];
                [tableViewSellerDetails reloadData];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Your Favorite ticket have been deleted successfully" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
            else{
                NSLog(@"Not deleted!!!");
                
            }
        }
                                                                                            failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                                                                UIAlertView *temp=[[UIAlertView alloc]initWithTitle:@"Message!" message:@"No response from webserver" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                                                                                [temp show];
                                                                                            }];
        [operation start];
    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
