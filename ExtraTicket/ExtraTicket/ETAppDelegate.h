//
//  ETAppDelegate.h
//  ExtraTicket
//
//  Created by mac on 10/15/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "ETSellerDetails.h"

@class ETChatViewController;

@interface ETAppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate>{
    
    NSString *userEmail,*password,*latitude,*longitude,*stringPushNotificationContent,*userId;
    NSString *stringLoadingFromPush;
    CLLocationManager *locationManager;
    UINavigationController *navController;
   
    ETSellerDetails *sellerDetails;
    id currentViewController;
}
@property (nonatomic, assign) id currentViewController;
@property (strong, nonatomic) ETChatViewController *viewController;
@property (strong, nonatomic) UIWindow *window;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
-(void)setLogin;
-(void)setFirstlyLogin;
- (void)saveContext;
-(void)addPush;
-(void)cancelPush;
- (NSURL *)applicationDocumentsDirectory;

@end
/*

 
 1.  photo cannot expand and make larger.
 when a user sends a photo in the chat/message,
 the user must have the option to tap on photo and make it larger.
 
 (see image 0111.png)
 
 2. the map GPS is not correct. does not show current location.
 
 (see image 0111.png)
 
 3. when a user is sent a chat/message(s),
 currently the user taps on unread message icon(s)
 and is directed to HOME page.
 (see images; 0033, 0022, redalert0033,
 Please make so when user taps on UNREAD ALERT (red icon), they are directed to the AD message page(0044.png)
 
 4. Do NOT place a RED ALERT icon on HOME page or
 on  MY TICKET page if there are NO UNREAD alerts( see image: home0unread, myticket0unread)
 
 5. ONLY place a RED ALERT icon on homepage, mypage on view ad page if chat/message is UNREAD. (see images: home1unread; 3unread;  )
 
 
 6. When a new ad is posted and user is sent alert(s), (see images: 0022; alert00777), user must tap on OK and directed to AD (image:00444)
 
 7. Reply box: message/chat reply increase to 300 characters (image: 0044)
 
 8. zip code box 5 characters ONLY (see image: 0066)
 
 9. REPLY to message/chat. When a user is sent a message/chat, they must be able to tap on mess/chat and REPLY and the response is sent to user.  (see image: alert00555) user taps on alertoo555 and is directed to ad chat/message (image: 0044) and has option to reply, send photo read message. Currently it is too confusing on how to REPLY to message.
 
 
*/