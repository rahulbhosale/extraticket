//
//  ETAboutInfoViewController.m
//  ExtraTicket
//
//  Created by mac on 10/18/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import "ETAboutInfoViewController.h"
#import "MFSideMenuContainerViewController.h"
#import "ETAppDelegate.h"

@interface ETAboutInfoViewController ()

@end

@implementation ETAboutInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupMenuBarButtonItems];
//    UIBarButtonItem *leftBar=[[UIBarButtonItem alloc]initWithTitle:@"Log Out" style:UIBarButtonItemStylePlain target:self action:@selector(logoutAction)];
//    self.navigationItem.rightBarButtonItem=leftBar;
     // Do any additional setup after loading the view from its nib.
}
-(void)logoutAction{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"LoggedIn"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    ETAppDelegate *appDelegate=(ETAppDelegate *)[[UIApplication sharedApplication]delegate];
    [appDelegate setFirstlyLogin];
}

#pragma  mark-split view controller
- (MFSideMenuContainerViewController *)menuContainerViewController {
    return (MFSideMenuContainerViewController *)self.navigationController.parentViewController;
}
#pragma mark - UIBarButtonItems

- (void)setupMenuBarButtonItems
{
    UIButton *leftBarButtonItem = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftBarButtonItem setImage:[UIImage imageNamed:@"menu-icon.png"] forState:UIControlStateNormal];
    leftBarButtonItem.frame = CGRectMake(0, 0, 43, 28);
    [leftBarButtonItem addTarget:self action:@selector(leftSideMenuButtonPressed:) forControlEvents:
     UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftBarButtonItem];
}


#pragma mark - UIBarButtonItem Callbacks

- (void)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)leftSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^
     {
         [self setupMenuBarButtonItems];
     }];
}

- (void)rightSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleRightSideMenuCompletion:^
     {
         [self setupMenuBarButtonItems];
     }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
