//
//  ETChatViewController.m
//  ExtraTicket
//
//  Created by mac on 10/22/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import "ETChatViewController.h"
#import "AFNetworking.h"
#import "UIColor+Convert.h"
#import "FontClass.h"
#import "ChatKeyboardViewController.h"
#import "ETLoggedUserDetails.h"
#import "ETMessageDetails.h"
#import "UIImageView+AFNetworking.h"
#import "JSON.h"
#import "PPMapViewAnnotation.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "SDWebImage/UIButton+WebCache.h"
#import "ETAppDelegate.h"
#import "MFSideMenuContainerViewController.h"

#define kMaxMessageLength 160
#define newlineMaxLength 2
#define newLineTextView 4
#define SEND_BUTTON_WIDTH 78.0f
#define kKeyboardAnimationDuration 0.3
#define kCellHeight 120.0

//ChatViewWithCamera

#import "SPHBubbleCell.h"
#import "SPHBubbleCellImage.h"
#import "SPHBubbleCellImageOther.h"
#import "SPHBubbleCellOther.h"
#import "SPHChatData.h"
#import "WebViewController.h"

#define messageWidth 260

@interface ETChatViewController (){
IBOutlet UIToolbar *toolBarPostComments;
IBOutlet UITextField *textFieldAddComments;
NSTimer  *timer;
NSString *stringRootID;
NSString *stringMessagedId;
CLLocationManager *locationManager;
    NSString *stringCity;
    NSString *stringState;
    NSString *stringCountry;
    NSString *stringLocality;
    NSMutableArray *arrayLocationDetails;
    NSString *locationDB;
    NSString *stringSearchedArea;
    BOOL isParsedOnce;
    NSString *stringAdTitle;
}

@end

@implementation ETChatViewController
@synthesize sellerFromMyTicketsForChat,messageDetailsFromMyTickets,stringFromMyTickets,stringCurrentLocation,stringSetLocation,stringLoadingFromPush;
@synthesize pullToRefreshManager = pullToRefreshManager_;
@synthesize reloads = reloads_;

@synthesize imgPicker;
@synthesize Uploadedimage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    isParsedOnce = FALSE;
    [super viewDidLoad];
    isImagePicked=NO;
    locationManager = [[CLLocationManager alloc] init];
    arrayLocationDetails=[[NSMutableArray alloc]init];
    messagesArray = [[NSMutableArray alloc] init];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.mode = MBProgressHUDModeAnnularDeterminate;
    HUD.progress = 0.01f;
    
    mapViewMyMap.showsUserLocation = TRUE;
    [mapViewMyMap setDelegate:self];
    
    //ETChatMessageWithCamera Starts
    
//    ETAppDelegate *MyWatcher = [[UIApplication sharedApplication] delegate];
//    MyWatcher.currentViewController = self;
    
    sphBubbledata=[[NSMutableArray alloc]init];
    [self setUpTextFieldforIphone];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
  //  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
    
//    pullToRefreshManager_ = [[MNMPullToRefreshManager alloc] initWithPullToRefreshViewHeight:60.0f
//                                                                                   tableView:self.sphChatTable
//                                                                                  withClient:self];
   // [self setUpDummyMessages];

    //ETChatMessageWithCamera Ends
    
    if ([stringFromMyTickets isEqualToString:@"MyTickets"]) {
        if ([stringSetLocation isEqualToString:@""]) {
            locationDB=stringCurrentLocation;
        }
        else{
            locationDB=stringSetLocation;
        }
    }
    else{
        if (!sellerFromMyTicketsForChat.stringPosterSetLocation.length) {
            locationDB=sellerFromMyTicketsForChat.stringPosterCurrentLocation;
        }
        else{
            locationDB=sellerFromMyTicketsForChat.stringPosterSetLocation;
        }
  
    }
    
   // [self showLocatons];
    //[self gettingCurrentLocation];
    ETLoggedUserDetails *loggedUserDetails=[ETLoggedUserDetails sharedObject];
    int messageCount = [loggedUserDetails.stringMessageCount intValue];
    if(messageCount > 0)
    {
        messageCount = messageCount - [messageDetailsFromMyTickets.stringMessageCountForUsersBadge intValue];
        loggedUserDetails.stringMessageCount = [NSString stringWithFormat:@"%d",messageCount];
    }
    self.navigationItem.title=@"Chat";

    if ([stringLoadingFromPush isEqualToString:@"pushnotification"]) {
    
            self.navigationController.navigationBarHidden=NO;
            UIBarButtonItem *leftBar=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(doneAction)];
            self.navigationItem.rightBarButtonItem=leftBar;
    }
   
    
    
    
    [self parseFunctionViewMessage];

    
    
//    [[UINavigationBar appearance] setTintColor:[UIColor blackColor]];
//    
//    //[self keyboardChatFunction];
  
    
timer=[NSTimer scheduledTimerWithTimeInterval:8.0 target:self selector:@selector(timerReloadTable) userInfo:nil repeats:YES];
    
     // Do any additional setup after loading the view from its nib.
}


-(void)setUpTextFieldforIphone
{
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget: self action:@selector(doDoubleTap:)];
    doubleTap.numberOfTapsRequired = 1;
    [self.sphChatTable addGestureRecognizer:doubleTap];
    
    
    containerView = [[UIView alloc] initWithFrame:CGRectMake(0,self.view.frame.size.height-40, 320, 42)];
   // textView = [[HPGrowingTextView alloc] initWithFrame:CGRectMake(40, 3, 206, 40)];
    textView = [[UITextView alloc] initWithFrame:CGRectMake(40, 3, 206, 40)];
  //  textView.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
 
//	textView.minNumberOfLines = 1;
//	textView.maxNumberOfLines = 6;
//	textView.returnKeyType = UIReturnKeyDefault; //just as an example
	textView.font = [UIFont systemFontOfSize:15.0f];
	textView.delegate = self;
   // textView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 0, 5, 0);
    textView.backgroundColor = [UIColor whiteColor];
   
    // textView.text = @"test\n\ntest";
	// textView.animateHeightChange = NO; //turns off animation
    
    [self.view addSubview:containerView];
	
    UIImage *rawEntryBackground = [UIImage imageNamed:@"MessageEntryInputField.png"];
    UIImage *entryBackground = [rawEntryBackground stretchableImageWithLeftCapWidth:13 topCapHeight:22];
    UIImageView *entryImageView = [[UIImageView alloc] initWithImage:entryBackground];
    entryImageView.frame = CGRectMake(40, 0,210, 40);
    entryImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    UIImage *rawBackground = [UIImage imageNamed:@"MessageEntryBackground.png"];
    UIImage *background = [rawBackground stretchableImageWithLeftCapWidth:13 topCapHeight:22];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:background];
    imageView.frame = CGRectMake(0, 0, containerView.frame.size.width, containerView.frame.size.height);
    imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    textView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    // view hierachy
    [containerView addSubview:imageView];
    [containerView addSubview:textView];
//    [containerView addSubview:entryImageView];
    
    UIImage *sendBtnBackground = [[UIImage imageNamed:@"MessageEntrySendButton.png"] stretchableImageWithLeftCapWidth:13 topCapHeight:0];
    
    UIImage *camBtnBackground = [[UIImage imageNamed:@"cam.png"] stretchableImageWithLeftCapWidth:0 topCapHeight:0];
    
    
    UIImage *selectedSendBtnBackground = [[UIImage imageNamed:@"MessageEntrySendButton.png"] stretchableImageWithLeftCapWidth:13 topCapHeight:0];
    
	UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
	doneBtn.frame = CGRectMake(containerView.frame.size.width - 69, 8, 63, 27);
    doneBtn.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
	[doneBtn setTitle:@"send" forState:UIControlStateNormal];
    
    [doneBtn setTitleShadowColor:[UIColor colorWithWhite:0 alpha:0.4] forState:UIControlStateNormal];
    doneBtn.titleLabel.shadowOffset = CGSizeMake (0.0, -1.0);
    doneBtn.titleLabel.font = [UIFont boldSystemFontOfSize:18.0f];
    
    [doneBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[doneBtn addTarget:self action:@selector(resignTextView) forControlEvents:UIControlEventTouchUpInside];
    [doneBtn setBackgroundImage:sendBtnBackground forState:UIControlStateNormal];
    [doneBtn setBackgroundImage:selectedSendBtnBackground forState:UIControlStateSelected];
	[containerView addSubview:doneBtn];
    
    
    
    UIButton *doneBtn2 = [UIButton buttonWithType:UIButtonTypeCustom];
	doneBtn2.frame = CGRectMake(containerView.frame.origin.x+1,2, 35,40);
    doneBtn2.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin;
	[doneBtn2 setTitle:@"" forState:UIControlStateNormal];
    
    [doneBtn2 setTitleShadowColor:[UIColor colorWithWhite:0 alpha:0.4] forState:UIControlStateNormal];
    doneBtn2.titleLabel.shadowOffset = CGSizeMake (0.0, -1.0);
    doneBtn2.titleLabel.font = [UIFont boldSystemFontOfSize:18.0f];
    
    [doneBtn2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[doneBtn2 addTarget:self action:@selector(uploadImage:) forControlEvents:UIControlEventTouchUpInside];
    [doneBtn2 setBackgroundImage:camBtnBackground forState:UIControlStateNormal];
    
    //[doneBtn2 setBackgroundImage:selectedSendBtnBackground forState:UIControlStateSelected];
    
	[containerView addSubview:doneBtn2];
    containerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
}
- (void) doDoubleTap: (UITapGestureRecognizer *)recognizer
{
    [textView resignFirstResponder];
}

#pragma  mark - text view delegate
- (BOOL)textView:(UITextView *)textVieww shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    [textView scrollRangeToVisible:range];
        NSUInteger newLength = [textVieww.text length] + [text length] - range.length;
        return (newLength > 300) ? NO : YES;
    
    if([text isEqualToString:@"\n"]) {
        [textVieww resignFirstResponder];
        return NO;
    }
    return YES;
}


-(void)resignTextView{
    if ([textView.text length]<1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please add text!"delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        [textView resignFirstResponder];
        [HUD show:TRUE];
        [self postChatWithImage];
    }
    
}
-(void)postChatWithImage{

    ETLoggedUserDetails *loggedUserDetails=[ETLoggedUserDetails sharedObject];
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormat stringFromDate:today];
    stringComment=textView.text;
    
    if (isParsedOnce) {
        ETMessageDetails *message = [messagesArray objectAtIndex:messagesArray.count - 1];
        stringRootID= message.stringMessageId;
    }else{
       stringRootID=@"0";
    }
    
    
   // http://extraticketapp.com/extraticket/insert_msg.php?from_userid=7&to_userid=6&root_id=0&message=hifrom sruthi&date_time=2012-12-13&ticket_id=1&image=aa.jpg
        
//        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
//        NSString *stringDevToken = [prefs valueForKey:@"token"];
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    if ([stringFromMyTickets isEqualToString:@"MyTickets"]) {
        [parameters setObject:messageDetailsFromMyTickets.stringReceiverId forKey:@"from_userid"];
        [parameters setObject:messageDetailsFromMyTickets.stringSenderId forKey:@"to_userid"];
        [parameters setObject:stringRootID forKey:@"root_id"];
        [parameters setObject:stringComment forKey:@"message"];
        [parameters setObject:dateString forKey:@"date_time"];
        [parameters setObject:messageDetailsFromMyTickets.stringTicketForMessageId forKey:@"ticket_id"];
        [parameters setObject:stringSetLocation forKey:@"select_location"];
        [parameters setObject:stringCurrentLocation forKey:@"current_location"];
       
    }
    else{
        [parameters setObject:loggedUserDetails.stringUserID forKey:@"from_userid"];
        [parameters setObject:sellerFromMyTicketsForChat.stringPosterId forKey:@"to_userid"];
        [parameters setObject:stringRootID forKey:@"root_id"];
        [parameters setObject:stringComment forKey:@"message"];
        [parameters setObject:dateString forKey:@"date_time"];
        [parameters setObject:sellerFromMyTicketsForChat.stringTicketId forKey:@"ticket_id"];
        [parameters setObject:sellerFromMyTicketsForChat.stringPosterSetLocation forKey:@"select_location"];
        [parameters setObject:sellerFromMyTicketsForChat.stringPosterCurrentLocation forKey:@"current_location"];
    }
    [textView resignFirstResponder];
     NSURL *url = [NSURL URLWithString:@"http://extraticketapp.com/extraticket/"];
//    pngData = UIImagePNGRepresentation(self.Uploadedimage.image);
    pngData = UIImageJPEGRepresentation(self.Uploadedimage.image, 0.6);
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    NSMutableURLRequest *afRequest = [httpClient multipartFormRequestWithMethod:@"POST"
                                                                path:@"insert_msg.php"
                                                                         parameters:parameters
                                                          constructingBodyWithBlock:^(id <AFMultipartFormData>formData)
                                          {
                                              if (isImagePicked) {
                                                  [formData appendPartWithFileData:pngData
                                                                              name:@"image"
                                                                          fileName:[NSString stringWithFormat:@"%@.jpg",dateString]
                                                                          mimeType:@"image/jpeg"];
                                                  isImagePicked=NO;
                                              }
                                              
                                              
                                          }
                                          ];
        AFHTTPRequestOperation *operation =
        [httpClient HTTPRequestOperationWithRequest:afRequest
                                            success:^(AFHTTPRequestOperation *operation, id json) {
                                                
                                                NSData *resultData=(NSData *)json;
                                                NSDictionary *result = [NSJSONSerialization JSONObjectWithData:resultData options:NSJSONReadingAllowFragments error:nil];
                                                
                                                if ([[[result objectForKey:@"root"]objectForKey:@"Result"] isEqualToString:@"Inserted"]) {
                                                    textView.text=@"";
                                                    [self parseFunctionViewMessage];
                                                }
                                                else{
                                                    NSLog(@"Not Updated!!!");
                                                    
                                                }
                                            }
                                            failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message!" message:@"Connection Error,No response from web servies!!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                [alert show];
                                                [HUD hide:YES];
                                            }];
        [httpClient enqueueHTTPRequestOperation:operation];
}


-(void)uploadImage:(id)sender{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Choose Image Source"delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: @"Take Photo",@"Choose Photo",nil];
    [alert show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    UIImagePickerController *picker=[[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    if (buttonIndex == 1){
        if (![UIImagePickerController isCameraDeviceAvailable: UIImagePickerControllerCameraDeviceRear])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Camera Available" message:@"his Feature requires camera"  delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil];
            [alert show];
        }
        else{
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:picker animated:YES completion:Nil];
            [picker setAllowsEditing:NO];
            
        }
    }
    else if (buttonIndex == 2){
        picker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:Nil];
        [picker setAllowsEditing:NO];
    }
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage : (UIImage *)image editingInfo:(NSDictionary *)editingInfo{
    isImagePicked=YES;
    self.Uploadedimage.image = image;
    [picker dismissViewControllerAnimated:YES completion:nil];
}
- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

-(void)addBubbledata:(NSString*)messageType  mtext:(NSString*)messagetext mtime:(NSString*)messageTime mimage:(UIImage*)messageImage  msgstatus:(NSString*)status;
{
    SPHChatData *feed_data=[[SPHChatData alloc]init];
    feed_data.messageText=messagetext;
    feed_data.messageImageURL=messagetext;
    feed_data.messageImage=messageImage;
    feed_data.messageTime=messageTime;
    feed_data.messageType=messageType;
    feed_data.messagestatus=status;
    [sphBubbledata addObject:feed_data];
    [self.sphChatTable reloadData];
    
    [self performSelector:@selector(scrollTableview) withObject:nil afterDelay:0.0];
}


/*-(void)adddBubbledata:(NSString*)messageType  mtext:(NSString*)messagetext mtime:(NSString*)messageTime mimage:(UIImage*)messageImage  msgstatus:(NSString*)status;
{
    SPHChatData *feed_data=[[SPHChatData alloc]init];
    feed_data.messageText=messagetext;
    feed_data.messageImageURL=messagetext;
    feed_data.messageImage=messageImage;
    feed_data.messageTime=messageTime;
    feed_data.messageType=messageType;
    feed_data.messagestatus=status;
    [sphBubbledata addObject:feed_data];
    [self.sphChatTable reloadData];
    
    [self performSelector:@selector(scrollTableview) withObject:nil afterDelay:0.0];
}
*/


-(void)doneAction{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void) keyboardWillChange:(NSNotification*)notify {
    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
    CGRect endFrame;
    float duration = [[[notify userInfo] valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    [[[notify userInfo] valueForKey:UIKeyboardFrameEndUserInfoKey] getValue:&endFrame];
    endFrame = [self.view convertRect:endFrame fromView:nil];
    
    if (iOSDeviceScreenSize.height == 568)
    {
        float y = (endFrame.origin.y > self.view.bounds.size.height ? self.view.bounds.size.height-48 : endFrame.origin.y-48);
        [UIView animateWithDuration:duration animations:^{
            toolBarPostComments.frame = CGRectMake(0, y, self.view.bounds.size.width, 44);
        }];
    }
    else if (iOSDeviceScreenSize.height == 480)
    {
        float y = (endFrame.origin.y > self.view.bounds.size.height ? self.view.bounds.size.height-44 : endFrame.origin.y-44);
        
        [UIView animateWithDuration:duration animations:^{
            toolBarPostComments.frame = CGRectMake(0, y, self.view.bounds.size.width, 44);
        }];
        
    }
    
}


//-(void)viewWillAppear:(BOOL)animated{
//  [self parseFunctionViewMessage];   
//}

-(void)viewWillAppear:(BOOL)animated
{
    ETAppDelegate *appdelegate=(ETAppDelegate *)[[UIApplication sharedApplication]delegate];
    [appdelegate cancelPush];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [timer invalidate];
    ETAppDelegate *appdelegate=(ETAppDelegate *)[[UIApplication sharedApplication]delegate];
    [appdelegate addPush];
    
}
-(void)gettingCurrentLocation{
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    [locationManager startUpdatingLocation];
}

#pragma mark- CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    MKCoordinateRegion mapRegion;
    mapRegion.center = mapView.userLocation.coordinate;
    mapRegion.span.latitudeDelta = 0.2;
    mapRegion.span.longitudeDelta = 0.2;
    
    [mapView setRegion:mapRegion animated: YES];
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    [locationManager stopUpdatingLocation];
    locationManager.delegate = nil;
    CLLocation *currentLocation = newLocation;
    if (currentLocation != nil) {
    }
    float degrees = newLocation.coordinate.latitude;
    double decimal = fabs(newLocation.coordinate.latitude - degrees);
    int minutes = decimal * 60;
    double seconds = decimal * 3600 - minutes * 60;
    degrees = newLocation.coordinate.longitude;
    decimal = fabs(newLocation.coordinate.longitude - degrees);
    minutes = decimal * 60;
    seconds = decimal * 3600 - minutes * 60;
    NSString *urlString = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&sensor=true",currentLocation.coordinate.latitude,currentLocation.coordinate.longitude];
    
    //NSString *latString=[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
    
    NSData *responseData=[NSData dataWithContentsOfURL :[NSURL URLWithString:urlString]];
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    NSArray *root = [(NSDictionary*)[responseString JSONValue] objectForKey:@"results"];
    
    if (!root.count) {
        UIAlertView *alertNoData=[[UIAlertView alloc]initWithTitle:@"Locaton Not found" message:@"Please fill in your location inside settings page manually" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Camera",@"Photo Album" ,nil];
        [alertNoData show];
    }
    
    NSArray *arrayAddressComponents = [[root objectAtIndex:0] objectForKey:@"address_components"];
    for (NSDictionary *dict in arrayAddressComponents) {
        if ([[[dict objectForKey:@"types"] objectAtIndex:0] isEqualToString:@"administrative_area_level_2"])
            stringCity = [dict objectForKey:@"long_name"];
        if ([[[dict objectForKey:@"types"] objectAtIndex:0] isEqualToString:@"administrative_area_level_1"])
            stringState = [dict objectForKey:@"long_name"];
        if ([[[dict objectForKey:@"types"] objectAtIndex:0] isEqualToString:@"locality"])
            stringLocality = [dict objectForKey:@"long_name"];
        if ([[[dict objectForKey:@"types"] objectAtIndex:0] isEqualToString:@"country"])
            stringCountry = [dict objectForKey:@"long_name"];
    }
//    [self showLocatons];
}
-(void)showLocatons{
   // [arrayLocationDetails addObject:stringLocality];
    [arrayLocationDetails addObject:locationDB];
    NSMutableArray *arrayAnnotations = [[NSMutableArray alloc] init];
    for (int i =0; i<[arrayLocationDetails count]; i++)
        
    {
        stringSearchedArea=[arrayLocationDetails objectAtIndex:i] ;
        NSArray *arr=[stringSearchedArea componentsSeparatedByString:@","];
        stringSearchedArea=[arr objectAtIndex:0];
        CLLocationCoordinate2D location = [self addressLocation];
        MKCoordinateRegion region;
        region.center = location;
        MKCoordinateSpan span;
        span.latitudeDelta = 0.99;
        span.longitudeDelta = 0.99;
        region.span=span;
        [mapViewMyMap setRegion:region animated:TRUE];
        PPMapViewAnnotation *myAnnotation=[[PPMapViewAnnotation alloc] initWithTitle:stringSearchedArea andCoordinate:location];
//        myAnnotation.publicPartyDetailsHistory = partyDetails;
        [mapViewMyMap addAnnotation:myAnnotation];
        [arrayAnnotations addObject:myAnnotation];
        [mapViewMyMap addAnnotations:arrayAnnotations];
    }
    MKMapRect flyTo = MKMapRectNull;
    for (id annotation in arrayAnnotations) {
        MKMapPoint annotationPoint;
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0, 0);
        if (MKMapRectIsNull(flyTo)) {
            flyTo = pointRect;
        }
        else{
            flyTo = MKMapRectUnion(flyTo, pointRect);
        }
    }
}
-(CLLocationCoordinate2D) addressLocation
{
    float latitude;
    float longitude;
    NSString *stringRequest = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@",[stringSearchedArea stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSData *dataResponse = [NSData dataWithContentsOfURL:[NSURL URLWithString:stringRequest]];
    NSError *jsonParsingError = nil;
    NSDictionary *dictionaryFAQs = [NSJSONSerialization JSONObjectWithData:dataResponse options:0 error:&jsonParsingError];
  //  NSString *status=[dictionaryFAQs objectForKey:@"status"];
    {
        NSArray *arrayResults = [dictionaryFAQs objectForKey:@"results"];
        NSDictionary *resultsDictionary = [[arrayResults objectAtIndex:0] objectForKey:@"geometry"];
        NSDictionary *locationDictionary = [resultsDictionary objectForKey:@"location"];
        NSString *stringLatitude = [locationDictionary objectForKey:@"lat"];
        NSString *stringLongitude = [locationDictionary objectForKey:@"lng"];
        latitude = [stringLatitude floatValue];
        longitude = [stringLongitude floatValue];
    };
    CLLocationCoordinate2D loc;
    loc.latitude = latitude;
    loc.longitude = longitude;
    return loc;
}
#pragma mark - map view delegates
//When a map annotation point is added, zoom to it (1500 range)
-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    MKPinAnnotationView *customPinView=[[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"currentloc"];
    customPinView.pinColor = MKPinAnnotationColorPurple;
    customPinView.animatesDrop=TRUE;
    customPinView.canShowCallout = YES;
    customPinView.calloutOffset = CGPointMake(-5, 5);
    
    UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    [rightButton setTitle:annotation.title forState:UIControlStateNormal];
    [rightButton addTarget:self
                    action:@selector(showDetails:)
          forControlEvents:UIControlEventTouchUpInside];
    customPinView.rightCalloutAccessoryView = rightButton;
    
    return customPinView;
    
}
-(void)showDetails:(id)sender{
    NSLog(@"MapView Location Button Clickedd");
}

-(void)parseFunctionViewMessageFromMyTickets{
    
}
-(void)parseFunctionViewMessage{
    ETLoggedUserDetails *loggedUserDetails=[ETLoggedUserDetails sharedObject];
    NSString *pathParameters;
    if ([stringFromMyTickets isEqualToString:@"MyTickets"]) {
    pathParameters = [NSString stringWithFormat:@"http://extraticketapp.com/extraticket/view_chat.php?from_userid=%@&to_userid=%@&ticket_id=%@&userId=%@",messageDetailsFromMyTickets.stringReceiverId,messageDetailsFromMyTickets.stringSenderId,messageDetailsFromMyTickets.stringTicketForMessageId,loggedUserDetails.stringUserID];
    }
    else{
    pathParameters= [NSString stringWithFormat:@"http://extraticketapp.com/extraticket/view_chat.php?from_userid=%@&to_userid=%@&ticket_id=%@&userId=%@",loggedUserDetails.stringUserID,sellerFromMyTicketsForChat.stringPosterId,sellerFromMyTicketsForChat.stringTicketId,loggedUserDetails.stringUserID];
    }
   
    NSURL *url = [[NSURL alloc] initWithString:pathParameters];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10];
    //NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
   // NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLCacheStorageNotAllowed timeoutInterval:60];
    NSSet *contentType = [[NSSet alloc] initWithObjects:@"text/plain",@"text/html", nil];
    [AFJSONRequestOperation addAcceptableContentTypes:contentType];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        id result=[JSON objectForKey:@"root"];
        if (![result isKindOfClass:[NSArray class]]) {
            [HUD hide:YES];
        }
        else
        {
            isParsedOnce = TRUE;

               [messagesArray removeAllObjects];
            for(int j=0;j<[result count];j++)
            {
                NSDictionary *dictionary = [result objectAtIndex:j];
                ETMessageDetails *messageDetails=[[ETMessageDetails alloc]init];
                messageDetails.stringSenderId=[dictionary objectForKey:@"from_userid"];
                messageDetails.stringReceiverId=[dictionary objectForKey:@"to_userid"];
                messageDetails.stringMessageContent=[dictionary objectForKey:@"message"];
                messageDetails.stringMessagePostedDate=[dictionary objectForKey:@"date_time"];
                messageDetails.stringMessageSenderName=[dictionary objectForKey:@"from_username"];
                messageDetails.stringMessageReceiverName=[dictionary objectForKey:@"to_username"];
                messageDetails.stringMessageId=[dictionary objectForKey:@"message_id"];
                messageDetails.stringRootId=[dictionary objectForKey:@"root_id"];
                messageDetails.stringMessagengerProfilePic=[dictionary objectForKey:@"from_userprofile"];
                messageDetails.stringMessageReceiverProfilePic=[dictionary objectForKey:@"to_userprofile"];
                messageDetails.stringMainMessagePic=[dictionary objectForKey:@"image"];
                messageDetails.stringADTitle=[dictionary objectForKey:@"ticket_title"];
                stringAdTitle=messageDetails.stringADTitle;
                stringRootID=messageDetails.stringRootId;
               // stringMessagedId= messageDetails.stringMessageId;
                
               [messagesArray addObject:messageDetails];
            }
            [HUD hide:YES];
            [self.sphChatTable reloadData];
            //[self performSelector:@selector(scrollTableview) withObject:nil afterDelay:0.0];
        }
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
//        UIAlertView *temp=[[UIAlertView alloc]initWithTitle:@"Message!" message:@"No response from webserver" delegate:self
//                                          cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [temp show];
//        [HUD hide:YES];
    }];
    [operation start];
}

-(void)timerReloadTable
{
    [self parseFunctionViewMessage];
}

/*- (void)setTitle:(NSString *)title
{
    [super setTitle:title];
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont fontWithName:@"Minion Pro" size:20];
        
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        
        titleView.textColor = [UIColor colorWithHexString:@"e6d3ad"]; // Change to desired color
        
        self.navigationItem.titleView = titleView;
        
    }
    titleView.text = title;
    [titleView sizeToFit];
}
*/
#pragma mark tableview

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return stringAdTitle;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return messagesArray.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float cellHeight;
    
    ETMessageDetails *messageDetails=[messagesArray objectAtIndex:indexPath.row];
    NSString *messageText = messageDetails.stringMessageContent;
    //
    CGSize boundingSize = CGSizeMake(messageWidth-20, 10000000);
    CGSize itemTextSize = [messageText sizeWithFont:[UIFont systemFontOfSize:14]
                                  constrainedToSize:boundingSize
                                      lineBreakMode:NSLineBreakByWordWrapping];

    if ([messageDetails.stringMainMessagePic isEqualToString:@"0"]) {

        // plain text
        cellHeight = itemTextSize.height;
        
        if (cellHeight<25) {
            
            cellHeight=25;
        }
        cellHeight = cellHeight+80;

    }
    else{
        cellHeight = itemTextSize.height + 150;
    }
    return cellHeight;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.sphChatTable deselectRowAtIndexPath:indexPath animated:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    UITableViewCell *cell1;
    
    ETLoggedUserDetails *loggedUserDetails=[ETLoggedUserDetails sharedObject];
    ETMessageDetails *messageDetails=[messagesArray objectAtIndex:indexPath.row];
    NSString *messageText = messageDetails.stringMessageContent;
    
    static NSString *CellIdentifier1 = @"Cell1";
    static NSString *CellIdentifier2 = @"Cell2";
    
    
    CGSize boundingSize = CGSizeMake(messageWidth-20, 10000000);
    CGSize itemTextSize = [messageText sizeWithFont:[UIFont systemFontOfSize:14]
                                  constrainedToSize:boundingSize
                                      lineBreakMode:NSLineBreakByWordWrapping];
    float textHeight = itemTextSize.height+7;
    int x=0;
    if (textHeight>200)
    {
        x=65;
    }else if (textHeight>150)
        {
            x=50;
        }
        else if (textHeight>80)
        {
            x=30;
        }else
            if (textHeight>50)
            {
                x=20;
            }else
                if (textHeight>30) {
                    x=8;
                }
    
    // Types= ImageByme  , imageByOther  textByme  ,textbyother
    
     if([loggedUserDetails.stringUserID isEqualToString:messageDetails.stringSenderId]) {
        
        SPHBubbleCellOther  *cell = (SPHBubbleCellOther *)[self.sphChatTable dequeueReusableCellWithIdentifier:CellIdentifier1];
        
        if (cell == nil) {
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SPHBubbleCellOther" owner:self options:nil];
            cell = [topLevelObjects objectAtIndex:0];
        }
        else{
            
        }
        
        UIImageView *bubbleImage=[[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"Bubbletyperight"] stretchableImageWithLeftCapWidth:21 topCapHeight:14]];
        bubbleImage.tag=55;
        [cell.contentView addSubview:bubbleImage];
        [bubbleImage setFrame:CGRectMake(260-itemTextSize.width,5,itemTextSize.width+24,textHeight+4)];
        
        UITextView *messageTextview=[[UITextView alloc]initWithFrame:CGRectMake(260 - itemTextSize.width+5,2,itemTextSize.width+10, textHeight-2)];
        [cell.contentView addSubview:messageTextview];
        messageTextview.editable=NO;
        messageTextview.text = messageText;
        messageTextview.dataDetectorTypes=UIDataDetectorTypeAll;
        messageTextview.textAlignment=NSTextAlignmentJustified;
        messageTextview.font=[UIFont fontWithName:@"Helvetica Neue" size:12.0];
        messageTextview.backgroundColor=[UIColor clearColor];
        messageTextview.tag=indexPath.row;
         
         NSString *imagePath=[NSString stringWithFormat:@"http://extraticketapp.com/extraticket/images/%@",messageDetails.stringMessagengerProfilePic];
         UIImage *placeHolder=[UIImage imageNamed:@"Customer_icon"];
         [cell.Avatar_Image sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:placeHolder];
        cell.name_Label.text=messageDetails.stringMessageSenderName;
        cell.time_Label.text=messageDetails.stringMessagePostedDate;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        messageTextview.scrollEnabled=NO;
        
        //[cell.Avatar_Image setupImageViewer];
        cell.Avatar_Image.layer.cornerRadius = 20.0;
        cell.Avatar_Image.layer.masksToBounds = YES;
        cell.Avatar_Image.layer.borderColor = [UIColor whiteColor].CGColor;
        cell.Avatar_Image.layer.borderWidth = 2.0;
        //return cell;
         
         if (![messageDetails.stringMainMessagePic isEqualToString:@"0"]) {
            
             UIImageView *imageViewBubble=[[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"Bubbletyperight"] stretchableImageWithLeftCapWidth:21 topCapHeight:14]];

             [imageViewBubble setFrame:CGRectMake(bubbleImage.frame.origin.x+bubbleImage.frame.size.width - 70, bubbleImage.frame.size.height +10 , 70, 70)];
             [cell.contentView addSubview:imageViewBubble];

             UIImageView *imageViewChatImage = [[UIImageView alloc] initWithFrame:CGRectMake(imageViewBubble.frame.origin.x+ 9, imageViewBubble.frame.origin.y + 11 , 45, 45)];
             [cell.contentView addSubview:imageViewChatImage];
             
             UIButton *buttonChat = [UIButton buttonWithType:UIButtonTypeCustom];
             [buttonChat setFrame:imageViewChatImage.frame];
             [buttonChat setAlpha:0.02];
             [buttonChat addTarget:self action:@selector(actionViewImage:) forControlEvents:UIControlEventTouchUpInside];
             [cell.contentView addSubview:buttonChat];
             
             NSString *imagePath=[NSString stringWithFormat:@"http://extraticketapp.com/extraticket/images/%@",messageDetails.stringMainMessagePic];
             UIImage *placeHolder=[UIImage imageNamed:@"Customer_icon"];
             [imageViewChatImage sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:placeHolder];
             [buttonChat setTitle:imagePath forState:UIControlStateNormal];
             [buttonChat sd_setImageWithURL:[NSURL URLWithString:imagePath] forState:UIControlStateNormal placeholderImage:placeHolder];
             
             cell.selectionStyle=UITableViewCellSelectionStyleNone;
         }
         cell1 = cell;
        }
     else
        if (![loggedUserDetails.stringUserID isEqualToString:messageDetails.stringSenderId]) {
            
            
            SPHBubbleCell  *cell = (SPHBubbleCell *)[self.sphChatTable dequeueReusableCellWithIdentifier:CellIdentifier2];
            
            if (cell == nil) {
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SPHBubbleCell" owner:self options:nil];
                cell = [topLevelObjects objectAtIndex:0];
            }
            else{
                
            }
            //[bubbleImage setFrame:CGRectMake(265-itemTextSize.width,5,itemTextSize.width+14,textHeight+4)];
            
            UIImageView *bubbleImage=[[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"Bubbletypeleft"] stretchableImageWithLeftCapWidth:21 topCapHeight:14]];
            [cell.contentView addSubview:bubbleImage];
            [bubbleImage setFrame:CGRectMake(45,5, itemTextSize.width+30, textHeight+4)];
            bubbleImage.tag=56;
            //CGRectMake(260 - itemTextSize.width+5,2,itemTextSize.width+10, textHeight-2)];
            UITextView *messageTextview=[[UITextView alloc]initWithFrame:CGRectMake(60,2,itemTextSize.width+10, textHeight-2)];
            [cell.contentView addSubview:messageTextview];
            messageTextview.editable=NO;
            messageTextview.text = messageText;
            messageTextview.dataDetectorTypes=UIDataDetectorTypeAll;
            messageTextview.textAlignment=NSTextAlignmentJustified;
            messageTextview.backgroundColor=[UIColor clearColor];
            messageTextview.font=[UIFont fontWithName:@"Helvetica Neue" size:12.0];
            messageTextview.scrollEnabled=NO;
            messageTextview.tag=indexPath.row;
            messageTextview.textColor=[UIColor whiteColor];
            cell.Avatar_Image.layer.cornerRadius = 20.0;
            cell.Avatar_Image.layer.masksToBounds = YES;
            cell.Avatar_Image.layer.borderColor = [UIColor whiteColor].CGColor;
            cell.Avatar_Image.layer.borderWidth = 2.0;
           // [cell.Avatar_Image setupImageViewer];
            
            NSString *imagePath=[NSString stringWithFormat:@"http://extraticketapp.com/extraticket/images/%@",messageDetails.stringMessagengerProfilePic];
            UIImage *placeHolder=[UIImage imageNamed:@"my_icon"];
            [cell.Avatar_Image sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:placeHolder];
            cell.name_Label.text=messageDetails.stringMessageSenderName;
            cell.time_Label.text=messageDetails.stringMessagePostedDate;
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
           // return cell;
           
            if (![messageDetails.stringMainMessagePic isEqualToString:@"0"]) {
                
                UIImageView *imageViewBubble=[[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"Bubbletypeleft"] stretchableImageWithLeftCapWidth:21 topCapHeight:14]];
                
                [imageViewBubble setFrame:CGRectMake(45, bubbleImage.frame.size.height +10 , 70, 70)];
                [cell.contentView addSubview:imageViewBubble];

                UIImageView *imageViewChatImage = [[UIImageView alloc] initWithFrame:CGRectMake(imageViewBubble.frame.origin.x+ 16, imageViewBubble.frame.origin.y + 11 , 45, 45)];
                [cell.contentView addSubview:imageViewChatImage];
                
                
                UIButton *buttonChat = [UIButton buttonWithType:UIButtonTypeCustom];
                [buttonChat setFrame:imageViewChatImage.frame];
                [buttonChat setAlpha:0.02];
                [buttonChat addTarget:self action:@selector(actionViewImage:) forControlEvents:UIControlEventTouchUpInside];
                [cell.contentView addSubview:buttonChat];

                NSString *imagePath=[NSString stringWithFormat:@"http://extraticketapp.com/extraticket/images/%@",messageDetails.stringMainMessagePic];
                UIImage *placeHolder=[UIImage imageNamed:@"Customer_icon"];
                [imageViewChatImage sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:placeHolder];
                [buttonChat setTitle:imagePath forState:UIControlStateNormal];
                [buttonChat sd_setImageWithURL:[NSURL URLWithString:imagePath] forState:UIControlStateNormal placeholderImage:placeHolder];
                NSLog(@"button image size %@",NSStringFromCGSize(buttonChat.currentBackgroundImage.size));

                cell.selectionStyle=UITableViewCellSelectionStyleNone;
                
            }
             cell1 =cell;
        }
    return cell1;
}


-(void)actionViewImage :(id)sender
{
    UIView *view = [[UIView alloc] initWithFrame:self.view.frame];
    [view setBackgroundColor:[UIColor blackColor]];
    CGFloat y = (self.view.frame.size.height - 300)/2;
    
    UIButton *button = (UIButton *) sender;
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, y, 300, 300)];
    [imageView setContentMode:UIViewContentModeScaleToFill];
    [imageView setImage:button.currentImage];
    
    UIButton *buttonClose = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonClose setTitle:@"Close" forState:UIControlStateNormal];
    [buttonClose setFrame:CGRectMake(240, 20, 80, 60)];
    [buttonClose addTarget:self action:@selector(removeChatImage:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:buttonClose];
    
    [view addSubview:imageView];
    [view setAlpha:0.0];
    [self.navigationController.view addSubview:view];
    [UIView animateWithDuration:0.50f delay:0.50f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        [view setAlpha:1.0];
    } completion:^(BOOL finished) {
    }];
}

-(void)removeChatImage : (id) sender
{
    UIButton *button = (UIButton *) sender;
    [button.superview removeFromSuperview];
    [UIView animateWithDuration:1.0f delay:0.50f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        [button.superview removeFromSuperview];
    } completion:^(BOOL finished) {
    }];
    
}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}


-(void)scrollTableview
{
    [self.sphChatTable reloadData];
    [HUD setProgress:1.0];
    [HUD hide:TRUE];
    int lastSection=[self.sphChatTable numberOfSections]-1;
    int lastRowNumber = [self.sphChatTable numberOfRowsInSection:lastSection]-1;
    NSIndexPath* ip = [NSIndexPath indexPathForRow:lastRowNumber inSection:lastSection];
    [self.sphChatTable scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
}

-(void) keyboardWillShow:(NSNotification *)note
{
    if (messagesArray.count>2) {
        
        [self performSelector:@selector(scrollTableview) withObject:nil afterDelay:0.0];
    }
    // get keyboard size and loctaion
	CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    // get a rect for the textView frame
	CGRect containerFrame = containerView.frame;
    containerFrame.origin.y = self.view.bounds.size.height - (keyboardBounds.size.height + containerFrame.size.height);
    
    CGRect tableviewframe=self.sphChatTable.frame;
    tableviewframe.size.height-=160;
    
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
	containerView.frame = containerFrame;
    self.sphChatTable.frame=tableviewframe;
    
	[UIView commitAnimations];
}

-(void) keyboardWillHide:(NSNotification *)note
{
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
	// get a rect for the textView frame
	CGRect containerFrame = containerView.frame;
    containerFrame.origin.y = self.view.bounds.size.height - containerFrame.size.height;
    CGRect tableviewframe=self.sphChatTable.frame;
    tableviewframe.size.height+=160;
    
    
	// animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
	// set views with new info
    self.sphChatTable.frame=tableviewframe;
	containerView.frame = containerFrame;
	// commit animations
	[UIView commitAnimations];
}



#pragma mark - TextView Functions
- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    float diff = (growingTextView.frame.size.height - height);
	CGRect r = containerView.frame;
    r.size.height -= diff;
    r.origin.y += diff;
	containerView.frame = r;
}


- (void)handleURL:(NSURL*)url
{
    WebViewController *controller = [[WebViewController alloc] initWithURL:url];
    [controller setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:controller animated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
