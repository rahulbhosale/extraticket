//
//  PPMapViewAnnotation.h
//  PostMyParty
//
//  Created by Srishti Innovative Computer Systems on 5/13/13.
//  Copyright (c) 2013 Srishti Innovative Computer Systems. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface PPMapViewAnnotation : NSObject<MKAnnotation>
{
    NSString *title;
	CLLocationCoordinate2D coordinate;
}
@property (nonatomic, copy) NSString *title;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

- (id)initWithTitle:(NSString *)ttl andCoordinate:(CLLocationCoordinate2D)c2d;
@end
