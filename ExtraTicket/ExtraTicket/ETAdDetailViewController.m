//
//  ETAdDetailViewController.m
//  ExtraTicket
//
//  Created by mac on 10/29/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import "ETAdDetailViewController.h"
#import "ETMyTicketsMessagesViewController.h"
#import "ETChatViewController.h"
#import "ETLoggedUserDetails.h"

@interface ETAdDetailViewController ()<NSURLConnectionDataDelegate>{
    IBOutlet UITextField *textFieldAdTitle;
    IBOutlet UITextView *textViewDescription;
    IBOutlet UILabel *labelDate;
    IBOutlet UILabel *labelPhone;
    BOOL isFromPush;
    NSMutableData *urlData;
}

@end

@implementation ETAdDetailViewController
@synthesize sellerAdDetails;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        isFromPush = NO;
        // Custom initialization
    }
    return self;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil fromPushNotification:(BOOL)isFromPushnotif
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        isFromPush = isFromPushnotif;
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title=@"Ad DetailView";
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    isFromPush?[self fetchAd]:[self setupAd];

}
-(void)setupAd{
    textFieldAdTitle.text=sellerAdDetails.stringAdTitle;
    textViewDescription.text=sellerAdDetails.stringPosterDescription;
    labelDate.text=sellerAdDetails.stringPosterDate;
    labelPhone.text=sellerAdDetails.stringPosterPhoneNo;
}
-(void)dissmissViewController{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)fetchAd{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dissmissViewController)];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://extraticketapp.com/extraticket/ticket_dls.php?ticket_id=%@",sellerAdDetails.stringTicketId]];
    NSLog(@"%@",[url absoluteString]);
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:[NSURLRequest requestWithURL:url] delegate:self];
    [connection start];
}
#pragma mark - NSURLConnection Delegates

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    urlData = [NSMutableData data];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [urlData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    [[[UIAlertView alloc] initWithTitle:error.localizedDescription message:Nil delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    NSError *error;
    id json = [NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:&error];
    if (!json) {
        [[[UIAlertView alloc] initWithTitle:error.localizedDescription message:Nil delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        return;
    }
    if ([json[@"result"] isEqualToString:@"Success"]) {
        sellerAdDetails.stringPosterId = json[@"details"][@"user_id"];
        sellerAdDetails.stringTicketId=json[@"details"][@"ticket_id"];
        sellerAdDetails.stringAdTitle=json[@"details"][@"ticket_title"];
        sellerAdDetails.stringPosterDescription=json[@"details"][@"ticket_description"];
        sellerAdDetails.stringPosterCurrentLocation=json[@"details"][@"current_location"];
        sellerAdDetails.stringPosterSetLocation=json[@"details"][@"select_location"];
        sellerAdDetails.stringPosterDate=json[@"details"][@"date_time"];
        sellerAdDetails.stringTypeOfTicket=json[@"details"][@"type"];
        sellerAdDetails.stringZipCode=json[@"details"][@"zipcode"];
        sellerAdDetails.stringPosterPhoneNo = json[@"details"][@"phone_num"];
    }
    [self setupAd];
}
/*
 {"ticket_id":"1",
 "user_id":"12",
 "ticket_title":"music",
 "ticket_description":"Ydjhdfjdhdjdhdj djfjdjdjddjdhhzxj hdjdjfjgkgkfkfkfjfjfgjf dhdhduufufirudifufufffufuu",
 "current_location":"Kazhakkoottam,Kerala",
 "select_location":"",
 "latitude":"8.56069902",
 "longitude":"76.88070041",
 "date_time":"2014-01-09 10:43:07",
 "zipcode":"86532",
 "type":"Sell"}
 */
#pragma mark UIButtonActions

-(IBAction)makePhoneCallAction:(id)sender
{
    UIAlertView *myAlert=[[UIAlertView alloc]initWithTitle:@"Make Call" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    myAlert.tag=1;
    [myAlert show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==1) {
        if (buttonIndex==1) {
            NSLog(@"OK");
            NSString *phoneNumber = [@"tel://" stringByAppendingString:sellerAdDetails.stringPosterPhoneNo];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];        }
    }
}
-(IBAction)makeChatAction:(id)sender{
   ETLoggedUserDetails *loggedUserDetails=[ETLoggedUserDetails sharedObject];
    if ([loggedUserDetails.stringUserID isEqualToString:sellerAdDetails.stringPosterId]) {
        ETMyTicketsMessagesViewController *myTicketsMessagesViewController=[[ETMyTicketsMessagesViewController alloc]initWithNibName:@"ETMyTicketsMessagesViewController" bundle:nil];
        myTicketsMessagesViewController.sellerFromMyTicketsForMessages=sellerAdDetails;
        [self.navigationController pushViewController:myTicketsMessagesViewController animated:YES];
    }
    else{
        ETChatViewController *chatViewController=[[ETChatViewController alloc]initWithNibName:@"ETChatViewController" bundle:nil];
        chatViewController.sellerFromMyTicketsForChat=sellerAdDetails;
      [self.navigationController pushViewController:chatViewController animated:YES];
    }
}

#pragma mark UITextViewDelegates

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}
#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
