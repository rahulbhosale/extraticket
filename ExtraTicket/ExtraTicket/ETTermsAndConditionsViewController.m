//
//  ETTermsAndConditionsViewController.m
//  ExtraTicket
//
//  Created by mac on 1/2/14.
//  Copyright (c) 2014 mac. All rights reserved.
//

#import "ETTermsAndConditionsViewController.h"

@interface ETTermsAndConditionsViewController ()

@end

@implementation ETTermsAndConditionsViewController
@synthesize delegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"termsconditions" ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    [webViewLoadContents loadHTMLString:htmlString baseURL:nil];
    // Do any additional setup after loading the view from its nib.
}
-(IBAction)didAgreeAction:(id)sender{
    NSString *stringAccept=@"accept";
    [self.delegate sendAccept:stringAccept];
[self dismissViewControllerAnimated:YES completion:nil];
}
-(IBAction)didCancelAction:(id)sender{
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
