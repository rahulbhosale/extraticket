//
//  ETBuyTicketViewController.m
//  ExtraTicket
//
//  Created by mac on 10/16/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import "ETBuyTicketViewController.h"
#import "MFSideMenuContainerViewController.h"
#import "JSON.h"
#import "ETSearchLocationViewController.h"
#import "ETLoggedUserDetails.h"
#import "AFNetworking.h"
#import "ETAppDelegate.h"
#import "ETHomeViewController.h"
#import "ETMyTicketsViewController.h"
#import "MBProgressHUD.h"

@interface ETBuyTicketViewController ()<sendPlaceDelegate>{
    CLLocationManager *locationManager;
    MBProgressHUD *HUD;
    NSString *stringCity;
    NSString *stringState;
    NSString *stringCountry;
    NSString *stringLocality;
    NSString *stringSetLocation;
    NSString *stringLatitudeToDB;
    NSString *stringLongitudeToDB;
}

@end

@implementation ETBuyTicketViewController
@synthesize sellerFromMyTickets,stringCheckEdit,stringCheckBuyOrCell;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    if ([stringCheckEdit isEqualToString:@"Edit"]) {
        textViewDescription.text=sellerFromMyTickets.stringPosterDescription;
        textFieldTitleToBuyTicket.text=sellerFromMyTickets.stringAdTitle;
        textFieldZipCode.text=sellerFromMyTickets.stringZipCode;
        
        
        if (![sellerFromMyTickets.stringPosterCurrentLocation isEqualToString:@""]){
              textFieldLocation.text=sellerFromMyTickets.stringPosterCurrentLocation;
            [switchLocation setOn:YES];
        }
        else if (![sellerFromMyTickets.stringPosterSetLocation isEqualToString:@""]){
             textFieldSetLocation.text=sellerFromMyTickets.stringPosterSetLocation;
          [switchLocation setOn:NO];
        }
       
        //textFieldSetLocation.text=sellerFromMyTickets.stringPosterLocation;
    }
    else{
    [switchLocation setOn:NO];
    [self setupMenuBarButtonItems];
    }
    [self.navigationController setNavigationBarHidden:NO];
    ETLoggedUserDetails *loggedUserDetails=[ETLoggedUserDetails sharedObject];
    if ([loggedUserDetails.stringBuyerOrSeller isEqualToString:@"Buyer"]) {
     self.navigationItem.title=@"Buy Ticket";
    }else if ([loggedUserDetails.stringBuyerOrSeller isEqualToString:@"Seller"]) {
     self.navigationItem.title=@"Sell Ticket";
    }
  
    // UIBarButtonItem *leftBar=[[UIBarButtonItem alloc]initWithTitle:@"Log Out" style:UIBarButtonItemStylePlain target:self action:@selector(logoutAction)];
     // self.navigationItem.rightBarButtonItem=leftBar;
}
-(void)logoutAction{
//    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"LoggedIn"];
//    [[NSUserDefaults standardUserDefaults]synchronize];
//    ETAppDelegate *appDelegate=(ETAppDelegate *)[[UIApplication sharedApplication]delegate];
//    [appDelegate setFirstlyLogin];
 //   [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:2] animated:TRUE];
//    NSArray *viewControllers = [self.navigationController viewControllers];
//    for (UIViewController *controller in viewControllers) {
//        if ([controller isKindOfClass:[ETHomeViewController class]]) {
//            [self.navigationController popToViewController:controller animated:YES];
//        }
//    }

}

#pragma  mark-split view controller
- (MFSideMenuContainerViewController *)menuContainerViewController {
    return (MFSideMenuContainerViewController *)self.navigationController.parentViewController;
}
#pragma mark - UIBarButtonItems

- (void)setupMenuBarButtonItems
{
    UIButton *leftBarButtonItem = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftBarButtonItem setImage:[UIImage imageNamed:@"menu-icon.png"] forState:UIControlStateNormal];
    leftBarButtonItem.frame = CGRectMake(0, 0, 43, 28);
    [leftBarButtonItem addTarget:self action:@selector(leftSideMenuButtonPressed:) forControlEvents:
     UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftBarButtonItem];
}


#pragma mark - UIBarButtonItem Callbacks

- (void)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)leftSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^
     {
         [self setupMenuBarButtonItems];
     }];
}

- (void)rightSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleRightSideMenuCompletion:^
     {
         [self setupMenuBarButtonItems];
     }];
}

#pragma mark- CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    [switchLocation setOn:NO];
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    [locationManager stopUpdatingLocation];
    locationManager.delegate = nil;
    CLLocation *currentLocation = newLocation;
    if (currentLocation != nil) {
//        longitudeLabel.text =[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
//        latitudeLabel.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
    }
    float degrees = newLocation.coordinate.latitude;
    double decimal = fabs(newLocation.coordinate.latitude - degrees);
    int minutes = decimal * 60;
    double seconds = decimal * 3600 - minutes * 60;
    degrees = newLocation.coordinate.longitude;
    decimal = fabs(newLocation.coordinate.longitude - degrees);
    minutes = decimal * 60;
    seconds = decimal * 3600 - minutes * 60;
    stringLongitudeToDB =[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
    stringLatitudeToDB = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];

    NSString *urlString = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&sensor=true",currentLocation.coordinate.latitude,currentLocation.coordinate.longitude];
    
    NSData *responseData=[NSData dataWithContentsOfURL :[NSURL URLWithString:urlString]];
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    NSArray *root = [(NSDictionary*)[responseString JSONValue] objectForKey:@"results"];
    
    if (!root.count) {
        UIAlertView *alertNoData=[[UIAlertView alloc]initWithTitle:@"Locaton Not found" message:@"Please fill in your location inside settings page manually" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Camera",@"Photo Album" ,nil];
        [alertNoData show];
    }
    NSArray *arrayAddressComponents = [[root objectAtIndex:0] objectForKey:@"address_components"];
    for (NSDictionary *dict in arrayAddressComponents) {
        if ([[[dict objectForKey:@"types"] objectAtIndex:0] isEqualToString:@"administrative_area_level_2"])
            stringCity = [dict objectForKey:@"long_name"];
        if ([[[dict objectForKey:@"types"] objectAtIndex:0] isEqualToString:@"administrative_area_level_1"])
            stringState = [dict objectForKey:@"long_name"];
        if ([[[dict objectForKey:@"types"] objectAtIndex:0] isEqualToString:@"locality"])
            stringLocality = [dict objectForKey:@"long_name"];
        if ([[[dict objectForKey:@"types"] objectAtIndex:0] isEqualToString:@"country"])
            stringCountry = [dict objectForKey:@"long_name"];
    }
    NSString *strPlace=[NSString stringWithFormat:@"%@,%@",stringLocality,stringState];
    textFieldLocation.text=strPlace;
   // stringSetLocation=textFieldLocation.text;
}
#pragma mark UITextViewDelegates

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}
#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if(textField == textFieldZipCode)
    {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 5) ? NO : YES;

    }
    return YES;
}

#pragma mark UIButtonActions
-(void)gettingCurrentLocation{
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    [locationManager startUpdatingLocation];
}
-(IBAction)didFindCurrentPositionOnSwitchAction:(id)sender{
    if (![textFieldSetLocation.text isEqualToString:@""]) {
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:Nil message:@"You have already set the location!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertView show];
        [switchLocation setOn:NO];
    }
    else{
    if ([switchLocation isOn]) {
         locationManager = [[CLLocationManager alloc] init];
         [self gettingCurrentLocation];
    }
    else{
        [locationManager stopUpdatingLocation];
        textFieldLocation.text=@"";
    }
    }
}
-(IBAction)saveDetailsToSetLocation:(id)sender{
    ETLoggedUserDetails *loggedUserDetails=[ETLoggedUserDetails sharedObject];
   
   
  // http://extraticketapp.com/extraticket/edit_mytickets.php?ticket_id=1&ticket_title=hghghghg&description=qwe&select_location=&current_location=zxc&date_time=2013-09-07%2010:00:00&longitude=7777&latitude=4444&zipcode=1111111
    
    if ([stringCheckEdit isEqualToString:@"Edit"]) {
        if (!stringLatitudeToDB && !stringLatitudeToDB) {
            stringLatitudeToDB= sellerFromMyTickets.stringLatitude;
            stringLongitudeToDB=sellerFromMyTickets.stringLongitude;
  
        }
        
        HUD = [[MBProgressHUD alloc] initWithView:self.view];
        [self.view addSubview:HUD];
        HUD.mode = MBProgressHUDModeAnnularDeterminate;
        HUD.progress = 0.01f;
        [HUD show:YES];
        
        NSDate *today = [NSDate date];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *dateString = [dateFormat stringFromDate:today];
        NSString *pathParameters = [NSString stringWithFormat:@"http://extraticketapp.com/extraticket/edit_mytickets.php?ticket_id=%@&ticket_title=%@&description=%@&select_location=%@&current_location=%@&date_time=%@&longitude=%@&latitude=%@&zipcode=%@",sellerFromMyTickets.stringTicketId,textFieldTitleToBuyTicket.text,textViewDescription.text,textFieldSetLocation.text,textFieldLocation.text,dateString,stringLongitudeToDB,stringLatitudeToDB,textFieldZipCode.text];
        pathParameters = [pathParameters stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [[NSURL alloc] initWithString:pathParameters];
        // NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:20
                                 ];
        NSSet *contentType = [[NSSet alloc] initWithObjects:@"text/plain",@"text/html", nil];
        [AFJSONRequestOperation addAcceptableContentTypes:contentType];
        AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
            if ([[JSON objectForKey:@"root"]isEqualToString:@"Updated"]) {
                [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:0] animated:TRUE];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Your ticket details Updated successfully" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                [HUD hide:YES];
            }
            else{
                NSLog(@"Not Updated!!!");
            }
        }
                                                                                            failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                                                                UIAlertView *temp=[[UIAlertView alloc]initWithTitle:@"Message!" message:@"No response from webserver" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                                                [temp show];
                                                                [HUD hide:YES];                               
                                                                                            }];
        [operation start];
    }
    else{
        HUD = [[MBProgressHUD alloc] initWithView:self.view];
        [self.view addSubview:HUD];
        HUD.mode = MBProgressHUDModeAnnularDeterminate;
        HUD.progress = 0.01f;
        [HUD show:YES];
        if ([textViewDescription.text isEqualToString:@""]) {
            UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:Nil message:@"Your description is missing" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alertView show];
            [HUD hide:YES];
        }
        else if ([textFieldTitleToBuyTicket.text isEqualToString:@""]){
            UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:Nil message:@"Your title is missing" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alertView show];
             [HUD hide:YES];
        }
        else if ([textFieldLocation.text isEqualToString:@""] && [textFieldSetLocation.text isEqualToString:@""]){
            UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:Nil message:@"Your location is missing" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alertView show];
             [HUD hide:YES];
        }
        else if ([textFieldZipCode.text isEqualToString:@""]){
            UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:Nil message:@"Your ZipCode is missing" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alertView show];
             [HUD hide:YES];
        }else if ([textFieldZipCode.text length]!=5){
            UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:Nil message:@"ZipCode must contain 5 digits!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alertView show];
            [HUD hide:YES];
        }
        else
        {
          
            //http://extraticketapp.com/extraticket/add_tickets.php?seller_id=1&description=asdf&current_location=&select_location=qww&date_time=2013-08-08%2010:00:00&latitude=123&longitude=456&ticket_title=weqewqw&zipcode=999999999&ticket_type=buy

            NSDate *today = [NSDate date];
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSString *dateString = [dateFormat stringFromDate:today];
            NSString *pathParameters = [NSString stringWithFormat:@"http://extraticketapp.com/extraticket/add_tickets.php?seller_id=%@&description=%@&current_location=%@&select_location=%@&date_time=%@&latitude=%@&longitude=%@&ticket_title=%@&zipcode=%@&ticket_type=%@",loggedUserDetails.stringUserID,textViewDescription.text,textFieldLocation.text,textFieldSetLocation.text,dateString,stringLatitudeToDB,stringLongitudeToDB,textFieldTitleToBuyTicket.text,textFieldZipCode.text,stringCheckBuyOrCell];
            pathParameters = [pathParameters stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            NSURL *url = [[NSURL alloc] initWithString:pathParameters];
            // NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
            NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:20
                                     ];
            NSSet *contentType = [[NSSet alloc] initWithObjects:@"text/plain",@"text/html", nil];
            [AFJSONRequestOperation addAcceptableContentTypes:contentType];
            AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                if ([[JSON objectForKey:@"root"]isEqualToString:@"Inserted"]) {
                     [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:0] animated:TRUE];
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Your ticket details inserted successfully" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                    [switchLocation setOn:NO];
                    textFieldLocation.text=@"";
                    textFieldSetLocation.text=@"";
                    textFieldTitleToBuyTicket.text=@"";
                    textFieldZipCode.text=@"";
                    textViewDescription.text=@"";
                    [HUD hide:YES];
                }
                else{
                    NSLog(@"Not inserted!!!");
                    
                    
                }
            }
                                                                                                failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
//                                                                                                    UIAlertView *temp=[[UIAlertView alloc]initWithTitle:@"Message!" message:@"No response from webserver" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//                                                                [temp show];
                                                                                                     [HUD hide:YES];
                                                                                                 }];
            [operation start];
        }
    }
}
-(IBAction)searchLocation:(id)sender{
    if (![textFieldLocation.text isEqualToString:@""]) {
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:Nil message:@"You have already set current location!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertView show];
    }
    else{
    ETSearchLocationViewController *searchLocationViewController=[[ETSearchLocationViewController alloc]initWithNibName:@"ETSearchLocationViewController" bundle:nil];
    [searchLocationViewController setDelegate:self];
    [self.navigationController pushViewController:searchLocationViewController animated:YES];
    }
}
-(void)sendPlace :(NSString *)place andLattitude :(NSString *)latitude andLongitude :(NSString *)longitude
{
    stringSetLocation=place;
    textFieldSetLocation.text=stringSetLocation;
    stringLatitudeToDB=latitude;
    stringLongitudeToDB=longitude;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
