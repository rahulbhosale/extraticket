//
//  ETLoggedUserDetails.m
//  ExtraTicket
//
//  Created by mac on 10/15/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import "ETLoggedUserDetails.h"
static ETLoggedUserDetails *sharedData=nil;
@implementation ETLoggedUserDetails
@synthesize stringUserID,stringUserEmail,stringUserName,stringUserPhoneNumber,stringBuyerOrSeller,stringProfilePic,stringPassword,stringMessageCount;

+(id)sharedObject{
    if (sharedData==nil) {
        sharedData=[[self alloc]init];
    }
    return sharedData;
}

@end
