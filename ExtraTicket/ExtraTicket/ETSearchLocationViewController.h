//
//  ETSearchLocationViewController.h
//  ExtraTicket
//
//  Created by mac on 10/16/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>


@protocol sendPlaceDelegate <NSObject>
-(void)sendPlace :(NSString *)place andLattitude :(NSString *)latitude andLongitude :(NSString *)longitude;

@end
@interface ETSearchLocationViewController : UIViewController<MKMapViewDelegate,CLLocationManagerDelegate>{
    IBOutlet UISearchBar *searchBarToFindLocationAndGps;
    NSString *stringArea;
    IBOutlet MKMapView *mapViewMyMap;
}
@property(strong,nonatomic)id<sendPlaceDelegate>delegate;

@end
