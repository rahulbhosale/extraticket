//
//  ETForgotPwdViewController.m
//  ExtraTicket
//
//  Created by mac on 10/21/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import "ETForgotPwdViewController.h"
#import "AFNetworking.h"
#import "ETLoggedUserDetails.h"

@interface ETForgotPwdViewController (){
    IBOutlet UITextField *textFieldForEmail;
}

@end

@implementation ETForgotPwdViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationItem.title=@"Forgot Password";
    // Do any additional setup after loading the view from its nib.
}
-(IBAction)btnClickForgotPwdSubmit:(id)sender{
    if ([textFieldForEmail.text isEqualToString:@""]) {
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:Nil message:@"Please Enter Your Email" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertView show];
    }
    else{
    NSString *pathParameters = [NSString stringWithFormat:@"http://extraticketapp.com/extraticket/forgot_password.php?email_id=%@",textFieldForEmail.text];
    pathParameters = [pathParameters stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [[NSURL alloc] initWithString:pathParameters];
    // NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:5
                             ];
    NSSet *contentType = [[NSSet alloc] initWithObjects:@"text/plain",@"text/html", nil];
    [AFJSONRequestOperation addAcceptableContentTypes:contentType];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        if ([[JSON objectForKey:@"root"] isEqualToString:@"Mail Send"]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Your new password has been sent to your mail!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please use the same mail when you have registered!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        }
                                                                                        failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                                                            UIAlertView *temp=[[UIAlertView alloc]initWithTitle:@"Message!" message:@"No response from webserver" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                                                                            [temp show];
                                                                                        }];
    [operation start];
    }
}
#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
