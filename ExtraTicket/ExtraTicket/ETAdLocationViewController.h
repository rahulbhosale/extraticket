//
//  ETAdLocationViewController.h
//  ExtraTicket
//
//  Created by mac on 10/29/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ETSellerDetails.h"

@interface ETAdLocationViewController : UIViewController{
    
}
@property(nonatomic,strong)ETSellerDetails *sellerAdLocationDetails;
@end
