//
//  ETAdDetailViewController.h
//  ExtraTicket
//
//  Created by mac on 10/29/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ETSellerDetails.h"

@interface ETAdDetailViewController : UIViewController{
    
}
-(IBAction)makePhoneCallAction:(id)sender;
-(IBAction)makeChatAction:(id)sender;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil fromPushNotification:(BOOL)isFromPushnotif;
@property(nonatomic,strong)ETSellerDetails *sellerAdDetails;
@end
