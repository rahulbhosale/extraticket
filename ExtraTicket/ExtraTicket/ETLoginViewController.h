//
//  ETLoginViewController.h
//  ExtraTicket
//
//  Created by mac on 10/15/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface ETLoginViewController : UIViewController <CLLocationManagerDelegate>{
    IBOutlet UITextField *textFieldUserName;
    IBOutlet UITextField *textFieldPassword;
    IBOutlet UIButton *buttonRememberMe;
    
    BOOL isSelectedForRememberMe;
}
-(IBAction)didLoginAction:(id)sender;
-(IBAction)didCreateAccountAction:(id)sender;
-(IBAction)didForgotPasswordAction:(id)sender;
@property (nonatomic, strong) CLLocationManager *locationManager;
@end
