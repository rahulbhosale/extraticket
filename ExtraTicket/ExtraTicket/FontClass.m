//
//  FontClass.m
//  SudokkuSweeperApp
//
//  Created by mac on 1/11/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import "FontClass.h"

@implementation FontClass

+(UIFont *)appFont{
    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
    return [UIFont fontWithName:@"Minion Pro" size:25];
    }
    else
    {
        if (iOSDeviceScreenSize.height == 480)
    {

         return [UIFont fontWithName:@"Minion Pro" size:13];
    }
        else
        {
            return [UIFont fontWithName:@"Minion Pro" size:13];
            
        }
    }
}
@end
