//
//  ETMyTicketsMessagesViewController.m
//  ExtraTicket
//
//  Created by mac on 10/24/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import "ETMyTicketsMessagesViewController.h"
#import "ETLoggedUserDetails.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "ETMessageDetails.h"
#import "ETChatViewController.h"
//#import "UIImageView+AFNetworking.h"
#import "SDWebImage/UIImageView+WebCache.h"


@interface ETMyTicketsMessagesViewController (){
    MBProgressHUD *HUD;
    NSMutableArray *arraySellersMessages;
    IBOutlet UITableView *tableViewSellerTicketsMessages;
    NSString *stringFromMyTickets;
    
    NSString *stringSetLocation;
    NSString *stringCurrentLocation;
}

@end

@implementation ETMyTicketsMessagesViewController
@synthesize sellerFromMyTicketsForMessages,stringLoadingFromPush;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
   [self viewChatsOnMyTickets];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    //[self viewChatsOnMyTickets];
    // Do any additional setup after loading the view from its nib.
    if ([stringLoadingFromPush isEqualToString:@"pushnotification"]) {
        UIBarButtonItem *leftBar=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(doneAction)];
        self.navigationItem.rightBarButtonItem=leftBar;
    }
    
        // Do any additional setup after loading the view from its nib.
}
-(void)doneAction{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)viewChatsOnMyTickets{
    arraySellersMessages=[[NSMutableArray alloc]init];
    ETLoggedUserDetails *loggedUserDetails=[ETLoggedUserDetails sharedObject];
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.mode = MBProgressHUDModeAnnularDeterminate;
    HUD.progress = 0.01f;
    [HUD show:YES];
    NSString *pathParameters = [NSString stringWithFormat:@"http://extraticketapp.com/extraticket/view_finalchat.php?user_id=%@&ticket_id=%@",loggedUserDetails.stringUserID,sellerFromMyTicketsForMessages.stringTicketId];
    NSURL *url = [[NSURL alloc] initWithString:pathParameters];
      NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10];
    NSSet *contentType = [[NSSet alloc] initWithObjects:@"text/plain",@"text/html", nil];
    [AFJSONRequestOperation addAcceptableContentTypes:contentType];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        id result=[JSON objectForKey:@"root"];
        if (![result isKindOfClass:[NSArray class]]) {
            [HUD hide:YES];
            UIAlertView *myAlert=[[UIAlertView alloc]initWithTitle:@"No Chats!" message:@"Buyers/sellers didn't yet initiate the chat." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
             myAlert.tag=1;
            [myAlert show];

        }
        else{
            for(int j=0;j<[result count];j++)
            {
                NSDictionary *dictionary = [result objectAtIndex:j];
                ETMessageDetails *messageDetails=[[ETMessageDetails alloc]init];
                messageDetails.stringSenderId=[dictionary objectForKey:@"from_userid"];
                messageDetails.stringReceiverId=[dictionary objectForKey:@"to_userid"];
                messageDetails.stringMessageContent=[dictionary objectForKey:@"message"];
                messageDetails.stringMessagePostedDate=[dictionary objectForKey:@"date_time"];
                messageDetails.stringMessageSenderName=[dictionary objectForKey:@"from_username"];
                messageDetails.stringMessageReceiverName=[dictionary objectForKey:@"to_username"];
                messageDetails.stringMessageId=[dictionary objectForKey:@"message_id"];
                messageDetails.stringRootId=[dictionary objectForKey:@"root_id"];
                messageDetails.stringTicketForMessageId=[dictionary objectForKey:@"ticket_id"];
                messageDetails.stringMessagengerProfilePic=[dictionary objectForKey:@"from_userprofile"];
                messageDetails.stringMessageReceiverProfilePic=[dictionary objectForKey:@"to_userprofile"];
                messageDetails.stringMessageCountForUsersBadge=[dictionary objectForKey:@"badge"];
                [arraySellersMessages addObject:messageDetails];
            }
            [HUD hide:YES];
            [tableViewSellerTicketsMessages reloadData];
        }
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        UIAlertView *temp=[[UIAlertView alloc]initWithTitle:@"Message!" message:@"No response from webserver" delegate:self
                                          cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [temp show];
        [HUD hide:YES];
    }];
    [operation start];
}
#pragma mark - Tableview datasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 75;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [arraySellersMessages count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    NSArray *nibs=[[NSBundle mainBundle] loadNibNamed:@"ETMyTicketsMessagesTableCell" owner:nil options:nil];
    if ([nibs count]>0) {
        cell=[nibs objectAtIndex:0];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    ETMessageDetails *messageDetails=[arraySellersMessages objectAtIndex:indexPath.row];
    ETLoggedUserDetails *loggedUser=[ETLoggedUserDetails sharedObject];
    UIImageView *imageViewProfile=(UIImageView *)[cell viewWithTag:2];
    UIImageView *imageViewDot=(UIImageView *)[cell viewWithTag:6];

    UILabel *labelName=(UILabel *)[cell viewWithTag:1];
    UILabel *labelMessage=(UILabel *)[cell viewWithTag:3];
    UILabel *labelMessageCountBadge=(UILabel *)[cell viewWithTag:4];
    [labelMessageCountBadge setText:messageDetails.stringMessageCountForUsersBadge];
    
    if([messageDetails.stringMessageCountForUsersBadge integerValue] == 0)
    {
        [labelMessageCountBadge setHidden:TRUE];
        [imageViewDot setHidden:TRUE];
        
    }

    if(loggedUser.stringUserID==messageDetails.stringSenderId){
        NSString *imagePath=[NSString stringWithFormat:@"http://extraticketapp.com/extraticket/images/%@",messageDetails.stringMessageReceiverProfilePic];
        UIImage *placeHolder=[UIImage imageNamed:@"profilepic.png"];
        [imageViewProfile sd_setImageWithURL:[NSURL URLWithString:imagePath]
                            placeholderImage:placeHolder
                                     options:SDWebImageRefreshCached];
        //[imageViewProfile setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:placeHolder];
        [labelName setText:messageDetails.stringMessageReceiverName];
    }
    else {
        NSString *imagePath=[NSString stringWithFormat:@"http://extraticketapp.com/extraticket/images/%@",messageDetails.stringMessagengerProfilePic];
        UIImage *placeHolder=[UIImage imageNamed:@"profilepic.png"];
        //[imageViewProfile setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:placeHolder];
        [imageViewProfile sd_setImageWithURL:[NSURL URLWithString:imagePath]
                         placeholderImage:placeHolder
                                  options:SDWebImageRefreshCached];
        [labelName setText:messageDetails.stringMessageSenderName];
    }
    [labelMessage setText:messageDetails.stringMessageContent];

    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    stringFromMyTickets=@"MyTickets";
    ETChatViewController *chatViewController=[[ETChatViewController alloc]initWithNibName:@"ETChatViewController" bundle:nil];
    chatViewController.messageDetailsFromMyTickets=[arraySellersMessages objectAtIndex:indexPath.row];
    [chatViewController setStringFromMyTickets:stringFromMyTickets];
    [chatViewController setStringCurrentLocation:sellerFromMyTicketsForMessages.stringPosterCurrentLocation];
    [chatViewController setStringSetLocation:sellerFromMyTicketsForMessages.stringPosterSetLocation];
    [self.navigationController pushViewController:chatViewController animated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
