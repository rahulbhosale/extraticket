//
//  ETMessageDetails.h
//  ExtraTicket
//
//  Created by mac on 10/23/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ETMessageDetails : NSObject{
    
}
@property(nonatomic,strong)NSString *stringMessageReceiverName;
@property(nonatomic,strong)NSString *stringMessageSenderName;
@property(nonatomic,strong)NSString *stringMessageContent;
@property(nonatomic,strong)NSString *stringMessagePostedDate;
@property(nonatomic,strong)NSString *stringRootId;
@property(nonatomic,strong)NSString *stringReceiverId;
@property(nonatomic,strong)NSString *stringSenderId;
@property(nonatomic,strong)NSString *stringMessageId;
@property(nonatomic,strong)NSString *stringTicketForMessageId;
@property(nonatomic,strong)NSString *stringMessagengerProfilePic;
@property(nonatomic,strong)NSString *stringMessageReceiverProfilePic;
@property(nonatomic,strong)NSString *stringMainMessagePic;
@property(nonatomic,strong)NSString *stringMessageCountForUsersBadge;
@property(nonatomic,strong)NSString *stringADTitle;
@end
