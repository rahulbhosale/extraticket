//
//  ETLoginViewController.m
//  ExtraTicket
//
//  Created by mac on 10/15/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import "ETLoginViewController.h"
#import "ETSignUpViewController.h"
#import "AFNetworking.h"
#import "ETLoggedUserDetails.h"
#import "ETHomeViewController.h"
#import "ETAppDelegate.h"
#import "ETForgotPwdViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "MBProgressHUD.h"

@interface ETLoginViewController ()<CLLocationManagerDelegate,MBProgressHUDDelegate>{
    int RememberValueForAutoLogIn;
//    CLLocationManager *locationManager;
    NSString *latitudeString;
    NSString *longitudeString;
    MBProgressHUD *HUD;
}

@end

@implementation ETLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
   [self.navigationController setNavigationBarHidden:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.locationManager = [[CLLocationManager alloc] init];
    if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [self.locationManager requestAlwaysAuthorization];
    }
    self.locationManager.delegate = self;
    
    self.locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest; // 100 m
    [self.locationManager startUpdatingLocation];
    [self.navigationController setNavigationBarHidden:YES];
       if ([[NSUserDefaults standardUserDefaults]boolForKey:@"RememberMe"]) {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        textFieldUserName.text = [prefs stringForKey:@"Email"];
        textFieldPassword.text = [prefs stringForKey:@"Password"];
        isSelectedForRememberMe=NO;
        RememberValueForAutoLogIn=1;
        [buttonRememberMe setBackgroundImage:[UIImage imageNamed:@"cb_dark_on.png"] forState:UIControlStateNormal];
    }
    else{
        textFieldUserName.placeholder =@"Email";
        textFieldPassword.placeholder =@"Password";
        isSelectedForRememberMe=YES;
    }
   
    // Do any additional setup after loading the view from its nib.
}
-(void)gettingCurrentLocation{

    if (self.locationManager == nil) {
        NSLog(@"here location manager is nil");
        self.locationManager = [[CLLocationManager alloc] init];
    }
    [self.locationManager stopUpdatingLocation];
    self.locationManager.delegate = self;
    
//    self.locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
//    self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    [self.locationManager startUpdatingLocation];
}
#pragma mark- CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    if ([error domain] == kCLErrorDomain) {
        
        // We handle CoreLocation-related errors here
        switch ([error code]) {
                // "Don't Allow" on two successive app launches is the same as saying "never allow". The user
                // can reset this for all apps by going to Settings > General > Reset > Reset Location Warnings.
            case kCLErrorDenied:
                errorAlert.title = @"Warning!!!";
                errorAlert.message = @"Yo have denied the location update, To enable it Go To Settings -> Privacy -> Location Services and enble it for your app.";
                
            case kCLErrorLocationUnknown:
                errorAlert.title = @"Error";
                errorAlert.message = @"Failed to Get Your Location";
            default:
                break;
        }
    } else {
        errorAlert.title = @"Error";
        errorAlert.message = [error localizedDescription];
    }
    

    [errorAlert show];
}
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    [self.locationManager stopUpdatingLocation];
    self.locationManager.delegate = nil;
    CLLocation *currentLocation = newLocation;
    if (currentLocation != nil) {
    }
    float degrees = newLocation.coordinate.latitude;
    double decimal = fabs(newLocation.coordinate.latitude - degrees);
    int minutes = decimal * 60;
    double seconds = decimal * 3600 - minutes * 60;
    degrees = newLocation.coordinate.longitude;
    decimal = fabs(newLocation.coordinate.longitude - degrees);
    minutes = decimal * 60;
    seconds = decimal * 3600 - minutes * 60;
    longitudeString =[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
    latitudeString = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
    [self performLogInAction];
}

//- (void)locationManager:(CLLocationManager *)manager
//     didUpdateLocations:(NSArray *)locations __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_6_0){
//    NSLog(@"locations : %@", [locations objectAtIndex:0]);
//}

-(void)performLogInAction{
    //http://extraticketapp.com/extraticket/login.php?email=a@gmail.com&password=aa
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.mode = MBProgressHUDModeAnnularDeterminate;
    HUD.progress = 0.01f;
    [HUD show:YES];

    if ([textFieldUserName.text isEqualToString:@""] || [textFieldPassword.text isEqualToString:@""]) {
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Some fields are missing" message:@"Please Enter all your fields" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertView show];
        [HUD hide:YES];
    }
    else
    {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *stringDevToken = [prefs valueForKey:@"token"];
        
        NSString *pathParameters = [NSString stringWithFormat:@"http://extraticketapp.com/extraticket/login.php?email=%@&password=%@&longitude=%@&latitude=%@&device_token=%@",textFieldUserName.text,textFieldPassword.text,longitudeString,latitudeString,stringDevToken];
        pathParameters = [pathParameters stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [[NSURL alloc] initWithString:pathParameters];
        // NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10
                                 ];
        NSSet *contentType = [[NSSet alloc] initWithObjects:@"text/plain",@"text/html", nil];
        [AFJSONRequestOperation addAcceptableContentTypes:contentType];
        AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
            if ([[[JSON objectForKey:@"root"]objectForKey:@"Result"]isEqualToString:@"Not a Member"]) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Your login failed.Please check your username and password." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                [HUD hide:YES];
            }
            else{
                ETLoggedUserDetails *loggedUserDetails=[ETLoggedUserDetails sharedObject];
                loggedUserDetails.stringUserID=[[[JSON objectForKey:@"root"]objectForKey:@"Details"] objectForKey:@"id"];
                loggedUserDetails.stringUserName=[[[JSON objectForKey:@"root"]objectForKey:@"Details"] objectForKey:@"username"];
                loggedUserDetails.stringUserEmail=[[[JSON objectForKey:@"root"]objectForKey:@"Details"] objectForKey:@"emailid"];
                loggedUserDetails.stringBuyerOrSeller=[[[JSON objectForKey:@"root"]objectForKey:@"Details"] objectForKey:@"type"];
                loggedUserDetails.stringUserPhoneNumber=[[[JSON objectForKey:@"root"]objectForKey:@"Details"] objectForKey:@"phone_num"];
                loggedUserDetails.stringProfilePic=[[[JSON objectForKey:@"root"]objectForKey:@"Details"] objectForKey:@"profile_pic"];
                loggedUserDetails.stringPassword=[[[JSON objectForKey:@"root"]objectForKey:@"Details"] objectForKey:@"password"];
                loggedUserDetails.stringMessageCount=[[[JSON objectForKey:@"root"]objectForKey:@"Details"] objectForKey:@"message_count"];
                
              if (RememberValueForAutoLogIn==1) {
                  [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"RememberMe"];
              
                  [[NSUserDefaults standardUserDefaults]setObject:textFieldUserName.text forKey:@"Email"];
                  [[NSUserDefaults standardUserDefaults]setObject:textFieldPassword.text forKey:@"Password"];
                  [[NSUserDefaults standardUserDefaults]synchronize];
              }
                
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"LoggedIn"];
                [[NSUserDefaults standardUserDefaults]setObject:loggedUserDetails.stringUserID forKey:@"userID"];
                [[NSUserDefaults standardUserDefaults]setObject:textFieldUserName.text forKey:@"Email"];
                [[NSUserDefaults standardUserDefaults]setObject:textFieldPassword.text forKey:@"Password"];
                [[NSUserDefaults standardUserDefaults]synchronize];
             
                ETAppDelegate *appDelegate=(ETAppDelegate *)[[UIApplication sharedApplication]delegate];
                [appDelegate setLogin];
        }
        }
                                                                                            failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                                                                UIAlertView *temp=[[UIAlertView alloc]initWithTitle:@"Message!" message:@"No response from webserver" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                                                                                [temp show];
                                                                                                [HUD hide:YES];
                                                                                                
                                                                                            }];
        [operation start];
    }
}
#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark UIButtonActions
-(IBAction)isSelectedForRemembeMeAction:(id)sender{
    if (isSelectedForRememberMe) {
        isSelectedForRememberMe=NO;
        RememberValueForAutoLogIn=1;
        [buttonRememberMe setBackgroundImage:[UIImage imageNamed:@"cb_dark_on.png"] forState:UIControlStateNormal];
    }
    else{
        [buttonRememberMe setBackgroundImage:[UIImage imageNamed:@"cb_dark_off.png"] forState:UIControlStateNormal];
        textFieldUserName.text=@"";
        textFieldPassword.text=@"";
        textFieldUserName.placeholder=@"Email";
        textFieldPassword.placeholder=@"Password";
        isSelectedForRememberMe=YES;
    }
  
}
-(IBAction)didCreateAccountAction:(id)sender{
    ETSignUpViewController *signUpViewController=[[ETSignUpViewController alloc]initWithNibName:@"ETSignUpViewController" bundle:nil];
    [self.navigationController pushViewController:signUpViewController animated:YES];
    
}
-(IBAction)didLoginAction:(id)sender{
    if([CLLocationManager locationServicesEnabled] &&
       [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied)
    {
        [self gettingCurrentLocation];
    }
    else{
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Alert!!!" message:@"You have denied location service for this application. Please go to settings and enable location services for this application." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alertView show];
    }
    //http://extraticketapp.com/extraticket/login.php?email=sra@gmail.com&password=sr
}
-(IBAction)didForgotPasswordAction:(id)sender{
    ETForgotPwdViewController *forgotPasswordViewController=[[ETForgotPwdViewController alloc]initWithNibName:@"ETForgotPwdViewController" bundle:nil];
    [self.navigationController pushViewController:forgotPasswordViewController animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
