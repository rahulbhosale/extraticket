//
//  ETAdLocationViewController.m
//  ExtraTicket
//
//  Created by mac on 10/29/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import "ETAdLocationViewController.h"
#import "PPMapViewAnnotation.h"
#import <MapKit/MapKit.h>

@interface ETAdLocationViewController ()<MKMapViewDelegate>{
    NSString *stringLocationDB;
    IBOutlet MKMapView *mapViewMyMap;
}

@end

@implementation ETAdLocationViewController
@synthesize sellerAdLocationDetails;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
  
    [super viewDidLoad];
    self.navigationItem.title=@"Ad Location";
    if ([sellerAdLocationDetails.stringPosterSetLocation isEqualToString:@""]) {
        stringLocationDB=sellerAdLocationDetails.stringPosterCurrentLocation;
    }
    else{
        stringLocationDB=sellerAdLocationDetails.stringPosterSetLocation;
    }
    NSLog(@"showLocatons is %@",stringLocationDB);
    [self showLocatons];
    // Do any additional setup after loading the view from its nib.
}
-(void)showLocatons{
    CLLocationCoordinate2D location = [self addressLocation];
    MKCoordinateRegion region;
    region.center = location;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.5;
    span.longitudeDelta = 0.5;
    region.span=span;
    [mapViewMyMap setRegion:region animated:TRUE];
    PPMapViewAnnotation *myAnnotation=[[PPMapViewAnnotation alloc] initWithTitle:stringLocationDB andCoordinate:location];
    [mapViewMyMap addAnnotation:myAnnotation];
}
-(CLLocationCoordinate2D) addressLocation
{
    float latitude;
    float longitude;
    NSString *stringRequest = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@",[stringLocationDB stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSLog(@"String url = %@",stringRequest);
    NSData *dataResponse = [NSData dataWithContentsOfURL:[NSURL URLWithString:stringRequest]];
    NSError *jsonParsingError = nil;
    NSDictionary *dictionaryFAQs = [NSJSONSerialization JSONObjectWithData:dataResponse options:0 error:&jsonParsingError];
    //  NSString *status=[dictionaryFAQs objectForKey:@"status"];
    {
        NSArray *arrayResults = [dictionaryFAQs objectForKey:@"results"];
        NSDictionary *resultsDictionary = [[arrayResults objectAtIndex:0] objectForKey:@"geometry"];
        NSDictionary *locationDictionary = [resultsDictionary objectForKey:@"location"];
        NSString *stringLatitude = [locationDictionary objectForKey:@"lat"];
        NSString *stringLongitude = [locationDictionary objectForKey:@"lng"];
        latitude = [stringLatitude floatValue];
        longitude = [stringLongitude floatValue];
    };
    CLLocationCoordinate2D loc;
    loc.latitude = latitude;
    loc.longitude = longitude;
    return loc;
}
#pragma mark - map view delegates
//When a map annotation point is added, zoom to it (1500 range)
-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    MKPinAnnotationView *customPinView=[[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"currentloc"];
    customPinView.pinColor = MKPinAnnotationColorPurple;
    customPinView.animatesDrop=TRUE;
    customPinView.canShowCallout = YES;
    customPinView.calloutOffset = CGPointMake(-5, 5);
    
    
    UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    [rightButton setTitle:annotation.title forState:UIControlStateNormal];
    [rightButton addTarget:self
                    action:@selector(showDetails:)
          forControlEvents:UIControlEventTouchUpInside];
    customPinView.rightCalloutAccessoryView = rightButton;
    
    return customPinView;
    
}
-(void)showDetails:(id)sender{
    NSLog(@"MapView Location Button Clickedd");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
