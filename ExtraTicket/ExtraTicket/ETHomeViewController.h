//
//  ETHomeViewController.h
//  ExtraTicket
//
//  Created by mac on 10/15/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+AFNetworking.h"
#define FAVOURITESFINDAFTERPARSEALL 1
#define FAVOURITESFINDAFTERPARSEBUY 2
#define FAVOURITESFINDAFTERPARSESELL 3
@interface ETHomeViewController : UIViewController{
    IBOutlet UITableView *tableViewSellerDetails;
    IBOutlet UISegmentedControl *segmentControlForSellAndBuy;
    NSMutableArray *arraytableData;
    IBOutlet UISearchBar *searchBarForTable;
}

@end
