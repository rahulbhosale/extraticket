//
//  PPMapViewAnnotation.m
//  PostMyParty
//
//  Created by Srishti Innovative Computer Systems on 5/13/13.
//  Copyright (c) 2013 Srishti Innovative Computer Systems. All rights reserved.
//

#import "PPMapViewAnnotation.h"

@implementation PPMapViewAnnotation
@synthesize title, coordinate;

- (id)initWithTitle:(NSString *)ttl andCoordinate:(CLLocationCoordinate2D)c2d {
    if ((self = [super init])) {
        coordinate = c2d;
        title = ttl;
    }
	return self;
}


@end
