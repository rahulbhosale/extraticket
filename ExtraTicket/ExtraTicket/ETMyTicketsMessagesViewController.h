//
//  ETMyTicketsMessagesViewController.h
//  ExtraTicket
//
//  Created by mac on 10/24/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ETSellerDetails.h"

@interface ETMyTicketsMessagesViewController : UIViewController{
    
}
@property(nonatomic,strong)ETSellerDetails *sellerFromMyTicketsForMessages;
@property(nonatomic,strong)NSString *stringLoadingFromPush;
@end
