//
//  ETTermsAndConditionsViewController.h
//  ExtraTicket
//
//  Created by mac on 1/2/14.
//  Copyright (c) 2014 mac. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol sendAcceptDelegate <NSObject>
-(void)sendAccept :(NSString *)acceptTermsAndConditions;

@end


@interface ETTermsAndConditionsViewController : UIViewController
{
    IBOutlet UIWebView *webViewLoadContents;
   
}
-(IBAction)didAgreeAction:(id)sender;
-(IBAction)didCancelAction:(id)sender;
@property(strong,nonatomic)id<sendAcceptDelegate>delegate;
@end
