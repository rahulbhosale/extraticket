//
//  ETChatViewController.h
//  ExtraTicket
//
//  Created by mac on 10/22/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreLocation/CoreLocation.h>
#import "PTSMessagingCell.h"
#import "MBProgressHUD.h"
#import "ETSellerDetails.h"
#import "ETMessageDetails.h"
#import <MapKit/MapKit.h>

//ETChatMessageWithCamera

#import "HPGrowingTextView.h"
#import "MNMPullToRefreshManager.h"

@class QBPopupMenu;


@interface ETChatViewController : UIViewController<UITextViewDelegate,CLLocationManagerDelegate,MKMapViewDelegate,HPGrowingTextViewDelegate,UIGestureRecognizerDelegate,MNMPullToRefreshManagerClient,UIImagePickerControllerDelegate,UINavigationControllerDelegate>{
    BOOL isKeyboardShown;
    UIToolbar *toolBarComment;
    UITextView *textViewComment;
    NSMutableArray * messagesArray;
    IBOutlet UITableView *tableChat;
    MBProgressHUD *HUD;
    NSString *stringComment;
    IBOutlet MKMapView *mapViewMyMap;
    
    //ETChatMessageWithCamera
    NSMutableArray *sphBubbledata;
    UIView *containerView;
    UITextView *textView;
    int selectedRow;
    BOOL newMedia;
    NSData *pngData;
    BOOL isImagePicked;
    UIImage *imageChat;
}
@property(nonatomic,strong)ETMessageDetails *messageDetailsFromMyTickets;
@property(nonatomic,strong)ETSellerDetails *sellerFromMyTicketsForChat;
@property(nonatomic,strong)NSString *stringFromMyTickets;
@property(nonatomic,strong)NSString *stringSetLocation;
@property(nonatomic,strong)NSString *stringCurrentLocation;
@property(nonatomic,strong)NSString *stringLoadingFromPush;
-(IBAction)onPostButtonPressed:(id)sender;

//ETChatMessageWithCamera

@property (nonatomic, readwrite, assign) NSUInteger reloads;
@property (nonatomic, readwrite, strong) MNMPullToRefreshManager *pullToRefreshManager;

@property (weak, nonatomic) IBOutlet UIImageView *Uploadedimage;
@property (nonatomic, strong) QBPopupMenu *popupMenu;
@property (weak, nonatomic) IBOutlet UITableView *sphChatTable;
@property (nonatomic, retain) UIImagePickerController *imgPicker;

- (void) handleURL:(NSURL *)url;


@end
