//
//  ETCommonTableViewCell.m
//  ExtraTicket
//
//  Created by mac on 10/15/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import "ETCommonTableViewCell.h"

@implementation ETCommonTableViewCell
@synthesize textFieldLogin,buttonLogIn;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}
-(id)initWithLogIn:(NSString *)text
{
    self=[super init];
    if (self) {
        textFieldLogin = [[UITextField alloc] initWithFrame:CGRectMake(10, 14, 300, 30)];
        textFieldLogin.placeholder=text;
        buttonLogIn= [[UIButton alloc] initWithFrame:CGRectMake(10, 14, 300, 30)];
        [self.contentView addSubview:buttonLogIn];
        [self.contentView addSubview:textFieldLogin];
    }
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
