//
//  ETSearchLocationViewController.m
//  ExtraTicket
//
//  Created by mac on 10/16/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import "ETSearchLocationViewController.h"
#import "PPMapViewAnnotation.h"

@interface ETSearchLocationViewController (){
    NSString *stringLatitude;
    NSString *stringLongitude;
}

@end

@implementation ETSearchLocationViewController
@synthesize delegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)showLocatons{
    stringArea=searchBarToFindLocationAndGps.text;
    CLLocationCoordinate2D location = [self addressLocation];
    if (location.latitude == 0.0 && location.longitude == 0.0) {
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Searched location could not found!!!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alertView show];

    }else{
        MKCoordinateRegion region;
        region.center = location;
        MKCoordinateSpan span;
        span.latitudeDelta = 0.5;
        span.longitudeDelta = 0.5;
        region.span=span;
        [mapViewMyMap setRegion:region animated:TRUE];
        PPMapViewAnnotation *myAnnotation=[[PPMapViewAnnotation alloc] initWithTitle:stringArea andCoordinate:location];
        [mapViewMyMap addAnnotation:myAnnotation];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *leftBar=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(done)];
    self.navigationItem.leftBarButtonItem=leftBar;
    // Do any additional setup after loading the view from its nib.
}

-(void)done
{
    [self.delegate sendPlace:stringArea andLattitude:stringLatitude andLongitude:stringLongitude];
    //[self.delegate sendPlace:stringArea :stringLatitude :stringLongitude];
    [self.navigationController popViewControllerAnimated:YES];
}
-(CLLocationCoordinate2D) addressLocation
{
    float latitude = 0.0;
    float longitude = 0.0;
    NSString *stringRequest = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@",[stringArea stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSLog(@"String url = %@",stringRequest);
    NSData *dataResponse = [NSData dataWithContentsOfURL:[NSURL URLWithString:stringRequest]];
    NSError *jsonParsingError = nil;
    NSDictionary *dictionaryFAQs = [NSJSONSerialization JSONObjectWithData:dataResponse options:0 error:&jsonParsingError];
   // NSString *status=[dictionaryFAQs objectForKey:@"status"];
    
        NSArray *arrayResults = [dictionaryFAQs objectForKey:@"results"];
        NSUInteger count = [arrayResults count];
        if (count > 0) {
            NSDictionary *resultsDictionary = [[arrayResults objectAtIndex:0] objectForKey:@"geometry"];
            NSDictionary *locationDictionary = [resultsDictionary objectForKey:@"location"];
            stringLatitude = [locationDictionary objectForKey:@"lat"];
            stringLongitude = [locationDictionary objectForKey:@"lng"];
            latitude = [stringLatitude floatValue];
            longitude = [stringLongitude floatValue];
        }
    
    CLLocationCoordinate2D loc;
    loc.latitude = latitude;
    loc.longitude = longitude;
    return loc;
}
#pragma mark - map view delegates
//When a map annotation point is added, zoom to it (1500 range)
-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    MKPinAnnotationView *customPinView=[[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"currentloc"];
    customPinView.pinColor = MKPinAnnotationColorPurple;
    customPinView.animatesDrop=TRUE;
    customPinView.canShowCallout = YES;
    customPinView.calloutOffset = CGPointMake(-5, 5);
    
    
    UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    [rightButton setTitle:annotation.title forState:UIControlStateNormal];
    [rightButton addTarget:self
                    action:@selector(showDetails:)
          forControlEvents:UIControlEventTouchUpInside];
    customPinView.rightCalloutAccessoryView = rightButton;
    
    return customPinView;
    
}
-(void)showDetails:(id)sender{
    NSLog(@"MapView Location Button Clickedd");
}

#pragma mark SearchBar Delegates
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    // only show the status bar's cancel button while in edit mode
    searchBarToFindLocationAndGps.showsCancelButton = YES;
    searchBarToFindLocationAndGps.autocorrectionType = UITextAutocorrectionTypeNo;
    // flush the previous search content
    
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    searchBarToFindLocationAndGps.showsCancelButton = NO;
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    searchBarToFindLocationAndGps.text=@"";
    [searchBarToFindLocationAndGps resignFirstResponder];
}
// called when Search (in our case “Done”) button pressed
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self showLocatons];
    [searchBar resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
