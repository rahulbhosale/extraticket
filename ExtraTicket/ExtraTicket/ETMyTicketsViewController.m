//
//  ETMyTicketsViewController.m
//  ExtraTicket
//
//  Created by mac on 10/17/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import "ETMyTicketsViewController.h"
#import "MFSideMenuContainerViewController.h"
#import "MBProgressHUD.h"
#import "AFNetworking.h"
#import "ETSellerDetails.h"
#import "ETLoggedUserDetails.h"
#import "UIImageView+AFNetworking.h"
#import "ETBuyTicketViewController.h"
#import "ETChatViewController.h"
#import "ETMyTicketsMessagesViewController.h"
#import "ETAdLocationViewController.h"
#import "ETAdDetailViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "ETAppDelegate.h"


@interface ETMyTicketsViewController (){
    MBProgressHUD *HUD;
    NSMutableArray *arraySellerDetails;
    IBOutlet UITableView *tableViewSellerDetails;
    NSString *stringCheckEdit;
}

@end

@implementation ETMyTicketsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
  [self getTicketDetails];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    arraySellerDetails=[[NSMutableArray alloc]init];
    [self setupMenuBarButtonItems];
    UIBarButtonItem *rightBar=[[UIBarButtonItem alloc]initWithTitle:@"Delete" style:UIBarButtonItemStylePlain target:self action:@selector(DeleteAction)];
    self.navigationItem.rightBarButtonItem=rightBar;
}
-(void)DeleteAction{
    if(self.editing)
	{
		[super setEditing:NO animated:NO];
		[tableViewSellerDetails setEditing:NO animated:NO];
		[tableViewSellerDetails reloadData];
		self.navigationItem.rightBarButtonItem.title=@"delete";
    }
	else
    {
		[super setEditing:YES animated:YES];
		[tableViewSellerDetails setEditing:YES animated:YES];
		[tableViewSellerDetails reloadData];
        self.navigationItem.rightBarButtonItem.title=@"done";
        
	}
}
#pragma  mark-split view controller
- (MFSideMenuContainerViewController *)menuContainerViewController {
    return (MFSideMenuContainerViewController *)self.navigationController.parentViewController;
}
#pragma mark - UIBarButtonItems

- (void)setupMenuBarButtonItems
{
    UIButton *leftBarButtonItem = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftBarButtonItem setImage:[UIImage imageNamed:@"menu-icon.png"] forState:UIControlStateNormal];
    leftBarButtonItem.frame = CGRectMake(0, 0, 43, 28);
    [leftBarButtonItem addTarget:self action:@selector(leftSideMenuButtonPressed:) forControlEvents:
     UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftBarButtonItem];
}


#pragma mark - UIBarButtonItem Callbacks

- (void)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)leftSideMenuButtonPressed:(id)sender
{
     [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadMenuTable" object:nil];
    
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^
     {
         [self setupMenuBarButtonItems];
     }];
}

- (void)rightSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleRightSideMenuCompletion:^
     {
         [self setupMenuBarButtonItems];
     }];
}
-(void)getTicketDetails{
    ETLoggedUserDetails *loggedUserDetails=[ETLoggedUserDetails sharedObject];
    [arraySellerDetails removeAllObjects];
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.mode = MBProgressHUDModeAnnularDeterminate;
    HUD.progress = 0.01f; 
    [HUD show:YES];
    NSString *pathParameters = [NSString stringWithFormat:@"http://extraticketapp.com/extraticket/list_mytickets.php?user_id=%@",loggedUserDetails.stringUserID];
    NSURL *url = [[NSURL alloc] initWithString:pathParameters];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10];
    NSSet *contentType = [[NSSet alloc] initWithObjects:@"text/plain",@"text/html", nil];
    [AFJSONRequestOperation addAcceptableContentTypes:contentType];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        id result=[JSON objectForKey:@"root"];
        if (![result isKindOfClass:[NSArray class]]) {
            [HUD hide:YES];
        }
        else{
            for(int j=0;j<[result count];j++)
            {
                NSDictionary *dictionary = [result objectAtIndex:j];
                ETSellerDetails *sellerDetails=[[ETSellerDetails alloc]init];
                sellerDetails.stringPosterId=[dictionary objectForKey:@"user_id"];
                sellerDetails.stringAdTitle=[dictionary objectForKey:@"ticket_title"];
                sellerDetails.stringDistance=[dictionary objectForKey:@"distance"];
                sellerDetails.stringPosterDescription=[dictionary objectForKey:@"ticket_description"];
                sellerDetails.stringPosterCurrentLocation=[dictionary objectForKey:@"current_location"];
                sellerDetails.stringPosterSetLocation=[dictionary objectForKey:@"select_location"];
                sellerDetails.stringPosterName=[dictionary objectForKey:@"user_name"];
                sellerDetails.stringPosterPhoneNo=[dictionary objectForKey:@"user_phone"];
                sellerDetails.stringPosterDate=[dictionary objectForKey:@"date_time"];
                sellerDetails.stringPosterImage=[dictionary objectForKey:@"user_profile_pic"];
                sellerDetails.stringLatitude=[dictionary objectForKey:@"latitude"];
                sellerDetails.stringLongitude=[dictionary objectForKey:@"longitude"];
                sellerDetails.stringTicketId=[dictionary objectForKey:@"ticket_id"];
                sellerDetails.stringTypeOfTicket=[dictionary objectForKey:@"type"];
                sellerDetails.stringZipCode=[dictionary objectForKey:@"zipcode"];
                sellerDetails.stringBadgeForMessageCount=[dictionary objectForKey:@"badge"];
                [arraySellerDetails addObject:sellerDetails];
            }
            [HUD hide:YES];
            [tableViewSellerDetails reloadData];
        }
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        UIAlertView *temp=[[UIAlertView alloc]initWithTitle:@"Message!" message:@"No response from webserver" delegate:self
                                          cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [temp show];
        [HUD hide:YES];
    }];
    [operation start];
}

#pragma mark - Tableview datasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 150;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [arraySellerDetails count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"HomeTableCell";
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell==nil) {
        NSArray *nibs=[[NSBundle mainBundle] loadNibNamed:@"ETHomeTableCell" owner:nil options:nil];
        if ([nibs count]>0) {
            cell=[nibs objectAtIndex:0];
        }
    }

    ETSellerDetails *sellerDetails=[arraySellerDetails objectAtIndex:indexPath.row];
    UITextView *textViewDocument=(UITextView *)[cell viewWithTag:1];
    [textViewDocument setText:sellerDetails.stringAdTitle];
    UIImageView *imageViewProfile=(UIImageView *)[cell viewWithTag:2];
    NSString *imagePath=[NSString stringWithFormat:@"http://extraticketapp.com/extraticket/images/%@",sellerDetails.stringPosterImage];
    UIImage *placeHolder=[UIImage imageNamed:@"profilepic.png"];
    //TODO : Have to comment : Baji
    [imageViewProfile sd_setImageWithURL:[NSURL URLWithString:imagePath]
                        placeholderImage:placeHolder
                                 options:SDWebImageRefreshCached];
    
    [imageViewProfile sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:placeHolder];
    
    UILabel *labelName=(UILabel *)[cell viewWithTag:3];
    [labelName setText:sellerDetails.stringPosterName];
    UILabel *labelPhoneNo=(UILabel *)[cell viewWithTag:4];
    [labelPhoneNo setText:sellerDetails.stringPosterPhoneNo];
    [labelPhoneNo setHidden:TRUE];
    UILabel *labelDate=(UILabel *)[cell viewWithTag:5];
    [labelDate setText:sellerDetails.stringPosterDate];
    UIButton *buttonEdit=(UIButton *)[cell viewWithTag:6];
    [buttonEdit addTarget:self action:@selector(editAction:) forControlEvents:UIControlEventTouchUpInside];
    UILabel *labelStartChat=(UILabel *)[cell viewWithTag:7];
    [labelStartChat setHidden:TRUE];
     UIImageView *imageViewTime=(UIImageView *)[cell viewWithTag:8];
     UIImageView *imageViewPhone=(UIImageView *)[cell viewWithTag:9];
    [imageViewTime setHidden:FALSE];
    [imageViewPhone setHidden:TRUE];
    UIButton *buttonFavourites=(UIButton *)[cell viewWithTag:11];
    [buttonFavourites setHidden:TRUE];
    UIButton *buttonPhone=(UIButton *)[cell viewWithTag:12];
    [buttonPhone setHidden:TRUE];
    UIButton *buttonChat=(UIButton *)[cell viewWithTag:11];
    [buttonChat addTarget:self action:@selector(chatAction:) forControlEvents:UIControlEventTouchUpInside];
    UILabel *labelDistanceMiles=(UILabel *)[cell viewWithTag:15];
    if ([sellerDetails.stringDistance isKindOfClass:[NSNull class]]) {
        
    }
    else if ([sellerDetails.stringDistance isEqualToString:@"0"]) {
        [labelDistanceMiles setText:sellerDetails.stringDistance];
    }

    else{
        NSString  *mystr=[sellerDetails.stringDistance substringToIndex:3];
        [labelDistanceMiles setText:mystr];
    }

    UIButton *buttonLocation=(UIButton *)[cell viewWithTag:13];
    [buttonLocation addTarget:self action:@selector(getLocation:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *buttonAdDetails=(UIButton *)[cell viewWithTag:14];
    [buttonAdDetails addTarget:self action:@selector(showDetailsAdAction:) forControlEvents:UIControlEventTouchUpInside];
    UILabel *labelType=(UILabel *)[cell viewWithTag:17];
    [labelType setText:[NSString stringWithFormat:@"(%@)",sellerDetails.stringTypeOfTicket]];
    UILabel *labelZipCode=(UILabel *)[cell viewWithTag:18];
    [labelZipCode setText:sellerDetails.stringZipCode];
    UILabel *labelMessageCount=(UILabel *)[cell viewWithTag:19];
    [labelMessageCount setText:sellerDetails.stringBadgeForMessageCount];
    UIImageView *imgView=(UIImageView *)[cell viewWithTag:20];
    if([sellerDetails.stringBadgeForMessageCount integerValue] == 0)
    {
        [labelMessageCount setHidden:TRUE];
        [imgView setHidden:TRUE];

    }
    return cell;
}
-(void)getLocation:(UIButton *)sender{
    UITableViewCell *cell;
    NSIndexPath *indexPath;
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // Load resources for iOS 6.1 or earlier
        cell  = (UITableViewCell *)[sender.superview superview];
        indexPath= [tableViewSellerDetails indexPathForCell:cell];
    } else {
        // Load resources for iOS 7 or later
        cell  = (UITableViewCell *)[[sender.superview superview] superview];
        indexPath = [tableViewSellerDetails indexPathForCell:cell];
    }
    ETAdLocationViewController *locationViewController=[[ETAdLocationViewController alloc]initWithNibName:@"ETAdLocationViewController" bundle:nil];
    locationViewController.sellerAdLocationDetails=[arraySellerDetails objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:locationViewController animated:YES];
    
}
-(void)showDetailsAdAction:(UIButton *)sender{
    UITableViewCell *cell;
    NSIndexPath *indexPath;
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // Load resources for iOS 6.1 or earlier
        cell  = (UITableViewCell *)[sender.superview superview];
        indexPath= [tableViewSellerDetails indexPathForCell:cell];
    } else {
        // Load resources for iOS 7 or later
        cell  = (UITableViewCell *)[[sender.superview superview] superview];
        indexPath = [tableViewSellerDetails indexPathForCell:cell];
    }
    ETAdDetailViewController *adDetailViewController=[[ETAdDetailViewController alloc]initWithNibName:@"ETAdDetailViewController" bundle:nil];
    adDetailViewController.sellerAdDetails=[arraySellerDetails objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:adDetailViewController animated:YES];
    
}
-(void)chatAction:(UIButton *)sender{
    UITableViewCell *cell;
    NSIndexPath *indexPath;
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // Load resources for iOS 6.1 or earlier
        cell  = (UITableViewCell *)[sender.superview superview];
        indexPath= [tableViewSellerDetails indexPathForCell:cell];
    } else {
        // Load resources for iOS 7 or later
        cell  = (UITableViewCell *)[[sender.superview superview] superview];
        indexPath = [tableViewSellerDetails indexPathForCell:cell];
    }
    ETMyTicketsMessagesViewController *myTicketsMessagesViewController=[[ETMyTicketsMessagesViewController alloc]initWithNibName:@"ETMyTicketsMessagesViewController" bundle:nil];
    myTicketsMessagesViewController.sellerFromMyTicketsForMessages=[arraySellerDetails objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:myTicketsMessagesViewController animated:YES];
}
//http://extraticketapp.com/extraticket/view_chat.php?from_userid=3&to_userid=2&ticket_id=8

-(void)editAction:(UIButton *)sender{
    stringCheckEdit=@"Edit";
    UITableViewCell *cell;
    NSIndexPath *indexPath;
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // Load resources for iOS 6.1 or earlier
        cell  = (UITableViewCell *)[sender.superview superview];
        indexPath= [tableViewSellerDetails indexPathForCell:cell];
    } else {
        // Load resources for iOS 7 or later
        cell  = (UITableViewCell *)[[sender.superview superview] superview];
        indexPath = [tableViewSellerDetails indexPathForCell:cell];
    }
    ETBuyTicketViewController *buyTicketViewController=[[ETBuyTicketViewController alloc]initWithNibName:@"ETBuyTicketViewController" bundle:nil];
    buyTicketViewController.sellerFromMyTickets=[arraySellerDetails objectAtIndex:indexPath.row];
    [buyTicketViewController setStringCheckEdit:stringCheckEdit];
    [self.navigationController pushViewController:buyTicketViewController animated:YES];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // No editing style if not editing or the index path is nil.
    if (self.editing == NO || !indexPath )
        return UITableViewCellEditingStyleNone;
    
    else
	{
		return UITableViewCellEditingStyleDelete;
	}
    return UITableViewCellEditingStyleNone;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {//http://extraticketapp.com/extraticket/delete_myticket.php?ticket_id=5
        ETSellerDetails *sellerDetails=[arraySellerDetails objectAtIndex:indexPath.row];
        NSString *pathParameters = [NSString stringWithFormat:@"http://extraticketapp.com/extraticket/delete_myticket.php?ticket_id=%@",sellerDetails.stringTicketId];
        pathParameters = [pathParameters stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [[NSURL alloc] initWithString:pathParameters];
        // NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10];
        NSSet *contentType = [[NSSet alloc] initWithObjects:@"text/plain",@"text/html", nil];
        [AFJSONRequestOperation addAcceptableContentTypes:contentType];
        AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
            if ([[JSON objectForKey:@"root"]isEqualToString:@"Deleted"]) {
                [arraySellerDetails removeObjectAtIndex:indexPath.row];
                [tableViewSellerDetails reloadData];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Your ticket have been deleted successfully" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
            else{
                NSLog(@"Not deleted!!!");
              
            }
        }
                                                                                            failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                                                                UIAlertView *temp=[[UIAlertView alloc]initWithTitle:@"Message!" message:@"No response from webserver" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                                                                                [temp show];
                                                                                            }];
        [operation start];
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
