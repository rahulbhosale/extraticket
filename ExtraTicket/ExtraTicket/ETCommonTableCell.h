//
//  ETCommonTableCell.h
//  ExtraTicket
//
//  Created by mac on 12/6/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ETSellerDetails.h"
@interface ETCommonTableCell : UITableViewCell{
    
}

@property(nonatomic,strong)ETSellerDetails *sellerDetails;
@property(nonatomic,strong)UIImageView *imageViewBgTop;
@property(nonatomic,strong)UIImageView *imageViewProfilePic;
@property(nonatomic,strong)UIImageView *imageViewFullBg;
@property(nonatomic,strong)UIImageView *imageViewTitle;
@property(nonatomic,strong)UIImageView *imageViewPhone;
@property(nonatomic,strong)UIImageView *imageViewTime;

@property(nonatomic,strong)UILabel *labelName;
@property(nonatomic,strong)UILabel *labelTypeSellOrBuy;
@property(nonatomic,strong)UILabel *labelTime;
@property(nonatomic,strong)UILabel *labelPhoneNumber;
@property(nonatomic,strong)UILabel *labelSetFavour;
@property(nonatomic,strong)UILabel *labelMiles;
@property(nonatomic,strong)UILabel *labelMilesStaticText;
@property(nonatomic,strong)UILabel *labelZipCode;

@property(nonatomic,strong)UIButton *buttonSetFavourite;
@property(nonatomic,strong)UIButton *buttonPhone;
@property(nonatomic,strong)UIButton *buttonMap;
@property(nonatomic,strong)UIButton *buttonTitleExpansion;

@property(nonatomic,strong)UITextView *textViewTitle;

@end
