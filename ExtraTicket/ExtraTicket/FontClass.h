//
//  FontClass.h
//  SudokkuSweeperApp
//
//  Created by mac on 1/11/13.
//  Copyright (c) 2013 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FontClass : NSObject
+(UIFont *)appFont;
@end
